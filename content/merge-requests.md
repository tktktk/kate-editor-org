---
title: Merge Requests
hideMeta: true
author: Christoph Cullmann
date: 2021-01-24T01:01:01+00:00
menu:
  main:
    weight: 100
    parent: menu
---

This pages provides an overview of the merge requests we are current working on or did already accepted for the Kate, KTextEditor, KSyntaxHighlighting and kate-editor.org repositories.

## Currently Open Merge Requests
Our team is still working on the following requests. Feel free to take a look and help out, if any of them is interesting for you!
### Kate

- **[Improve appearance of sessions plasmoid](https://invent.kde.org/utilities/kate/-/merge_requests/441)**<br />
Request authored by [Mufeed Ali](https://invent.kde.org/fushinari).

- **[LSP workspace folders](https://invent.kde.org/utilities/kate/-/merge_requests/436)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts).

- **[lspclient: bypass shutdown delay if not needed and cleanup defunct code](https://invent.kde.org/utilities/kate/-/merge_requests/440)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts).

- **[sessions: upgrade anonymous session to regular one](https://invent.kde.org/utilities/kate/-/merge_requests/422)**<br />
Request authored by [Michal Humpula](https://invent.kde.org/michalhumpula).

- **[Netshare performance optimizations](https://invent.kde.org/utilities/kate/-/merge_requests/368)**<br />
Request authored by [Robert Hoffmann](https://invent.kde.org/hoffmannrobert).

- **[Utilize kcoreaddons_add_plugin CMake method](https://invent.kde.org/utilities/kate/-/merge_requests/404)**<br />
Request authored by [Alexander Lohnau](https://invent.kde.org/alex).

- **[Draft: Project plugin drag & drop](https://invent.kde.org/utilities/kate/-/merge_requests/385)**<br />
Request authored by [Krzysztof Stokop](https://invent.kde.org/mynewnickname).

### KTextEditor

- **[Improvements to the sizing / positioning of the completion widget](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/172)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar).

- **[Add a button to searchbar to replace current match only](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/61)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir).

- **[Netshare performance optimizations](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/133)**<br />
Request authored by [Robert Hoffmann](https://invent.kde.org/hoffmannrobert).

- **[Draft: Port QStringRef (deprecated) to QStringView](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/142)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir).

### KSyntaxHighlighting

- **[GLSL: based on GLSL 4.6](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/205)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen).


## Overall Accepted Merge Requests
- 386 patches for [Kate](https://invent.kde.org/utilities/kate/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 161 patches for [KTextEditor](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 197 patches for [KSyntaxHighlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 30 patches for [kate-editor.org](https://invent.kde.org/websites/kate-editor-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)



## Accepted Merge Requests of 2021

- 254 patches for [Kate](https://invent.kde.org/utilities/kate/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 112 patches for [KTextEditor](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 65 patches for [KSyntaxHighlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 22 patches for [kate-editor.org](https://invent.kde.org/websites/kate-editor-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)

### June 2021

#### Week 25

- **Kate - [LSP Completion: Support textEdit](https://invent.kde.org/utilities/kate/-/merge_requests/438)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 5 days.

- **KSyntaxHighlighting - [Remove unneeded LGPL-2.0-only license text](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/206)**<br />
Request authored by [Andreas Cord-Landwehr](https://invent.kde.org/cordlandwehr) and merged at creation day.

- **kate-editor.org - [Blog about the recently added dart support](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/32)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

#### Week 24

- **KSyntaxHighlighting - [GLSL: add types, qualifiers and reserved keywords](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/204)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KTextEditor - [Some more improvements to minimap perf](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/171)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Improve minimap performance](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/170)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [Falcon theme](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/202)**<br />
Request authored by [Alberto Salvia Novella](https://invent.kde.org/albertosalvianovella) and merged after 4 days.

- **KTextEditor - [Make completion more efficient](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/168)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Always emit delayedUpdateOfView to fix update issues](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/169)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Improve minimap performance](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/165)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 4 days.

- **KTextEditor - [Always check for shouldStartCompletion](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/167)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 3 days.

- **Kate - [LSP maintenance](https://invent.kde.org/utilities/kate/-/merge_requests/439)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after one day.

- **KTextEditor - [Update mode config page UI when a filetype's name is changed](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/166)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after one day.

- **KSyntaxHighlighting - [Update IRC network name in wml.xml comment](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/203)**<br />
Request authored by [Nicolás Alvarez](https://invent.kde.org/nalvarez) and merged at creation day.

- **Kate - [LSP var substitution and path search](https://invent.kde.org/utilities/kate/-/merge_requests/435)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after 5 days.

- **KSyntaxHighlighting - [Julia highlighting fixes](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/201)**<br />
Request authored by [Ilia Kats](https://invent.kde.org/iliakats) and merged after 2 days.

- **Kate - [implement workspace/symbols request](https://invent.kde.org/utilities/kate/-/merge_requests/437)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **KTextEditor - [improve handling of repainting for ranges](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/163)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 2 days.

#### Week 23

- **KTextEditor - [Set devicePixelRatio for drag pixmap](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/164)**<br />
Request authored by [Kai Uwe Broulik](https://invent.kde.org/broulik) and merged at creation day.

- **KTextEditor - [Show Custom Filetypes on config page](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/156)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after 16 days.

- **KTextEditor - [Mark TextRange and TextCursor classes as final](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/162)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [katetextrange: Use non-virtual functions internally](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/161)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **Kate - [Dont create moving ranges when attribute is null](https://invent.kde.org/utilities/kate/-/merge_requests/434)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Simplify Ctrl-Click cursor unsetting](https://invent.kde.org/utilities/kate/-/merge_requests/432)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [katepluginmanager: Remove unneeded check for service types](https://invent.kde.org/utilities/kate/-/merge_requests/431)**<br />
Request authored by [Alexander Lohnau](https://invent.kde.org/alex) and merged at creation day.

- **Kate - [Set ZDepth before setAttribute for MovingRanges](https://invent.kde.org/utilities/kate/-/merge_requests/433)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [fallback to directory of document for root](https://invent.kde.org/utilities/kate/-/merge_requests/427)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **Kate - [LSP maintenance](https://invent.kde.org/utilities/kate/-/merge_requests/426)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after one day.

- **Kate - [LSP: Support textDocument/SemanticTokens/range request](https://invent.kde.org/utilities/kate/-/merge_requests/429)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Always call MovingRange::setZDepth() before setAttribute()](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/160)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged at creation day.

- **KTextEditor - [Clean up katedocument](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/158)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 4 days.

- **KTextEditor - [Un-overload signals](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/137)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after 55 days.

#### Week 22

- **Kate - [Add Dart LSP](https://invent.kde.org/utilities/kate/-/merge_requests/428)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Clean up katefiletreemodel](https://invent.kde.org/utilities/kate/-/merge_requests/425)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 4 days.

- **KTextEditor - [syntax: less manual memory mgmt and cleanup](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/157)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 8 days.

- **KTextEditor - [Take into account wordCompletionRemoveTail in completionRange() default implementation](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/148)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after 29 days.

- **KSyntaxHighlighting - [Add Dart language](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/200)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Fix vim cursor gets stuck with 'b'](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/159)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [SQL addon: use KPasswordLineEdit instead of KLineEdit::passwordMode](https://invent.kde.org/utilities/kate/-/merge_requests/423)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged at creation day.

### May 2021

#### Week 21

- **Kate - [lspclient: add action to request and apply quick fix code action](https://invent.kde.org/utilities/kate/-/merge_requests/421)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged at creation day.

- **Kate - [kateview*: Use more std containers and cleanup](https://invent.kde.org/utilities/kate/-/merge_requests/416)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 4 days.

- **Kate - [Prevent crash in git-blame for HUGE git commits](https://invent.kde.org/utilities/kate/-/merge_requests/420)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged at creation day.

- **Kate - [Work around conflicting shortcuts in preview plugin](https://invent.kde.org/utilities/kate/-/merge_requests/419)**<br />
Request authored by [Christoph Roick](https://invent.kde.org/croick) and merged after 2 days.

- **Kate - [Small cmake cleanups of things no longer needed due to dep version bumps](https://invent.kde.org/utilities/kate/-/merge_requests/418)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged after one day.

- **Kate - [KateDocManager: Less manual memory handling and use more std stuff](https://invent.kde.org/utilities/kate/-/merge_requests/415)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 3 days.

- **Kate - [No manual memory management for katesession*](https://invent.kde.org/utilities/kate/-/merge_requests/413)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Fix crash when turning on "bg shading" for Documents plugin](https://invent.kde.org/utilities/kate/-/merge_requests/417)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Adjust completion parentheses behavior](https://invent.kde.org/utilities/kate/-/merge_requests/412)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after one day.

- **Kate - [S&R: Fix matching ^ and $ in multi-line expressions](https://invent.kde.org/utilities/kate/-/merge_requests/414)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged at creation day.

- **KTextEditor - [Limit shortcut context to active view for Reuse Word Above/Below](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/155)**<br />
Request authored by [Christoph Roick](https://invent.kde.org/croick) and merged at creation day.

- **Kate - [Project context menu update](https://invent.kde.org/utilities/kate/-/merge_requests/375)**<br />
Request authored by [Krzysztof Stokop](https://invent.kde.org/mynewnickname) and merged after 28 days.

- **Kate - [Style LSP locations treeview for better readibility](https://invent.kde.org/utilities/kate/-/merge_requests/406)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 5 days.

- **Kate - [Reduce manual memory allocs and use more std stuff in katemdi](https://invent.kde.org/utilities/kate/-/merge_requests/411)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KSyntaxHighlighting - [Rust syntax - Separate ControlFlow from Keywords](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/199)**<br />
Request authored by [Amaury Bouchra Pilet](https://invent.kde.org/abp) and merged after one day.

#### Week 20

- **Kate - [S&R: update highlight upon toggle of check state](https://invent.kde.org/utilities/kate/-/merge_requests/408)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after 2 days.

- **Kate - [Open Folder With: Kate](https://invent.kde.org/utilities/kate/-/merge_requests/409)**<br />
Request authored by [Thiago Sueto](https://invent.kde.org/thiagosueto) and merged at creation day.

- **Kate - [Clean up KateRunningInstanceInfo](https://invent.kde.org/utilities/kate/-/merge_requests/407)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [S&R: mark handling](https://invent.kde.org/utilities/kate/-/merge_requests/405)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after one day.

- **KTextEditor - [Add "Transpose Words" feature](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/153)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after one day.

- **Kate - [Implement LSP semantic tokens protocol](https://invent.kde.org/utilities/kate/-/merge_requests/398)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 3 days.

- **KTextEditor - [Fix on-the-fly spell checking with recent Qt](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/154)**<br />
Request authored by [Antonio Rojas](https://invent.kde.org/arojas) and merged at creation day.

- **KSyntaxHighlighting - [Use more target-centric cmake code](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/198)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged at creation day.

- **Kate - [lspclient: only unset cursor if previously set](https://invent.kde.org/utilities/kate/-/merge_requests/403)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged at creation day.

- **Kate - [lspclient updates](https://invent.kde.org/utilities/kate/-/merge_requests/402)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged at creation day.

- **KSyntaxHighlighting - [Port away from legacy LINK_PUBLIC & LINK_PRIVATE](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/197)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged at creation day.

#### Week 19

- **Kate - [Make quickopen sizing like other hud dialogs](https://invent.kde.org/utilities/kate/-/merge_requests/401)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Fix leak when staging lines / hunks](https://invent.kde.org/utilities/kate/-/merge_requests/399)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [fix quick open for untitled documents](https://invent.kde.org/utilities/kate/-/merge_requests/400)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [Add support for Vala language server](https://invent.kde.org/utilities/kate/-/merge_requests/397)**<br />
Request authored by [Pani Ram](https://invent.kde.org/ducksoft) and merged after one day.

- **KTextEditor - [Remove empty destructors & port to std::vector](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/151)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 6 days.

- **Kate - [gitblame: add action to toggle showing of blame info](https://invent.kde.org/utilities/kate/-/merge_requests/396)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after one day.

- **Kate - [quickopen: Use std::vector and remove QFileInfo](https://invent.kde.org/utilities/kate/-/merge_requests/393)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 4 days.

- **Kate - [Prevent storing diff files in recent files history](https://invent.kde.org/utilities/kate/-/merge_requests/395)**<br />
Request authored by [Méven Car](https://invent.kde.org/meven) and merged after 2 days.

- **Kate - [Git: Allow amending last commit message](https://invent.kde.org/utilities/kate/-/merge_requests/388)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 6 days.

- **KTextEditor - [Fixup registering text hint providers](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/152)**<br />
Request authored by [David Redondo](https://invent.kde.org/davidre) and merged at creation day.

- **KSyntaxHighlighting - [Fix perl qw< > highlighting and extend test](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/196)**<br />
Request authored by [Daniel Sonck](https://invent.kde.org/dsonck) and merged at creation day.

- **Kate - [Simplify and modernize KateViewSpace::saveConfig](https://invent.kde.org/utilities/kate/-/merge_requests/394)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [LSP: Improve tooltip](https://invent.kde.org/utilities/kate/-/merge_requests/392)**<br />
Request authored by [Méven Car](https://invent.kde.org/meven) and merged after 2 days.

#### Week 18

- **KTextEditor - [Use std::vector for MovingRanges and TextHintProvider](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/150)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Use std::vector in WordCounter](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/149)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KSyntaxHighlighting - [Update GAS syntax to highlight reg/instrs/jmp instrs](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/195)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 8 days.

- **Kate - [Project git: add icon for refresh action in the menu](https://invent.kde.org/utilities/kate/-/merge_requests/389)**<br />
Request authored by [Méven Car](https://invent.kde.org/meven) and merged at creation day.

- **Kate - [Project git tree : Fix single file context menus action](https://invent.kde.org/utilities/kate/-/merge_requests/390)**<br />
Request authored by [Méven Car](https://invent.kde.org/meven) and merged at creation day.

- **KTextEditor - [Theme combo box: Add a separator line after 'Automatic Selection'](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/147)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged at creation day.

- **Kate - [[addons/project/git/gitutils] Add missing include optional to make bsd happy](https://invent.kde.org/utilities/kate/-/merge_requests/387)**<br />
Request authored by [Ömer Fadıl Usta](https://invent.kde.org/usta) and merged at creation day.

- **Kate - [Fix branch change not reflected if projectbase != dotGit](https://invent.kde.org/utilities/kate/-/merge_requests/386)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [remove openheader plugin in favor of LSP](https://invent.kde.org/utilities/kate/-/merge_requests/384)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **Kate - [Fix leak in kategdbplugin](https://invent.kde.org/utilities/kate/-/merge_requests/383)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Misc quickopen fixes and adjustments](https://invent.kde.org/utilities/kate/-/merge_requests/381)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after one day.

- **Kate - [kate: show commandbar action in menu](https://invent.kde.org/utilities/kate/-/merge_requests/382)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged at creation day.

- **KTextEditor - [Fix leak in VariableLineEdit](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/146)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [project plugin: Use more forward decls](https://invent.kde.org/utilities/kate/-/merge_requests/380)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Remove dead code](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/145)**<br />
Request authored by [Hannah von Reth](https://invent.kde.org/vonreth) and merged at creation day.

#### Week 17

- **Kate - [lspclient: add a "Code Action" menu](https://invent.kde.org/utilities/kate/-/merge_requests/379)**<br />
Request authored by [Olivier Goffart](https://invent.kde.org/ogoffart) and merged at creation day.

- **KTextEditor - [less manual heap memory management in vimode](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/144)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KTextEditor - [Fix leak in KateCompletionWidget](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/143)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

### April 2021

#### Week 17

- **Kate - [Change terminal actions icons and text](https://invent.kde.org/utilities/kate/-/merge_requests/376)**<br />
Request authored by [Felipe Kinoshita](https://invent.kde.org/fhek) and merged after 3 days.

- **Kate - [Remove dead lsp semantic highlighting code](https://invent.kde.org/utilities/kate/-/merge_requests/378)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **KTextEditor - [remove filtering code, unused](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/140)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 4 days.

- **Kate - [Try to fix of a rare crash with Ctrl + Click in LSP](https://invent.kde.org/utilities/kate/-/merge_requests/377)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Fix leaks in KateConfigDialog](https://invent.kde.org/utilities/kate/-/merge_requests/372)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Adjust file history diff display](https://invent.kde.org/utilities/kate/-/merge_requests/373)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after one day.

- **Kate - [Make dependency on KI18n and KTextWidgets explicit](https://invent.kde.org/utilities/kate/-/merge_requests/374)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged at creation day.

- **KTextEditor - [Make KTextWidgets dependency explicit](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/141)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged at creation day.

#### Week 16

- **Kate - [Avoid choosing false compile_commands.json file](https://invent.kde.org/utilities/kate/-/merge_requests/371)**<br />
Request authored by [Alexander Lohnau](https://invent.kde.org/alex) and merged at creation day.

- **Kate - [project clazy tool: Fix clazy plugin in case build dir is outside of source](https://invent.kde.org/utilities/kate/-/merge_requests/370)**<br />
Request authored by [Alexander Lohnau](https://invent.kde.org/alex) and merged at creation day.

- **Kate - [addons: Add missing Qt5Concurrent to project and search](https://invent.kde.org/utilities/kate/-/merge_requests/369)**<br />
Request authored by [Andreas Sturmlechner](https://invent.kde.org/asturmlechner) and merged at creation day.

- **Kate - [Project closing](https://invent.kde.org/utilities/kate/-/merge_requests/355)**<br />
Request authored by [Krzysztof Stokop](https://invent.kde.org/mynewnickname) and merged after 35 days.

- **KTextEditor - [remove completion config - hidden since years](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/139)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KTextEditor - [Port KLineEdit to QLineEdit where the former's features aren't used](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/138)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after 2 days.

#### Week 15

- **KSyntaxHighlighting - [systemd unit: update to systemd v248](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/193)**<br />
Request authored by [Andreas Gratzer](https://invent.kde.org/andreasgr) and merged after 10 days.

- **kate-editor.org - [Use hugoi18n](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/31)**<br />
Request authored by [Phu Nguyen](https://invent.kde.org/phunh) and merged after 2 days.

- **KSyntaxHighlighting - [add support for Godot's gd-scripts](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/194)**<br />
Request authored by [Mario Aichinger](https://invent.kde.org/aichingm) and merged at creation day.

- **KTextEditor - [Fix dragging when folding stuff is around](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/136)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Don't insert null QActions](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/135)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged at creation day.

#### Week 14

- **KTextEditor - [Attempt fix crash on dragging](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/134)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Fix incremental selection-only search](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/128)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after 15 days.

- **Kate - [Bash lsp added](https://invent.kde.org/utilities/kate/-/merge_requests/366)**<br />
Request authored by [Daniele Scasciafratte](https://invent.kde.org/dscasciafratte) and merged after one day.

- **kate-editor.org - [Make better use of title and description](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/30)**<br />
Request authored by [Phu Nguyen](https://invent.kde.org/phunh) and merged after one day.

- **Kate - [Downgrade lsp semantic highlighting warnings](https://invent.kde.org/utilities/kate/-/merge_requests/367)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [Update Readme](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/192)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **Kate - [[applets/preview] Port from KMimeTypeTrader to KParts::PartLoader](https://invent.kde.org/utilities/kate/-/merge_requests/363)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged after one day.

- **Kate - [Fix some build warnings](https://invent.kde.org/utilities/kate/-/merge_requests/365)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

#### Week 13

- **Kate - [Fix shortcut display for multi-key shortcuts](https://invent.kde.org/utilities/kate/-/merge_requests/360)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 3 days.

- **Kate - [Quick open improvements](https://invent.kde.org/utilities/kate/-/merge_requests/314)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 30 days.

- **KTextEditor - [Fix sizing of completion tree](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/116)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 26 days.

- **KTextEditor - [Make completionWidget border less bright and more like Kate::LSPTooltip](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/132)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [Bash/Zsh: (#5) fix SubShell context that starts as an arithmetic evaluation](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/190)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Doxygen: fix Word style when the word is at the end of line](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/191)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **Kate - [Fix renamed file is not openable directly](https://invent.kde.org/utilities/kate/-/merge_requests/362)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Port from KToolInvocation to KIO::CommandLauncherJob](https://invent.kde.org/utilities/kate/-/merge_requests/361)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged after one day.

- **KSyntaxHighlighting - [LaTeX: allow Math env within another Math env](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/189)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **kate-editor.org - [Add the real blog about the git-blame plugin](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/29)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged after one day.

### March 2021

#### Week 13

- **kate-editor.org - [Add April 1 blog about git blame](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/28)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged at creation day.

- **Kate - [Change license from GPL 2.0+ to LGPL 2.0+](https://invent.kde.org/utilities/kate/-/merge_requests/359)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

#### Week 12

- **Kate - [Highlighting/themes documentation: remove references to Kate's flatpak package](https://invent.kde.org/utilities/kate/-/merge_requests/358)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged after one day.

- **KTextEditor - [Don't warn about unsaved changes when closing if blank and unsaved](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/131)**<br />
Request authored by [Nate Graham](https://invent.kde.org/ngraham) and merged after one day.

- **KTextEditor - [Use the text selection background color for the scrollbar minimap slider](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/129)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after one day.

- **KSyntaxHighlighting - [java: add assert and var keywords](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/187)**<br />
Request authored by [Denis Lisov](https://invent.kde.org/tanriol) and merged after one day.

- **KSyntaxHighlighting - [cmake.xml: Updates for CMake 3.20](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/188)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged at creation day.

- **Kate - [Use more icons in git plugin](https://invent.kde.org/utilities/kate/-/merge_requests/357)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Work/clazy tool](https://invent.kde.org/utilities/kate/-/merge_requests/353)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 6 days.

- **KSyntaxHighlighting - [PHP: add preg_last_error_msg() function and classes that was previously a resource](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/186)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Js: numeric separator ; fix Octal ; add new classes](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/185)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KTextEditor - [Remove unused ref to KateView* from KateAnnotationItemDelegate](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/127)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [avoid gaps in indentation line drawing](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/126)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **Kate - [ensure output view scrolls correctly](https://invent.kde.org/utilities/kate/-/merge_requests/356)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [Use some of the new git icons in the git integration](https://invent.kde.org/utilities/kate/-/merge_requests/349)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged after 5 days.

- **KTextEditor - [don't use F9 & F10 as shortcuts](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/125)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KTextEditor - [Remove unused includes + use more fwd decls](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/124)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KSyntaxHighlighting - [Make it possible to compile with Qt 6](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/184)**<br />
Request authored by [David Schulz](https://invent.kde.org/davidschulz) and merged at creation day.

- **KSyntaxHighlighting - [Php 8.0 and 8.1](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/183)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **Kate - [Fix file/folder input dialog label](https://invent.kde.org/utilities/kate/-/merge_requests/350)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 4 days.

- **KTextEditor - [Add basic touchscreen support](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/123)**<br />
Request authored by [Daniel Tang](https://invent.kde.org/danielt) and merged after 4 days.

- **KSyntaxHighlighting - [Fix #5 and add Path style with alternate value (${xx:-/path/})](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/182)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Markdown: add folding in sections](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/179)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged after one day.

- **KSyntaxHighlighting - [Python: add match and case keywords (Python 3.10)](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/181)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Disable spellchecking in Rust code](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/180)**<br />
Request authored by [William Wold](https://invent.kde.org/wmww) and merged after one day.

#### Week 11

- **Kate - [Use hybrid, deterministic + cursorPositionChanged for location history](https://invent.kde.org/utilities/kate/-/merge_requests/352)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 3 days.

- **Kate - [Tell user what to do instead of using sudo or kdesu](https://invent.kde.org/utilities/kate/-/merge_requests/354)**<br />
Request authored by [Nate Graham](https://invent.kde.org/ngraham) and merged after one day.

- **Kate - [Use colors from KColorScheme](https://invent.kde.org/utilities/kate/-/merge_requests/348)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **Kate - [Fix remote branch comparing](https://invent.kde.org/utilities/kate/-/merge_requests/347)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Add a widget to view projects TODOs/FIXMEs in project info toolview](https://invent.kde.org/utilities/kate/-/merge_requests/343)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **Kate - [Fix jump-forward action not enabled.](https://invent.kde.org/utilities/kate/-/merge_requests/351)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged at creation day.

- **KTextEditor - [[Vimode] Fix/Improve Paragraph and Sentence text objects](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/121)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after one day.

- **KTextEditor - [Port away from deprecated KCompletion signals](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/122)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after one day.

- **Kate - [git compare branches](https://invent.kde.org/utilities/kate/-/merge_requests/340)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **Kate - [Fix i18n in commit dialog and use breeze-danger-red color](https://invent.kde.org/utilities/kate/-/merge_requests/346)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Correct minor typo](https://invent.kde.org/utilities/kate/-/merge_requests/345)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KTextEditor - [Restrict horizontal range of cursor to avoid unintentionally wrapping.](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/120)**<br />
Request authored by [Pat Brown](https://invent.kde.org/patbrown) and merged at creation day.

- **KSyntaxHighlighting - [Fix BracketMatching color in Breeze Dark theme](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/178)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **Kate - [Put () in front when autocompleting functions](https://invent.kde.org/utilities/kate/-/merge_requests/342)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Make sure hover close button is visible on non-kde platforms](https://invent.kde.org/utilities/kate/-/merge_requests/341)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

#### Week 10

- **Kate - [Make filetree toolbar hidable](https://invent.kde.org/utilities/kate/-/merge_requests/339)**<br />
Request authored by [M  B ](https://invent.kde.org/brenneis) and merged at creation day.

- **Kate - [Viewspace settings on startup improvements](https://invent.kde.org/utilities/kate/-/merge_requests/295)**<br />
Request authored by [Marcell Fülöp](https://invent.kde.org/marekful) and merged after 15 days.

- **Kate - [use markdown directly in lsptooltip](https://invent.kde.org/utilities/kate/-/merge_requests/338)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Add default external tools C++, KDE and Qt API](https://invent.kde.org/utilities/kate/-/merge_requests/337)**<br />
Request authored by [Thiago Sueto](https://invent.kde.org/thiagosueto) and merged after one day.

- **Kate - [Show a close button on hover in Documents plugin](https://invent.kde.org/utilities/kate/-/merge_requests/282)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 18 days.

- **Kate - [Allow opening any directory from kate as project](https://invent.kde.org/utilities/kate/-/merge_requests/336)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Allow treeview to retain focus for keyboard navigation](https://invent.kde.org/utilities/kate/-/merge_requests/333)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Allow creating new file/folder in project](https://invent.kde.org/utilities/kate/-/merge_requests/332)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Parse groovy files with the parseCppSymbols() - same as Java](https://invent.kde.org/utilities/kate/-/merge_requests/331)**<br />
Request authored by [roman 149](https://invent.kde.org/romanonefournine) and merged after 2 days.

- **Kate - [Fix text not centered in commit dialog subject](https://invent.kde.org/utilities/kate/-/merge_requests/334)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Also update the indent/tab width limit to 200 in VariableLineEdit](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/118)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged at creation day.

- **Kate - [Use a single fixed view for viewing diffs](https://invent.kde.org/utilities/kate/-/merge_requests/335)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [turn on line numbers & modification markers](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/117)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **KSyntaxHighlighting - [Add support for fluent language files](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/176)**<br />
Request authored by [Fabian Wunsch](https://invent.kde.org/fabi) and merged after 4 days.

- **Kate - [Allow opening results in new doc + use full doc text if no selection](https://invent.kde.org/utilities/kate/-/merge_requests/329)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Avoid static QScrollBar instance](https://invent.kde.org/utilities/kate/-/merge_requests/330)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged at creation day.

- **KSyntaxHighlighting - [Add `findloc` Fortran function](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/177)**<br />
Request authored by [Jakub Benda](https://invent.kde.org/albandil) and merged at creation day.

- **Kate - [Update syntax highlighting documentation](https://invent.kde.org/utilities/kate/-/merge_requests/327)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **Kate - [Re-add history actions to mainwindow](https://invent.kde.org/utilities/kate/-/merge_requests/328)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Add option to keep spaces to the left of cursor when saving](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/112)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after 6 days.

- **Kate - [Fix histories for viewspaces are mixed](https://invent.kde.org/utilities/kate/-/merge_requests/326)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Manually get file's relative path to dir in while creating project](https://invent.kde.org/utilities/kate/-/merge_requests/325)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Disable search commands while searching/replacing](https://invent.kde.org/utilities/kate/-/merge_requests/322)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged after one day.

- **Kate - [Refactor projectplugin git ls-files](https://invent.kde.org/utilities/kate/-/merge_requests/324)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Allow cancelling git operations](https://invent.kde.org/utilities/kate/-/merge_requests/321)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

#### Week 09

- **Kate - [S&R: Add UseUnicodePropertiesOption to regexps](https://invent.kde.org/utilities/kate/-/merge_requests/323)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged at creation day.

- **KTextEditor - [Use unicode regex ;=)](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/113)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KTextEditor - [Fix unit test](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/115)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged at creation day.

- **KTextEditor - [Search: Enable Unicode support in QRegularExpression](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/114)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged at creation day.

- **KTextEditor - [[Vimode] Improve search wrapped hint](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/107)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after 12 days.

- **Kate - [Create Git process on demand](https://invent.kde.org/utilities/kate/-/merge_requests/320)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Configurable single / double click in git-status view](https://invent.kde.org/utilities/kate/-/merge_requests/316)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Dont depend on gitwidget process in StashDialog](https://invent.kde.org/utilities/kate/-/merge_requests/319)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Add shortcut for showing git commit info](https://invent.kde.org/utilities/kate/-/merge_requests/317)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged at creation day.

- **Kate - [Show modified lines count in git status](https://invent.kde.org/utilities/kate/-/merge_requests/311)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Use build icon for Build Selected Target](https://invent.kde.org/utilities/kate/-/merge_requests/315)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Move forward/backward actions to mainToolBar](https://invent.kde.org/utilities/kate/-/merge_requests/313)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged at creation day.

- **Kate - [Enable the back button when navigating forward.](https://invent.kde.org/utilities/kate/-/merge_requests/312)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged at creation day.

- **Kate - [Hide/show history jump buttons with the tab-bar](https://invent.kde.org/utilities/kate/-/merge_requests/310)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged at creation day.

- **Kate - [Allow to stash changes on Close](https://invent.kde.org/utilities/kate/-/merge_requests/228)**<br />
Request authored by [Méven Car](https://invent.kde.org/meven) and merged after 26 days.

- **Kate - [Add jumping back and forth between locations](https://invent.kde.org/utilities/kate/-/merge_requests/306)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 3 days.

- **Kate - [Git push: initial commit](https://invent.kde.org/utilities/kate/-/merge_requests/308)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **kate-editor.org - [Remove impressum](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/27)**<br />
Request authored by [Phu Nguyen](https://invent.kde.org/phunh) and merged at creation day.

- **Kate - [Implement Git show file history](https://invent.kde.org/utilities/kate/-/merge_requests/294)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 4 days.

- **Kate - [Use a more explicit description for the modified files dialog option](https://invent.kde.org/utilities/kate/-/merge_requests/293)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after 4 days.

- **Kate - [Search plugin: fix crash with multiline regexp search](https://invent.kde.org/utilities/kate/-/merge_requests/307)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged at creation day.

- **KTextEditor - [Dont use F11 for Toggle-Line-Numbers](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/111)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Improve performance for project loading](https://invent.kde.org/utilities/kate/-/merge_requests/305)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [More quickopen performance improvement](https://invent.kde.org/utilities/kate/-/merge_requests/304)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Refactor projectplugin quick-dialogs](https://invent.kde.org/utilities/kate/-/merge_requests/303)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **kate-editor.org - [Add config and README for a local development environment](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/26)**<br />
Request authored by [Phu Nguyen](https://invent.kde.org/phunh) and merged at creation day.

### February 2021

#### Week 08

- **Kate - [Kate outputview improvements](https://invent.kde.org/utilities/kate/-/merge_requests/302)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [brightscript: Add inline lambda call syntax](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/174)**<br />
Request authored by [Daniel Levin](https://invent.kde.org/dlevin) and merged after one day.

- **KSyntaxHighlighting - [Fix Markdown custom styling on Dracula theme](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/175)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **Kate - [Implement git line hunk staging / unstaging](https://invent.kde.org/utilities/kate/-/merge_requests/290)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 3 days.

- **Kate - [Add filtering + clear to output view](https://invent.kde.org/utilities/kate/-/merge_requests/301)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [use output view for external tools status things](https://invent.kde.org/utilities/kate/-/merge_requests/300)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [Let LSP plugin use the new global "Output" tool view](https://invent.kde.org/utilities/kate/-/merge_requests/299)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [Improve quickopen performance](https://invent.kde.org/utilities/kate/-/merge_requests/298)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Add goimports to default external tools](https://invent.kde.org/utilities/kate/-/merge_requests/297)**<br />
Request authored by [Wale Afolabi](https://invent.kde.org/adewale) and merged at creation day.

- **KTextEditor - [add Editor::font() to access user set editor font](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/110)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **kate-editor.org - [Make menu items detectable as active](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/25)**<br />
Request authored by [Phu Nguyen](https://invent.kde.org/phunh) and merged at creation day.

- **Kate - [Make sure ViewSpace config groups are preserved on exit in a session](https://invent.kde.org/utilities/kate/-/merge_requests/292)**<br />
Request authored by [Marcell Fülöp](https://invent.kde.org/marekful) and merged at creation day.

- **Kate - [Update git status on toolview shown](https://invent.kde.org/utilities/kate/-/merge_requests/291)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Refactor temporary file opening code](https://invent.kde.org/utilities/kate/-/merge_requests/289)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Work/git status visual imp](https://invent.kde.org/utilities/kate/-/merge_requests/285)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **KSyntaxHighlighting - [Enhance XML highlighter colors](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/173)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged after 2 days.

- **Kate - [Fix HUD Dialogs vertical text alignment](https://invent.kde.org/utilities/kate/-/merge_requests/287)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [git blame plugin](https://invent.kde.org/utilities/kate/-/merge_requests/266)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged after 5 days.

- **Kate - [Remember last triggered actions in command bar](https://invent.kde.org/utilities/kate/-/merge_requests/283)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Make sure we can highlight more than 2 parameters](https://invent.kde.org/utilities/kate/-/merge_requests/286)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Re-run clang-format](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/109)**<br />
Request authored by [Alexander Lohnau](https://invent.kde.org/alex) and merged at creation day.

- **KSyntaxHighlighting - [merge regexes generated by generate-cmake-syntax.py](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/171)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 3 days.

- **KSyntaxHighlighting - [fix endless state transitions](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/172)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 2 days.

#### Week 07

- **Kate - [Use QToolButtons for branch button and stage button](https://invent.kde.org/utilities/kate/-/merge_requests/279)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged at creation day.

- **Kate - [avoid error for non-git users](https://invent.kde.org/utilities/kate/-/merge_requests/280)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [Add prettier, rustfmt and gofmt to external tools](https://invent.kde.org/utilities/kate/-/merge_requests/278)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Implement clangd textDocument/switchSourceHeader](https://invent.kde.org/utilities/kate/-/merge_requests/277)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [project-plugin: Allow renaming files](https://invent.kde.org/utilities/kate/-/merge_requests/216)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 20 days.

- **Kate - [Move git to separate toolview](https://invent.kde.org/utilities/kate/-/merge_requests/276)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Git Fixes and improvements](https://invent.kde.org/utilities/kate/-/merge_requests/274)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Fix backtrace browser unable to open file](https://invent.kde.org/utilities/kate/-/merge_requests/273)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Implement git stash](https://invent.kde.org/utilities/kate/-/merge_requests/272)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Add option to preselect nothing when automatic completion is invoked](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/106)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after one day.

- **KTextEditor - [[Vimode] Ensure the % command (go to matching bracket) always works](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/105)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after 2 days.

- **KTextEditor - [Temporarily save unfinished search/replace text](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/94)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after 21 days.

- **KSyntaxHighlighting - [Indexer: request to merge some rules and add ^ with RegExpr column=0](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/170)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **Kate - [create LSP tooltip on demand, minimal invasive change](https://invent.kde.org/utilities/kate/-/merge_requests/271)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [Display tag if we are in a tag and master when new repo](https://invent.kde.org/utilities/kate/-/merge_requests/268)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [git discard and .gitignore](https://invent.kde.org/utilities/kate/-/merge_requests/269)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Allow signing-off commit messages using commit dialog](https://invent.kde.org/utilities/kate/-/merge_requests/270)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Make sure button rects look good on gnome](https://invent.kde.org/utilities/kate/-/merge_requests/267)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Use QTextLayout for better text rendering in BranchesDialog](https://invent.kde.org/utilities/kate/-/merge_requests/265)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [Update monokai & dracula for diff colors](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/169)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **kate-editor.org - [Improvements for dark background](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/24)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **Kate - [Show untracked files in project plugin](https://invent.kde.org/utilities/kate/-/merge_requests/264)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Migrate quickopen style delegate to use QTextLayout](https://invent.kde.org/utilities/kate/-/merge_requests/263)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Normalize color theme docs images, restore processibility and structure of the docs](https://invent.kde.org/utilities/kate/-/merge_requests/262)**<br />
Request authored by [Yuri Chornoivan](https://invent.kde.org/yurchor) and merged at creation day.

- **Kate - [Dont use QTextDocument in style delegates](https://invent.kde.org/utilities/kate/-/merge_requests/261)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Add documentation for Color Themes](https://invent.kde.org/utilities/kate/-/merge_requests/256)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged after one day.

- **KTextEditor - [Work/deprecate redundant signals](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/103)**<br />
Request authored by [Alexander Lohnau](https://invent.kde.org/alex) and merged after 7 days.

- **kate-editor.org - [Fix and improve the get-it page](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/23)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged after 3 days.

- **Kate - [Dont use project baseDir for git operations](https://invent.kde.org/utilities/kate/-/merge_requests/257)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Git commit and plugin improvements](https://invent.kde.org/utilities/kate/-/merge_requests/259)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [ignore esc the same way for projects plugin toolviews](https://invent.kde.org/utilities/kate/-/merge_requests/258)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [[proposal] split project settings into shared and local parts](https://invent.kde.org/utilities/kate/-/merge_requests/254)**<br />
Request authored by [Héctor Mesa Jiménez](https://invent.kde.org/hectorm) and merged after one day.

- **KTextEditor - [Fix i18n](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/104)**<br />
Request authored by [Albert Astals Cid](https://invent.kde.org/aacid) and merged at creation day.

- **Kate - [Add git support](https://invent.kde.org/utilities/kate/-/merge_requests/255)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [Indexer: check for duplicate InclusionRules](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/168)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 2 days.

#### Week 06

- **Kate - [purge hints for python plugins](https://invent.kde.org/utilities/kate/-/merge_requests/253)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KSyntaxHighlighting - [Indexer: suggest RegExpr to RangeDetect/DetectIdentifier and check additionalDeliminator/wordDelimiters](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/167)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **kate-editor.org - [Make sure we run with the latest version of the theme](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/22)**<br />
Request authored by [Carl Schwan](https://invent.kde.org/carlschwan) and merged after one day.

- **Kate - [require KF 5.79 & cleanup](https://invent.kde.org/utilities/kate/-/merge_requests/252)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [[KateMwModOnHdDialog] Only enable buttons when at least one file is checked](https://invent.kde.org/utilities/kate/-/merge_requests/251)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged at creation day.

- **KSyntaxHighlighting - [Bash/Zsh: fix comment and = style in VarName ; highlight parameter for getopts and let](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/166)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **kate-editor.org - [Improve i18n](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/21)**<br />
Request authored by [Phu Nguyen](https://invent.kde.org/phunh) and merged after one day.

- **Kate - [cleanup KTextEditor signals](https://invent.kde.org/utilities/kate/-/merge_requests/245)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **Kate - [Ensure we always have readable colors in tooltip](https://invent.kde.org/utilities/kate/-/merge_requests/250)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Make contextmenu work inside lsp tooltip](https://invent.kde.org/utilities/kate/-/merge_requests/246)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Move checkout button above the project tree view](https://invent.kde.org/utilities/kate/-/merge_requests/248)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Remove FilterByPath/ByName in Quickopen](https://invent.kde.org/utilities/kate/-/merge_requests/247)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **kate-editor.org - [Fix translations](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/20)**<br />
Request authored by [Phu Nguyen](https://invent.kde.org/phunh) and merged at creation day.

- **Kate - [konsole: Hide toolview on Esc keypress](https://invent.kde.org/utilities/kate/-/merge_requests/237)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **Kate - [Lsp Tooltip improvements](https://invent.kde.org/utilities/kate/-/merge_requests/244)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Make tooltip a bit less annoying](https://invent.kde.org/utilities/kate/-/merge_requests/243)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [LSP: Implement a new UI for tooltips](https://invent.kde.org/utilities/kate/-/merge_requests/242)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **kate-editor.org - [Allow translators to add language name for the language chooser](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/17)**<br />
Request authored by [Carl Schwan](https://invent.kde.org/carlschwan) and merged after 3 days.

- **kate-editor.org - [Fix typo: then -> than](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/19)**<br />
Request authored by [Yuri Chornoivan](https://invent.kde.org/yurchor) and merged at creation day.

- **Kate - [Fix branch fetch from git](https://invent.kde.org/utilities/kate/-/merge_requests/241)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [LSP: Allow disabling signature help](https://invent.kde.org/utilities/kate/-/merge_requests/235)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [LSP: Highlight active function argument during autocomplete](https://invent.kde.org/utilities/kate/-/merge_requests/240)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Allow users to enable indexing by clicking button](https://invent.kde.org/utilities/kate/-/merge_requests/239)**<br />
Request authored by [Alexander Lohnau](https://invent.kde.org/alex) and merged at creation day.

- **Kate - [Checkout new branch and Checkout new branch from branch](https://invent.kde.org/utilities/kate/-/merge_requests/238)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **kate-editor.org - [Use shared theming](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/18)**<br />
Request authored by [Carl Schwan](https://invent.kde.org/carlschwan) and merged after 2 days.

- **Kate - [Fix command-bar unable to show shortcuts with '+'](https://invent.kde.org/utilities/kate/-/merge_requests/232)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [kateproject: Hide tool-infoview on Esc key press like other bottom toolviews](https://invent.kde.org/utilities/kate/-/merge_requests/236)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Change branch & reload if a branch was changed outside Kate](https://invent.kde.org/utilities/kate/-/merge_requests/234)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Do not create new tabs for Middle/Right button double clicks](https://invent.kde.org/utilities/kate/-/merge_requests/233)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged at creation day.

- **KSyntaxHighlighting - [C++ Highlighting: add some QVariant-related variants](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/162)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after 7 days.

- **Kate - [git checkout initial implementation](https://invent.kde.org/utilities/kate/-/merge_requests/227)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **Kate - [Fix font sizes for windows](https://invent.kde.org/utilities/kate/-/merge_requests/231)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [create quick open only when needed](https://invent.kde.org/utilities/kate/-/merge_requests/230)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KTextEditor - [[Vimode] Do not remove empty lines when reformatting](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/102)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged at creation day.

#### Week 05

- **Kate - [Quickopen performance improvements](https://invent.kde.org/utilities/kate/-/merge_requests/226)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [[Vimode] Display message when search is wrapped](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/99)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after 5 days.

- **KSyntaxHighlighting - [PHP: fix string, number and folding region ; refactor styles](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/165)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [avoid spell check for SPDX identifiers and stuff](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/164)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **Kate - [Improve the color picker plugin](https://invent.kde.org/utilities/kate/-/merge_requests/190)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after 17 days.

- **KTextEditor - [Fix a corner case with camel cursor](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/101)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Implement camel cursor movement](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/92)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 12 days.

- **KTextEditor - [Enable Smart Copy Cut by default](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/100)**<br />
Request authored by [Alexander Lohnau](https://invent.kde.org/alex) and merged after 2 days.

- **KSyntaxHighlighting - [Breeze Light: new color for Control Flow](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/163)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Indexer: check unreachable elements](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/160)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 7 days.

- **Kate - [Update fuzzy matching code](https://invent.kde.org/utilities/kate/-/merge_requests/225)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Fix ctrl-click for #include with quotes](https://invent.kde.org/utilities/kate/-/merge_requests/223)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **kate-editor.org - [i18n cleanup and fixes](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/16)**<br />
Request authored by [Ada](https://invent.kde.org/adasauce) and merged at creation day.

- **Kate - [Allow to open a project using a command line parameter](https://invent.kde.org/utilities/kate/-/merge_requests/219)**<br />
Request authored by [Alexander Lohnau](https://invent.kde.org/alex) and merged after 4 days.

- **Kate - [Fix leak in quick dialogs](https://invent.kde.org/utilities/kate/-/merge_requests/222)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **kate-editor.org - [Markdown formatting for the kate-vi-mode page](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/15)**<br />
Request authored by [Ada](https://invent.kde.org/adasauce) and merged at creation day.

- **Kate - [Multi-threaded file collection for search in folder](https://invent.kde.org/utilities/kate/-/merge_requests/221)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

### January 2021

#### Week 04

- **Kate - [Do not restore Projects tool view when no project exists](https://invent.kde.org/utilities/kate/-/merge_requests/218)**<br />
Request authored by [Alexander Lohnau](https://invent.kde.org/alex) and merged at creation day.

- **Kate - [Also set selected foreground in semantic highlighting](https://invent.kde.org/utilities/kate/-/merge_requests/213)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Show dir selection dialog for ProjectConfig->index](https://invent.kde.org/utilities/kate/-/merge_requests/217)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [quickopen: construct context menu on demand](https://invent.kde.org/utilities/kate/-/merge_requests/215)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Don't expand semantic highlighting ranges to prevent incorrect highlighting while typing](https://invent.kde.org/utilities/kate/-/merge_requests/214)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Port away from Qt's forever](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/98)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged at creation day.

- **KSyntaxHighlighting - [highlighter_benchmark: ignore .clang-format](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/161)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **Kate - [Use atomic variables for lock free access](https://invent.kde.org/utilities/kate/-/merge_requests/212)**<br />
Request authored by [Anthony Fieroni](https://invent.kde.org/anthonyfieroni) and merged at creation day.

- **KTextEditor - [Increase maximum indentation width to 200](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/97)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged at creation day.

- **KTextEditor - [[Vimode] Do not switch view when changing case (~ command)](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/96)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged at creation day.

- **Kate - [improve threading](https://invent.kde.org/utilities/kate/-/merge_requests/211)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [Work/fixes](https://invent.kde.org/utilities/kate/-/merge_requests/210)**<br />
Request authored by [Alexander Lohnau](https://invent.kde.org/alex) and merged at creation day.

- **Kate - [Work/clang format](https://invent.kde.org/utilities/kate/-/merge_requests/207)**<br />
Request authored by [Alexander Lohnau](https://invent.kde.org/alex) and merged after one day.

- **KTextEditor - [Add test case for range accessed after deletion](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/95)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [Less vibrant operator color for Breeze themes](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/159)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **Kate - [Search disk files multithreaded](https://invent.kde.org/utilities/kate/-/merge_requests/208)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged at creation day.

- **Kate - [Clean up command bar code](https://invent.kde.org/utilities/kate/-/merge_requests/209)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Konsole plugin: Sync terminal path when the current document's url changes (e.g. first document open, save as)](https://invent.kde.org/utilities/kate/-/merge_requests/205)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after one day.

- **KTextEditor - [Only show "There are no more chars for next bookmark" error when in vimode](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/93)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged at creation day.

- **Kate - [LSP: Fix Ctrl + click underline highlighting broken in #include context](https://invent.kde.org/utilities/kate/-/merge_requests/203)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [LSP: Fix highlighting not cleared after commenting code out](https://invent.kde.org/utilities/kate/-/merge_requests/202)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Update Haskell for settings.json](https://invent.kde.org/utilities/kate/-/merge_requests/201)**<br />
Request authored by [hololeap hololeap](https://invent.kde.org/hololeap) and merged at creation day.

- **Kate - [Add Workaround for KateTabBar sometimes disappearing on session load](https://invent.kde.org/utilities/kate/-/merge_requests/199)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after one day.

- **KTextEditor - [Use new signal/slot syntax](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/89)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after one day.

- **KTextEditor - [Retain replacement text as long as the power search bar is not closed](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/90)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged at creation day.

- **KTextEditor - [[vimode] Fix motion to matching item off-by-one](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/91)**<br />
Request authored by [Arusekk ­­](https://invent.kde.org/arusekk) and merged at creation day.

#### Week 03

- **Kate - [Fix color loading for LSP client plugin](https://invent.kde.org/utilities/kate/-/merge_requests/198)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [KateBookMarks: modernise code base](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/88)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged at creation day.

- **KTextEditor - [Fix alpha channel being ignored when reading from config interface](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/87)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Use the new theme function to provide better semantic highlighting](https://invent.kde.org/utilities/kate/-/merge_requests/195)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Fixed colors for parameters for some themes](https://invent.kde.org/utilities/kate/-/merge_requests/197)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Work/ahmad/signal syntax 2](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/82)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after 3 days.

- **Kate - [Add missing HTML tag](https://invent.kde.org/utilities/kate/-/merge_requests/196)**<br />
Request authored by [Yunhe Guo](https://invent.kde.org/guoyunhe) and merged at creation day.

- **KTextEditor - [Improve the opening bracket preview](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/83)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after one day.

- **KTextEditor - [CommandRangeExpressionParser: port QRegExp to QRegularExpression](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/85)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged at creation day.

- **KTextEditor - [avoid duplicated highlighting ranges that kill ARGB rendering](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/86)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KSyntaxHighlighting - [Atom Light, Breeze Dark/Light: new color for Operator ; Breeze Light: new color for ControlFlow](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/154)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 3 days.

- **KSyntaxHighlighting - [Improve readability of Solarized dark theme](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/157)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [remove unnecessary captures with a dynamic rule](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/156)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [add const overloads for some accessors](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/158)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **kate-editor.org - [Fix translations](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/14)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **Kate - [Use editor theme colors to theme Search&Replace plugin](https://invent.kde.org/utilities/kate/-/merge_requests/194)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Fix extra inverted comma on autocomplete with autobrackets](https://invent.kde.org/utilities/kate/-/merge_requests/192)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [expose the global KSyntaxHighlighting::Repository read-only](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/84)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KTextEditor - [Correct indentation bug when line contains "for" or "else".](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/79)**<br />
Request authored by [Francis Laniel](https://invent.kde.org/eiffel) and merged after 2 days.

- **kate-editor.org - [Fix button translation in Get It page](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/13)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **kate-editor.org - [Fix minor typo: This -> These](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/12)**<br />
Request authored by [Yuri Chornoivan](https://invent.kde.org/yurchor) and merged at creation day.

- **Kate - [try to ignore tabbar shortcuts](https://invent.kde.org/utilities/kate/-/merge_requests/189)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **Kate - [Draw tiny buttons for shortcuts instead of just text](https://invent.kde.org/utilities/kate/-/merge_requests/188)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Add a couple of extra checks for sanity and remove commented code](https://invent.kde.org/utilities/kate/-/merge_requests/191)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Fix compilation warning](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/81)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged at creation day.

- **KSyntaxHighlighting - [Bash, Zsh: fix cmd;; in a case](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/155)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **kate-editor.org - [Improve Get It page](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/11)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **Kate - [Add documentation for copy, cut and paste view functions.](https://invent.kde.org/utilities/kate/-/merge_requests/187)**<br />
Request authored by [Francis Laniel](https://invent.kde.org/eiffel) and merged at creation day.

- **Kate - [Draw rectancle inline color notes including a hover effect](https://invent.kde.org/utilities/kate/-/merge_requests/181)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged after 2 days.

- **KTextEditor - [CMake: Use configure_file() to ensure noop incremental builds](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/80)**<br />
Request authored by [Daan De Meyer](https://invent.kde.org/daandemeyer) and merged at creation day.

- **Kate - [Add a quick-open style command bar](https://invent.kde.org/utilities/kate/-/merge_requests/179)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Use erase/remove_if instead of loop to avoid massive random vector element removal costs](https://invent.kde.org/utilities/kate/-/merge_requests/185)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Optimize search some more and reduce memory usage](https://invent.kde.org/utilities/kate/-/merge_requests/184)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Tweak fuzzy matcher to score sequential matches a bit higher for kate command bar](https://invent.kde.org/utilities/kate/-/merge_requests/183)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Allow 'QMenu' inside the command bar](https://invent.kde.org/utilities/kate/-/merge_requests/182)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Correct an indentation bug.](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/70)**<br />
Request authored by [Francis Laniel](https://invent.kde.org/eiffel) and merged after 3 days.

#### Week 02

- **Kate - [Use an abstract item model for search and replace matches](https://invent.kde.org/utilities/kate/-/merge_requests/180)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged at creation day.

- **Kate - [Unbreak quickopen's reslect first](https://invent.kde.org/utilities/kate/-/merge_requests/178)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Avoid module-wide includes (QtTest and QtDBus)](https://invent.kde.org/utilities/kate/-/merge_requests/177)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged at creation day.

- **KTextEditor - [Work/clang format stuff](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/78)**<br />
Request authored by [Alexander Lohnau](https://invent.kde.org/alex) and merged at creation day.

- **KTextEditor - [paint the small gap in selection color, if previous line end is in selection](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/77)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KTextEditor - [Fix indent for when pressing enter and the function param has a comma at the end](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/76)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [avoid full line selection painting, more in line with other editors](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/75)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KTextEditor - [Port remaining uses of QRegExp in src/vimode into QRegularExpression](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/67)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after 4 days.

- **KTextEditor - [Include QTest instead of QtTest (to avoid module-wide includes)](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/74)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged at creation day.

- **KSyntaxHighlighting - [email.xml: Detect nested comments and escaped characters](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/153)**<br />
Request authored by [Martin Walch](https://invent.kde.org/martinwalch) and merged at creation day.

- **KSyntaxHighlighting - [Update atom light to use alpha colors](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/152)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Respect alpha colors while exporting html](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/73)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Alpha colors support: Config layer](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/72)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Current line highlighting over full icon border](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/71)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **KSyntaxHighlighting - [remap some Symbol and Operator styles to dsOperator](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/150)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Bash: fix } in ${!xy*} and more Parameter Expansion Operator (# in ${#xy} ;...](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/151)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **Kate - [Dont override the cursor for whole application when building](https://invent.kde.org/utilities/kate/-/merge_requests/175)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Enable alpha channel for editor colors](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/69)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [Allow using the alpha channel in theme colors](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/148)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 3 days.

- **KSyntaxHighlighting - [Update monokai colors and fix incorrect blue](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/149)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KSyntaxHighlighting - [Update Kconfig highlighter to Linux 5.9](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/144)**<br />
Request authored by [Martin Walch](https://invent.kde.org/martinwalch) and merged after 6 days.

- **Kate - [Allow / separated pattern to filter same filename across different directories](https://invent.kde.org/utilities/kate/-/merge_requests/176)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Fix current line highlight has a tiny gap at the beginning](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/68)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Fix crash due to invalid pointer when jumping using CtrlClick](https://invent.kde.org/utilities/kate/-/merge_requests/174)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Fix compilation on Kubuntu 20.04](https://invent.kde.org/utilities/kate/-/merge_requests/173)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged at creation day.

- **KTextEditor - [[Vimode] Do not skip folded range when the motion lands inside](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/64)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after 4 days.

- **KTextEditor - [Port the (trivial) uses of QRegExp in normal vimode to QRegularExpression](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/62)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after 6 days.

- **KSyntaxHighlighting - [C++: fix us suffix](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/147)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Bash: fix #5: $ at the end of a double quoted string](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/146)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **Kate - [LSP Client: Jump to Declaration / Definition on Control-click](https://invent.kde.org/utilities/kate/-/merge_requests/172)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [VHDL: fix function, procedure, type range/units and other improvements](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/145)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

#### Week 01

- **Kate - [Improve the ctags-GotoSymbol plugin](https://invent.kde.org/utilities/kate/-/merge_requests/171)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Use fuzzy filter in kate lspclientplugin symbol view](https://invent.kde.org/utilities/kate/-/merge_requests/170)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [expose the KSyntaxHighlighting theme](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/66)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KTextEditor - [add configChanged to KTextEditor::Editor, too, for global configuration changes](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/65)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KSyntaxHighlighting - [Update breeze-dark.theme](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/143)**<br />
Request authored by [Andrey Karepin](https://invent.kde.org/akarepin) and merged after one day.

- **Kate - [Improve the UI for quick-open](https://invent.kde.org/utilities/kate/-/merge_requests/168)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Dont show absouloute path for files in quick-open](https://invent.kde.org/utilities/kate/-/merge_requests/167)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Use fuzzy filter in kateprojectplugin](https://invent.kde.org/utilities/kate/-/merge_requests/166)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Fix html tags appearing in ctags-goto-symbol](https://invent.kde.org/utilities/kate/-/merge_requests/163)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 2 days.

- **Kate - [Improve text for goto symbol actions](https://invent.kde.org/utilities/kate/-/merge_requests/165)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [allow to access & alter the current theme via config interface](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/63)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [Support fuzzy matching for quick open/...](https://invent.kde.org/utilities/kate/-/merge_requests/140)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 20 days.

- **KTextEditor - [Minor QRegularExpression optimisations](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/60)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged at creation day.

- **KSyntaxHighlighting - [Raku: #7: fix symbols that starts with Q](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/141)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

#### Week 53

- **KTextEditor - [Add KTextEditor::LineRange](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/57)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged after one day.

- **KTextEditor - [Cursor, Range: Add fromString(QStringView) overload](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/58)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged after one day.

- **KTextEditor - [Allow "Dynamic Word Wrap Align Indent" to be disabled](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/59)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged at creation day.

- **KSyntaxHighlighting - [Add Oblivion color scheme from GtkSourceView/Pluma/gEdit](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/132)**<br />
Request authored by [ntninja](https://invent.kde.org/ntninja) and merged after 7 days.

- **Kate - [batch matches together to avoid massive costs for repeated model update](https://invent.kde.org/utilities/kate/-/merge_requests/159)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **Kate - [UrlInfo: handle filenames starting with ':'](https://invent.kde.org/utilities/kate/-/merge_requests/134)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after 21 days.

- **Kate - [Cache colors outside the loop](https://invent.kde.org/utilities/kate/-/merge_requests/162)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [[Vimode]Fix search inside fold ranges](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/53)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after 6 days.

- **KTextEditor - [[Vimode] Fix Macro Completion Replay](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/52)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after 8 days.

- **KTextEditor - [Improve search performance](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/56)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [avoid stall at end of search if not expand all set + show intermediate match count](https://invent.kde.org/utilities/kate/-/merge_requests/161)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [allow to ignore binary files for project based search, too](https://invent.kde.org/utilities/kate/-/merge_requests/158)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [Dont reconstruct the same i18n string everytime without any reason](https://invent.kde.org/utilities/kate/-/merge_requests/157)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.



## Accepted Merge Requests of 2020

- 87 patches for [Kate](https://invent.kde.org/utilities/kate/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 49 patches for [KTextEditor](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 132 patches for [KSyntaxHighlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 2 patches for [kate-editor.org](https://invent.kde.org/websites/kate-editor-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)

### December 2020

#### Week 53

- **Kate - [Update replace display results to match the search results](https://invent.kde.org/utilities/kate/-/merge_requests/156)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [External Tools: Trim executable, name and command name](https://invent.kde.org/utilities/kate/-/merge_requests/155)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged at creation day.

- **Kate - [External Tools: Track mimetype changes to enable/disable actions](https://invent.kde.org/utilities/kate/-/merge_requests/154)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged at creation day.

- **Kate - [Improve search performance by reducing the amount of work](https://invent.kde.org/utilities/kate/-/merge_requests/153)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Variable expansion: Add support for %{Document:Variable:<name>}](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/55)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged after 2 days.

- **KSyntaxHighlighting - [C++ Highlighting: QOverload and co.](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/139)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged at creation day.

- **KSyntaxHighlighting - [Fix labels beginning with period not being highlighted in GAS](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/137)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Show the line text in search results instead of line no & col](https://invent.kde.org/utilities/kate/-/merge_requests/149)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 3 days.

- **Kate - [Changed Go Language Server](https://invent.kde.org/utilities/kate/-/merge_requests/150)**<br />
Request authored by [Marvin Ahlgrimm](https://invent.kde.org/treagod) and merged after 2 days.

- **KSyntaxHighlighting - [C++ highlighting: add qGuiApp macro](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/138)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged at creation day.

- **KSyntaxHighlighting - [Improve dracula theme](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/136)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [Bash, Zsh: ! with if, while, until ; Bash: pattern style for ${var,patt} and ${var^patt}](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/135)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **Kate - [LSP: Add support for textDocument/implementation](https://invent.kde.org/utilities/kate/-/merge_requests/152)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Add documetnation about external tools plugin](https://invent.kde.org/utilities/kate/-/merge_requests/151)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged at creation day.

- **KTextEditor - [Show the dragged text when dragging](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/54)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KSyntaxHighlighting - [fix #5: Bash, Zsh: comments within array](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/134)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Cucumber feature syntax](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/133)**<br />
Request authored by [Samu Voutilainen](https://invent.kde.org/svoutilainen) and merged at creation day.

- **KSyntaxHighlighting - [Zsh: fix brace expansion in a command](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/131)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

#### Week 52

- **Kate - [Add quick open go-to symbol support in CTags plugin](https://invent.kde.org/utilities/kate/-/merge_requests/145)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Highlight documentation: new attributes for some rules: weakDeliminator and additionalDeliminator](https://invent.kde.org/utilities/kate/-/merge_requests/147)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [add weakDeliminator and additionalDeliminator with keyword, WordDetect, Int,...](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/111)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 29 days.

- **KSyntaxHighlighting - [some fixes for Bash many fixes and improvements for Zsh](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/129)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **KSyntaxHighlighting - [Indexer: reset currentKeywords and currentContext when opening a new definition](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/130)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **KTextEditor - [Fix detach in TextRange::fixLookup()](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/51)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [Don't highlight currentLine if there is an overlapping selection](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/50)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KSyntaxHighlighting - [Update Monokai theme](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/127)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KSyntaxHighlighting - [Verify the correctness/presence of custom-styles in themes](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/128)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **Kate - [Add Inline Color Picker Plugin](https://invent.kde.org/utilities/kate/-/merge_requests/133)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after 10 days.

- **KSyntaxHighlighting - [Add GitHub Dark/Light themes](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/126)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

#### Week 51

- **Kate - [Add StartupWMClass parameters to desktop files](https://invent.kde.org/utilities/kate/-/merge_requests/144)**<br />
Request authored by [Markus Slopianka](https://invent.kde.org/markuss) and merged at creation day.

- **KTextEditor - [KateRegExpSearch: fix logic when adding '\n' between range lines](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/49)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after one day.

- **KSyntaxHighlighting - [Fix: Backport changes by Andreas Gratzer in MR!113 to SPDX syntax template](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/119)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged after 13 days.

- **KSyntaxHighlighting - [Add Atom One dark/light themes](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/125)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KSyntaxHighlighting - [Fix monokai attribute & operator color](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/124)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [improve replace speed](https://invent.kde.org/utilities/kate/-/merge_requests/142)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **KSyntaxHighlighting - [Add Monokai color theme](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/123)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Do not set deprecated CustomName property in knsrc file](https://invent.kde.org/utilities/kate/-/merge_requests/141)**<br />
Request authored by [Alexander Lohnau](https://invent.kde.org/alex) and merged after one day.

- **KSyntaxHighlighting - [CMake: Add missed 3.19 variables and some new added in 3.19.2](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/122)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged at creation day.

- **KTextEditor - [add an action to trigger copy & paste as one action](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/48)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 2 days.

- **Kate - [Select first item in quick-open if only one item in list](https://invent.kde.org/utilities/kate/-/merge_requests/139)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Add item to tree after setting attributes](https://invent.kde.org/utilities/kate/-/merge_requests/138)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **Kate - [Improve search performance by a huge margin](https://invent.kde.org/utilities/kate/-/merge_requests/136)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

- **KTextEditor - [Fix some clazy warnings](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/46)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after one day.

- **KTextEditor - [feat: add text-wrap action icon for Dynamic Word Wrap](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/47)**<br />
Request authored by [Gary Wang](https://invent.kde.org/garywang) and merged at creation day.

- **Kate - [Make Ctrl+W shortcut work in Ctrl+Tab tabswitcher](https://invent.kde.org/utilities/kate/-/merge_requests/135)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged at creation day.

#### Week 50

- **KTextEditor - [Undo indent in one step](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/45)**<br />
Request authored by [Waqar Ahmed](https://invent.kde.org/waqar) and merged after 3 days.

- **KTextEditor - [Pass parent to Q*Layout ctor instead of calling setLayout()](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/44)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after 9 days.

- **KSyntaxHighlighting - [Improvements of Java, Scala, Kotlin and Groovy](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/121)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **Kate - [Remove unused include](https://invent.kde.org/utilities/kate/-/merge_requests/132)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged after one day.

- **KSyntaxHighlighting - [fix #5: && and || in a subcontext and fix function name pattern](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/120)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [add capture attribute to the RegExpr rule. capture=0 (default) disables capture groups](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/118)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 4 days.

#### Week 49

- **KSyntaxHighlighting - [Bash: add (...), ||, && in [[ ... ]] ; add backquote in [ ... ] and [[ ... ]]](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/117)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

- **Kate - [lspclient: switch to a maintained javascript language server](https://invent.kde.org/utilities/kate/-/merge_requests/131)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after one day.

- **KSyntaxHighlighting - [fix dependencies of the generated files](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/116)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [indexer: load all xml files in memory to simplify and fix the checkers](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/107)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 11 days.

- **KSyntaxHighlighting - [SPDX comments: fix broken highlighting for multi-line comments](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/113)**<br />
Request authored by [Andreas Gratzer](https://invent.kde.org/andreasgr) and merged after one day.

- **KSyntaxHighlighting - [relaunch syntax generators when the source file is modified](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/114)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [C++ highlighting: update to Qt 5.15](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/115)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged at creation day.

### November 2020

#### Week 49

- **KSyntaxHighlighting - [systemd unit: update to systemd v247](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/112)**<br />
Request authored by [Andreas Gratzer](https://invent.kde.org/andreasgr) and merged at creation day.

#### Week 48

- **Kate - [filetree addon: make methods const where possible](https://invent.kde.org/utilities/kate/-/merge_requests/128)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after 2 days.

- **Kate - [ui.rc files: consistenly use <gui> instead of deprecated <kpartgui>](https://invent.kde.org/utilities/kate/-/merge_requests/129)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged after one day.

- **Kate - [Remove unused includes](https://invent.kde.org/utilities/kate/-/merge_requests/130)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged after one day.

- **KSyntaxHighlighting - [ILERPG: simplify and test](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/110)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 2 days.

- **Kate - [filetree addon: fix crash in filetree_model_test](https://invent.kde.org/utilities/kate/-/merge_requests/126)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged at creation day.

- **KSyntaxHighlighting - [Zsh, Bash, Fish, Tcsh: add truncate and tsort in unixcommand keywords](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/108)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Latex: some math environments can be nested](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/109)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

#### Week 47

- **Kate - [Port some KComboBox usage to QComboBox](https://invent.kde.org/utilities/kate/-/merge_requests/125)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged at creation day.

- **Kate - [Require KF 5.68](https://invent.kde.org/utilities/kate/-/merge_requests/124)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged after one day.

- **KTextEditor - [Port KComboBox to QComboBox](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/43)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged at creation day.

- **Kate - [Properly unset EDITOR env var, instead of setting it as empty.](https://invent.kde.org/utilities/kate/-/merge_requests/123)**<br />
Request authored by [Thomas Friedrichsmeier](https://invent.kde.org/tfry) and merged after one day.

- **KSyntaxHighlighting - [Bash: many fixes and improvements](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/105)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 3 days.

- **KSyntaxHighlighting - [add --syntax-trace=stackSize](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/106)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 3 days.

#### Week 46

- **Kate - [Port to new KPluginMetaData-based KParts API](https://invent.kde.org/utilities/kate/-/merge_requests/121)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged after 2 days.

- **KSyntaxHighlighting - [php.xml: Fix matching endforeach](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/104)**<br />
Request authored by [Fabian Vogt](https://invent.kde.org/fvogt) and merged after 2 days.

- **KSyntaxHighlighting - [Move bestThemeForApplicationPalette from KTextEditor here](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/103)**<br />
Request authored by [Xaver Hugl](https://invent.kde.org/zamundaaa) and merged after one day.

- **KTextEditor - [Improve the automatic theme selection](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/41)**<br />
Request authored by [Xaver Hugl](https://invent.kde.org/zamundaaa) and merged after one day.

- **Kate - [Port away from deprecated Qt::MidButton](https://invent.kde.org/utilities/kate/-/merge_requests/120)**<br />
Request authored by [Robert-André Mauchin](https://invent.kde.org/rmauchin) and merged at creation day.

- **KSyntaxHighlighting - [Update README](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/101)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **kate-editor.org - [Improve get-it page](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/9)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **kate-editor.org - [Update images in about-kate page](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/10)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

#### Week 45

- **KSyntaxHighlighting - [alert.xml: Add `NOQA` yet another popular alert in source code](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/100)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged at creation day.

- **KSyntaxHighlighting - [The "compact" core function is missing.](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/99)**<br />
Request authored by [Vincenzo Buttazzo](https://invent.kde.org/buttazzo) and merged after one day.

- **KSyntaxHighlighting - [The position:sticky value is missing.](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/97)**<br />
Request authored by [Vincenzo Buttazzo](https://invent.kde.org/buttazzo) and merged at creation day.

- **KTextEditor - [[Vimode] Prevent search box from disappearing](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/40)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged at creation day.

- **KSyntaxHighlighting - [Feature: Add the `comments.xml` as an umbrella syntax for various comment kinds](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/96)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged after one day.

#### Week 44

- **KSyntaxHighlighting - [Feature: Add `SPDX-Comments` syntax generator and XML](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/90)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged after 4 days.

### October 2020

#### Week 44

- **Kate - [Add support for the Zig language server](https://invent.kde.org/utilities/kate/-/merge_requests/117)**<br />
Request authored by [Matheus C. França](https://invent.kde.org/catarino) and merged after 5 days.

- **KTextEditor - [KateModeMenuList: remove special margins for Windows](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/39)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **KSyntaxHighlighting - [Improvement: Add some more boolean values to `cmake.xml`](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/93)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged at creation day.

- **KSyntaxHighlighting - [Fix: CMake syntax now marks `1` and `0` as special boolean values](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/95)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged at creation day.

- **KSyntaxHighlighting - [Improvement: Include Modelines rules in files where Alerts has been addded](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/94)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged at creation day.

- **KSyntaxHighlighting - [Misc: Add `SPDX-FileContributor` to `kateschema2theme`](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/89)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged after 5 days.

- **KSyntaxHighlighting - [Solarized themes: improve separator](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/92)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged after one day.

- **KSyntaxHighlighting - [Improvement: Updates for CMake 3.19](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/91)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged after 2 days.

- **KSyntaxHighlighting - [Add support for systemd unit files](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/86)**<br />
Request authored by [Andreas Gratzer](https://invent.kde.org/andreasgr) and merged after 5 days.

- **Kate - [Fix: Allow to `KActionSelector` @ filesystem browser config page to stretch vertically](https://invent.kde.org/utilities/kate/-/merge_requests/118)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged at creation day.

- **KTextEditor - [angle bracket matching](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/38)**<br />
Request authored by [Milian Wolff](https://invent.kde.org/mwolff) and merged at creation day.

#### Week 43

- **Kate - [filetree plugin: make filter case insensitive](https://invent.kde.org/utilities/kate/-/merge_requests/114)**<br />
Request authored by [Andreas Hartmetz](https://invent.kde.org/ahartmetz) and merged after 13 days.

- **KSyntaxHighlighting - [Feature: Allow multiple `-s` options for `kateschema2theme` tool](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/88)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged at creation day.

- **KSyntaxHighlighting - [Improvement: Add various tests to the `kateschemsa2theme` converter](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/87)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged at creation day.

- **KSyntaxHighlighting - [kateschema2theme: Add a Python tool to convert old schema files](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/85)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged after one day.

- **Kate - [Correct a typo in README.md](https://invent.kde.org/utilities/kate/-/merge_requests/116)**<br />
Request authored by [Felix Yan](https://invent.kde.org/felixonmars) and merged at creation day.

- **KSyntaxHighlighting - [Decrease opacity in separator of Breeze & Dracula themes](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/83)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **KSyntaxHighlighting - [Update README with "Color theme files" section](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/84)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **KSyntaxHighlighting - [Fix: Use `KDE_INSTALL_DATADIR` when install syntax files](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/81)**<br />
Request authored by [Alex Turbov](https://invent.kde.org/turbov) and merged at creation day.

#### Week 42

- **Kate - [kate: switch to recently used document when closing one](https://invent.kde.org/utilities/kate/-/merge_requests/115)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged at creation day.

- **KTextEditor - [Fix memory leak in KateMessageLayout](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/36)**<br />
Request authored by [Milian Wolff](https://invent.kde.org/mwolff) and merged at creation day.

- **KSyntaxHighlighting - [fix rendering of --syntax-trace=region with multiple graphics on the same offset](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/80)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 5 days.

- **KSyntaxHighlighting - [fix some issues of fish shell](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/79)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 12 days.

#### Week 41

- **Kate - [filetree plugin: make it possible to filter items in the tree view](https://invent.kde.org/utilities/kate/-/merge_requests/113)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged at creation day.

- **Kate - [Cleanup unused comments in KNSRC file](https://invent.kde.org/utilities/kate/-/merge_requests/112)**<br />
Request authored by [Alexander Lohnau](https://invent.kde.org/alex) and merged at creation day.

- **KTextEditor - [ontheflycheck: port QRegExp to QRegularExpression](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/33)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after 2 days.

- **KTextEditor - [katedocument: port one last QRegExp to QRegularExpression](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/34)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after 2 days.

- **KTextEditor - [kateview: port QRegExp to QRegularExpression](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/35)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after 2 days.

- **KSyntaxHighlighting - [Replace some RegExpr by StringDetect and StringDetect by Detect2Chars / DetectChar](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/78)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 5 days.

#### Week 40

- **KTextEditor - [Compile without deprecated method against qt5.15](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/32)**<br />
Request authored by [Laurent Montel](https://invent.kde.org/mlaurent) and merged after 2 days.

- **KSyntaxHighlighting - [AppArmor: update highlighting for AppArmor 3.0](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/77)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **Kate - [Documentation fixes: Use more entities, some punctuation](https://invent.kde.org/utilities/kate/-/merge_requests/111)**<br />
Request authored by [Antoni Bella Pérez](https://invent.kde.org/bellaperez) and merged at creation day.

- **KSyntaxHighlighting - [color cache for rgb to ansi256colors conversions (speeds up markdown loading)](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/74)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [SELinux: use include keywords](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/75)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **KSyntaxHighlighting - [SubRip Subtitles & Logcat: small improvements](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/76)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **KSyntaxHighlighting - [generator for doxygenlua.xml](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/73)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

### September 2020

#### Week 40

- **Kate - [add open-with entry to the context menu of the filebrowser](https://invent.kde.org/utilities/kate/-/merge_requests/110)**<br />
Request authored by [Mario Aichinger](https://invent.kde.org/aichingm) and merged at creation day.

- **Kate - [Add settings to toggle tabs close button and expansion](https://invent.kde.org/utilities/kate/-/merge_requests/109)**<br />
Request authored by [George Florea Bănuș](https://invent.kde.org/georgefb) and merged at creation day.

- **Kate - [addons/externaltools: Really respect BUILD_TESTING](https://invent.kde.org/utilities/kate/-/merge_requests/108)**<br />
Request authored by [Andreas Sturmlechner](https://invent.kde.org/asturmlechner) and merged at creation day.

- **Kate - [externaltools: extend git blame command line](https://invent.kde.org/utilities/kate/-/merge_requests/107)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged at creation day.

- **KTextEditor - [[kateprinter] Portaway from deprecated QPrinter methods](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/31)**<br />
Request authored by [Ömer Fadıl Usta](https://invent.kde.org/usta) and merged after one day.

- **Kate - [improve Kate config dialog](https://invent.kde.org/utilities/kate/-/merge_requests/105)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **Kate - [Use colorful "behavior" icon in settings window sidebar](https://invent.kde.org/utilities/kate/-/merge_requests/106)**<br />
Request authored by [Nate Graham](https://invent.kde.org/ngraham) and merged at creation day.

- **KSyntaxHighlighting - [Fix doxygen latex formulas](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/72)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after one day.

#### Week 39

- **Kate - [Port KRun to new KIO classes](https://invent.kde.org/utilities/kate/-/merge_requests/104)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after one day.

- **KTextEditor - [Don't create temporary buffer to detect mimetype for saved local file](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/30)**<br />
Request authored by [Milian Wolff](https://invent.kde.org/mwolff) and merged at creation day.

- **KSyntaxHighlighting - [ANSI highlighter](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/70)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 8 days.

- **KSyntaxHighlighting - [Add Radical color theme](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/68)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged after 5 days.

- **KSyntaxHighlighting - [make separator color less intrusive](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/71)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 3 days.

#### Week 38

- **KTextEditor - [[Vimode]Improve presentation of unset buffers on error](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/27)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after 2 days.

- **KTextEditor - [move separator from between icon border and line numbers to between bar and text](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/28)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KSyntaxHighlighting - [Add Nord color theme](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/67)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **KSyntaxHighlighting - [add proper license information to all themes](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/69)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KSyntaxHighlighting - [gruvbox theme](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/66)**<br />
Request authored by [Frederik Banning](https://invent.kde.org/laubblaeser) and merged after 2 days.

- **Kate - [Update configuration docs](https://invent.kde.org/utilities/kate/-/merge_requests/103)**<br />
Request authored by [Yuri Chornoivan](https://invent.kde.org/yurchor) and merged at creation day.

- **KTextEditor - [[Vimode]Make the behavior of numbered registers more accurate](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/26)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged at creation day.

- **KTextEditor - [Restore behavior of find selected when no selection is available](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/25)**<br />
Request authored by [Milian Wolff](https://invent.kde.org/mwolff) and merged after one day.

- **Kate - [implement optional limit for number of tabs and LRU replacement](https://invent.kde.org/utilities/kate/-/merge_requests/101)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **Kate - [Bug:426451](https://invent.kde.org/utilities/kate/-/merge_requests/102)**<br />
Request authored by [Ayushmaan jangid](https://invent.kde.org/ayushmaanjangid) and merged at creation day.

- **KTextEditor - [Clean up TwoViewCursor, open/close distinction has been redundant since 2007](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/24)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged at creation day.

- **KSyntaxHighlighting - [Add ayu color theme (with light, dark and mirage variants)](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/65)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **KSyntaxHighlighting - [Add POSIX/gmake alternate for Makefile simple variable assignment](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/64)**<br />
Request authored by [Peter Mello](https://invent.kde.org/pmello) and merged after one day.

#### Week 37

- **KTextEditor - [Completely switch over to KSyntaxHighlighting::Theme usage](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/21)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 6 days.

- **KSyntaxHighlighting - [tools to generate a graph from a syntax file](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/63)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **Kate - [Avoid some unneeded clones of snippet repositories.](https://invent.kde.org/utilities/kate/-/merge_requests/99)**<br />
Request authored by [Thomas Friedrichsmeier](https://invent.kde.org/tfry) and merged after 2 days.

- **Kate - [Fix cancel button being focused](https://invent.kde.org/utilities/kate/-/merge_requests/100)**<br />
Request authored by [Alexander Lohnau](https://invent.kde.org/alex) and merged at creation day.

- **KTextEditor - [add shortcut for paste mouse selection](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/23)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KTextEditor - [Move DocumentPrivate::attributeAt to KateViewInternal::attributeAt](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/22)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged at creation day.

- **KSyntaxHighlighting - [Add unit tests for Alerts and Modelines](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/59)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **KSyntaxHighlighting - [Update README](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/61)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

#### Week 36

- **KTextEditor - [Use KSyntaxHighlighting::Theme infrastructure for color themes in KTextEditor](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/20)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 11 days.

- **KSyntaxHighlighting - [Add Debian changelog and control example files](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/57)**<br />
Request authored by [Samuel Gaist](https://invent.kde.org/sgaist) and merged at creation day.

- **KSyntaxHighlighting - [add the 'Dracula' color theme](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/58)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KSyntaxHighlighting - [change theme json format, use meta object enum names for editor colors](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/54)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **KSyntaxHighlighting - [check kateversion >= 5.62 for fallthroughContext without fallthrough="true"...](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/56)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Add syntax definition for todo.txt](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/55)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Fix Int, Float and HlC* rules](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/53)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 4 days.

- **Kate - [Drop empty X-KDE-PluginInfo-Depends](https://invent.kde.org/utilities/kate/-/merge_requests/98)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged at creation day.

### August 2020

#### Week 36

- **KSyntaxHighlighting - [rename 'Default' to 'Breeze Light'](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/52)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KSyntaxHighlighting - [Varnish, Vala & TADS3: use the default color style](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/51)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **KSyntaxHighlighting - [Proposal to improve the Vim Dark color theme](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/50)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

#### Week 35

- **KSyntaxHighlighting - [Add Vim Dark color theme](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/49)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **KSyntaxHighlighting - [Ruby/Rails/RHTML: use the default color style and other improvements](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/48)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **KSyntaxHighlighting - [LDIF, VHDL, D, Clojure & ANS-Forth94: use the default color style](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/47)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **KSyntaxHighlighting - [ASP: use the default color style and other improvements](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/46)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **KSyntaxHighlighting - [Various examples](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/45)**<br />
Request authored by [Samuel Gaist](https://invent.kde.org/sgaist) and merged after one day.

- **KSyntaxHighlighting - [Some optimizations on the loading of xml files and Rule::isWordDelimiter function](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/38)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 2 days.

- **KSyntaxHighlighting - [SELinux CIL & Scheme: update bracket colors for dark themes](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/44)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **KSyntaxHighlighting - [POV-Ray: use default color style](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/43)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **KSyntaxHighlighting - [Test dark highlighting mode, too](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/39)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **KSyntaxHighlighting - [Django example](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/42)**<br />
Request authored by [Samuel Gaist](https://invent.kde.org/sgaist) and merged at creation day.

- **KSyntaxHighlighting - [CMake: fix illegible colors in dark themes and other improvements](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/41)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

#### Week 34

- **KTextEditor - [Use Q_DECLARE_OPERATORS_FOR_FLAGS in same namespace as flags definition](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/19)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged at creation day.

- **KSyntaxHighlighting - [Extend R syntax highlighting test](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/40)**<br />
Request authored by [Momo Cao](https://invent.kde.org/momocao) and merged at creation day.

- **KTextEditor - [autotests: skip crashing tests if Qt doesn't have the fix](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/18)**<br />
Request authored by [David Faure](https://invent.kde.org/dfaure) and merged after one day.

- **KSyntaxHighlighting - [BrightScript: Add exception syntax](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/35)**<br />
Request authored by [Daniel Levin](https://invent.kde.org/dlevin) and merged after 2 days.

- **KSyntaxHighlighting - [Improve comments in some syntax definitions (Part 3)](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/36)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **KSyntaxHighlighting - [R Script: use default color style and small improvements](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/37)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **KTextEditor - [API dox: use doxygen @warning tag to highlight warnings](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/17)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged at creation day.

- **KTextEditor - [Do not style internal code comments with doxygen-triggering brackets /** */](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/16)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged at creation day.

- **KTextEditor - [Vimode: Make numbered registers and the small delete register behave more similar to vim](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/15)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged at creation day.

- **KTextEditor - [Remove obsolete COPYING files](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/14)**<br />
Request authored by [Andreas Cord-Landwehr](https://invent.kde.org/cordlandwehr) and merged at creation day.

- **KSyntaxHighlighting - [Improve comments in some syntax definitions (Part 2)](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/32)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **KSyntaxHighlighting - [Remove obsolete COPYING files](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/34)**<br />
Request authored by [Andreas Cord-Landwehr](https://invent.kde.org/cordlandwehr) and merged at creation day.

- **KSyntaxHighlighting - [Modelines: remove LineContinue rules](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/33)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

#### Week 33

- **KSyntaxHighlighting - [Assembly languages: several fixes and more context highlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/30)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KSyntaxHighlighting - [Optimization: check if we are on a comment before using ##Doxygen which contains several RegExpr](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/31)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged at creation day.

- **KTextEditor - [Make "search for selection" search if there is no selection](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/13)**<br />
Request authored by [Simon Persson](https://invent.kde.org/persson) and merged at creation day.

- **KSyntaxHighlighting - [Improve comments in some syntax definitions](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/29)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged after 3 days.

- **KSyntaxHighlighting - [ColdFusion: use the default color style and replace some RegExpr rules](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/28)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged after 3 days.

- **Kate - [Port away from KMimeTypeTrader](https://invent.kde.org/utilities/kate/-/merge_requests/97)**<br />
Request authored by [Nicolas Fella](https://invent.kde.org/nicolasfella) and merged after one day.

- **KTextEditor - [Port the search interface from QRegExp to QRegularExpression](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/7)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after 14 days.

- **KTextEditor - [Vimode: Implement append-copy (Append to register instead of overwriting)](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/11)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged at creation day.

- **KTextEditor - [KTextEditor: Convert copyright statements to SPDX expressions](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/12)**<br />
Request authored by [Andreas Cord-Landwehr](https://invent.kde.org/cordlandwehr) and merged at creation day.

- **KSyntaxHighlighting - [Use angle brackets for context information](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/27)**<br />
Request authored by [Andreas Cord-Landwehr](https://invent.kde.org/cordlandwehr) and merged at creation day.

- **KSyntaxHighlighting - [Syntax-Highlighting: Convert copyright statements to SPDX expressions](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/25)**<br />
Request authored by [Andreas Cord-Landwehr](https://invent.kde.org/cordlandwehr) and merged at creation day.

- **KSyntaxHighlighting - [Revert removal of byte-order-mark](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/26)**<br />
Request authored by [Andreas Cord-Landwehr](https://invent.kde.org/cordlandwehr) and merged at creation day.

- **KSyntaxHighlighting - [xslt: change color of XSLT tags](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/24)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **KSyntaxHighlighting - [txt2tags: improvements and fixes, use the default color style](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/22)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **KSyntaxHighlighting - [Ruby, Perl, QML, VRML & xslt: use the default color style and improve comments](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/23)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **KSyntaxHighlighting - [replace some Regexp with .*$ and add syntax tests](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/18)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 19 days.

#### Week 32

- **KTextEditor - [Speed up a *lot* loading large files](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/8)**<br />
Request authored by [Tomaz  Canabrava](https://invent.kde.org/tcanabrava) and merged after 5 days.

- **KTextEditor - [Add a text Zoom indicator](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/10)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged at creation day.

- **KTextEditor - [Show a preview of the matching open bracket's line (similar to the Arduino IDE)](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/6)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after 31 days.

- **KTextEditor - [Use consitently categorized logged warnings](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/9)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged at creation day.

#### Week 31

- **Kate - [Highlight documentation: improve description of attributes of the comment elements](https://invent.kde.org/utilities/kate/-/merge_requests/96)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **KTextEditor - [Allow more control over invocation of completion models when automatic invocation is not used.](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/5)**<br />
Request authored by [Thomas Friedrichsmeier](https://invent.kde.org/tfry) and merged after 35 days.

- **KSyntaxHighlighting - [create StateData on demand](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/21)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after one day.

- **Kate - [Add support for the R language server](https://invent.kde.org/utilities/kate/-/merge_requests/95)**<br />
Request authored by [Luca Beltrame](https://invent.kde.org/lbeltrame) and merged after 3 days.

- **KSyntaxHighlighting - [Scheme: add datum comment, nested-comment and other improvements](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/15)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 11 days.

- **KSyntaxHighlighting - [Mathematica: some improvements](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/19)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged after 8 days.

- **KSyntaxHighlighting - [Doxygen: fix some errors ; DoxygenLua: starts only with --- or --!](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/17)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 11 days.

- **KSyntaxHighlighting - [AutoHotkey: complete rewrite](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/16)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 11 days.

- **KSyntaxHighlighting - [Add syntax highlighting for Nim](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/20)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged after 6 days.

### July 2020

#### Week 29

- **KSyntaxHighlighting - [replace some RegExp by AnyChar, DetectChar, Detect2Chars or StringDetect](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/13)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 7 days.

- **Kate - [Highlight documentation: add feature of comment position](https://invent.kde.org/utilities/kate/-/merge_requests/94)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged after one day.

- **KSyntaxHighlighting - [language.xsd: remove HlCFloat and introduce char type](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/11)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 6 days.

- **KSyntaxHighlighting - [KConfig: fix $(...) and operators + some improvements](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/14)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 2 days.

- **KSyntaxHighlighting - [ISO C++: fix performance in highlighting numbers](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/10)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged after 13 days.

- **KSyntaxHighlighting - [replace RegExpr=[.]{1,1} by DetectChar](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/12)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 4 days.

- **KSyntaxHighlighting - [Lua: attribute with Lua54 and some other improvements](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/9)**<br />
Request authored by [jonathan poelen](https://invent.kde.org/jpoelen) and merged after 13 days.

#### Week 28

- **Kate - [Update D LSP Server](https://invent.kde.org/utilities/kate/-/merge_requests/93)**<br />
Request authored by [Ernesto Castellotti](https://invent.kde.org/ernytech) and merged at creation day.

- **Kate - [Fix error if executable has spaces](https://invent.kde.org/utilities/kate/-/merge_requests/92)**<br />
Request authored by [Ernesto Castellotti](https://invent.kde.org/ernytech) and merged after one day.

#### Week 27

- **Kate - [kate: set last active view as current when restoring viewspace](https://invent.kde.org/utilities/kate/-/merge_requests/91)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged at creation day.

### June 2020

#### Week 26

- **KTextEditor - [Make "goto line" work backwards](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/4)**<br />
Request authored by [Nazar Kalinowski](https://invent.kde.org/nazark) and merged after one day.

- **Kate - [Use QTabBar](https://invent.kde.org/utilities/kate/-/merge_requests/89)**<br />
Request authored by [Tomaz  Canabrava](https://invent.kde.org/tcanabrava) and merged after one day.

- **KSyntaxHighlighting - [Add YARA language](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/4)**<br />
Request authored by [Wes H](https://invent.kde.org/hurdw) and merged after 32 days.

- **KSyntaxHighlighting - [Add Snort/Suricata language highlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/5)**<br />
Request authored by [Wes H](https://invent.kde.org/hurdw) and merged after 27 days.

#### Week 25

- **Kate - [build-plugin: also accept + in message filename detection](https://invent.kde.org/utilities/kate/-/merge_requests/88)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after 3 days.

#### Week 24

- **Kate - [session: sort list by ascending name](https://invent.kde.org/utilities/kate/-/merge_requests/87)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged at creation day.

- **KSyntaxHighlighting - [no deprecated binary json](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/8)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **KSyntaxHighlighting - [JavaScript/TypeScript: highlight tags in templates](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/6)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged after one day.

### May 2020

#### Week 22

- **Kate - [Fix "Run Current Document" so that it uses shebang or correct file path](https://invent.kde.org/utilities/kate/-/merge_requests/67)**<br />
Request authored by [Marcello Massaro](https://invent.kde.org/mmassaro) and merged after 99 days.

- **Kate - [Preview addon: Better update delay logic.](https://invent.kde.org/utilities/kate/-/merge_requests/85)**<br />
Request authored by [aa bb](https://invent.kde.org/aabb) and merged after 6 days.

- **Kate - [session: Sort session list by name again](https://invent.kde.org/utilities/kate/-/merge_requests/86)**<br />
Request authored by [Alex Hermann](https://invent.kde.org/alexhe) and merged after one day.

- **KTextEditor - [Add .diff to the file-changed-diff to enable mime detection on windows.](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/2)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged after one day.

- **KSyntaxHighlighting - [Raku: fix fenced code blocks in Markdown](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/3)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged after 6 days.

- **Kate - [[gdbplugin] Port QRegExp to QRegular Expression](https://invent.kde.org/utilities/kate/-/merge_requests/83)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after 7 days.

- **Kate - [[gdbplugin] Make extremely long variable names/values easier to view](https://invent.kde.org/utilities/kate/-/merge_requests/84)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged after 6 days.

#### Week 21

- **KTextEditor - [scrollbar minimap: performance: delay update for inactive documents](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/1)**<br />
Request authored by [Sven Brauch](https://invent.kde.org/brauch) and merged after one day.

- **KSyntaxHighlighting - [Add collaboration guide in README](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/2)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **KSyntaxHighlighting - [Rename Perl6 to Raku and add new file extensions](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/1)**<br />
Request authored by [Juan Francisco Cantero Hurtado](https://invent.kde.org/juanfra) and merged after one day.

#### Week 20

- **Kate - [Update debug target combobox when a target is selected](https://invent.kde.org/utilities/kate/-/merge_requests/82)**<br />
Request authored by [Jan Paul Batrina](https://invent.kde.org/jbatrina) and merged at creation day.

#### Week 19

- **Kate - [Patch for Bug 408174. Bug when opening remote folder via SFTP.](https://invent.kde.org/utilities/kate/-/merge_requests/44)**<br />
Request authored by [Charles Vejnar](https://invent.kde.org/vejnar) and merged after 199 days.

- **Kate - [Highlighting Doc: expand explanation about XML files directory](https://invent.kde.org/utilities/kate/-/merge_requests/76)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged after 10 days.

- **Kate - [Fix starting LSP server on Windows](https://invent.kde.org/utilities/kate/-/merge_requests/79)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged at creation day.

### April 2020

#### Week 17

- **Kate - [Initialize overlooked variables (and remove redundant calls)](https://invent.kde.org/utilities/kate/-/merge_requests/74)**<br />
Request authored by [Filip Gawin](https://invent.kde.org/gawin) and merged after 20 days.

- **Kate - [Highlighting Doc: add XML files directory of Kate's Flatpak/Snap package](https://invent.kde.org/utilities/kate/-/merge_requests/75)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

### March 2020

#### Week 13

- **Kate - [lspclient: use pyls entry script for python LSP server](https://invent.kde.org/utilities/kate/-/merge_requests/71)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after 13 days.

- **Kate - [build-plugin: pick and build default target when so requested](https://invent.kde.org/utilities/kate/-/merge_requests/72)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after 3 days.

- **Kate - [[XMLTools Plugin] Port QRegExp to QRegularExpression](https://invent.kde.org/utilities/kate/-/merge_requests/62)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after 47 days.

#### Week 11

- **Kate - [Use MarkInterfaceV2 to pass QIcons for the marker sidebar, not QPixmaps](https://invent.kde.org/utilities/kate/-/merge_requests/68)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged after 19 days.

### February 2020

#### Week 09

- **Kate - [Session manager: empty session list](https://invent.kde.org/utilities/kate/-/merge_requests/70)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [Prefer data-error/warning/information icons over dialog-* for line marker](https://invent.kde.org/utilities/kate/-/merge_requests/69)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged after 6 days.

#### Week 08

- **Kate - [search: handle both optional Project search options equally](https://invent.kde.org/utilities/kate/-/merge_requests/66)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after one day.

#### Week 06

- **Kate - [#417328 Properly declare supported file types](https://invent.kde.org/utilities/kate/-/merge_requests/64)**<br />
Request authored by [Jan Przybylak](https://invent.kde.org/janpr) and merged at creation day.

- **Kate - [[BackTrace Plugin] QRegExp to QRegularExpression](https://invent.kde.org/utilities/kate/-/merge_requests/63)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after one day.

- **Kate - [KUserFeedback integration](https://invent.kde.org/utilities/kate/-/merge_requests/60)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged after 14 days.

#### Week 05

- **Kate - [Fix usage of QtSingleApplication on Windows and Apple](https://invent.kde.org/utilities/kate/-/merge_requests/61)**<br />
Request authored by [Kåre Särs](https://invent.kde.org/sars) and merged at creation day.

### January 2020

#### Week 04

- **Kate - [ColorSchemeChooser improvements](https://invent.kde.org/utilities/kate/-/merge_requests/59)**<br />
Request authored by [David Redondo](https://invent.kde.org/davidre) and merged after 4 days.

- **Kate - [Fix External Tool "Google Selected Text"](https://invent.kde.org/utilities/kate/-/merge_requests/58)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged at creation day.

- **Kate - [External tools: Correctly set the working directory](https://invent.kde.org/utilities/kate/-/merge_requests/57)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged after 2 days.

#### Week 03

- **Kate - [doc: extended lspclient documentation](https://invent.kde.org/utilities/kate/-/merge_requests/56)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged at creation day.

#### Week 02

- **Kate - [Highlight documentation: add equivalence in regex for number rules and add lookbehind assertions](https://invent.kde.org/utilities/kate/-/merge_requests/55)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.



## Accepted Merge Requests of 2019

- 45 patches for [Kate](https://invent.kde.org/utilities/kate/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
- 6 patches for [kate-editor.org](https://invent.kde.org/websites/kate-editor-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)

### December 2019

#### Week 52

- **Kate - [Add icons to metadata of plugins supporting KDevelop/Plugin](https://invent.kde.org/utilities/kate/-/merge_requests/53)**<br />
Request authored by [Friedrich W. H. Kossebau](https://invent.kde.org/kossebau) and merged after 10 days.

#### Week 51

- **Kate - [LSP: add support for semantic highlighting](https://invent.kde.org/utilities/kate/-/merge_requests/47)**<br />
Request authored by [Milian Wolff](https://invent.kde.org/mwolff) and merged after 23 days.

#### Week 50

- **Kate - [doc: extend lspclient documentation](https://invent.kde.org/utilities/kate/-/merge_requests/52)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after 7 days.

#### Week 49

- **Kate - [[lspclient addon] Replace KRecursiveFilterProxyModel with QSortFilterProxyModel](https://invent.kde.org/utilities/kate/-/merge_requests/49)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after 2 days.

- **Kate - [Fix: Do not translate KActionCollection identifier](https://invent.kde.org/utilities/kate/-/merge_requests/50)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged at creation day.

- **Kate - [Fix translation of external tools](https://invent.kde.org/utilities/kate/-/merge_requests/51)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged at creation day.

- **Kate - [[project addon] Don't use deprecated KRecursiveFilterProxyModel](https://invent.kde.org/utilities/kate/-/merge_requests/48)**<br />
Request authored by [Ahmad Samir](https://invent.kde.org/ahmadsamir) and merged after one day.

### November 2019

#### Week 47

- **Kate - [Read the user visible name of the default color scheme](https://invent.kde.org/utilities/kate/-/merge_requests/46)**<br />
Request authored by [David Redondo](https://invent.kde.org/davidre) and merged after 11 days.

#### Week 46

- **Kate - [Add external tools for formatting XML and JSON content](https://invent.kde.org/utilities/kate/-/merge_requests/45)**<br />
Request authored by [Volker Krause](https://invent.kde.org/vkrause) and merged at creation day.

#### Week 45

- **Kate - [Remove redundant and c style casts](https://invent.kde.org/utilities/kate/-/merge_requests/42)**<br />
Request authored by [Filip Gawin](https://invent.kde.org/gawin) and merged after 22 days.

### October 2019

#### Week 42

- **Kate - [Multi project ctags](https://invent.kde.org/utilities/kate/-/merge_requests/43)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged at creation day.

- **Kate - [Update documentation of highlight & RegExp](https://invent.kde.org/utilities/kate/-/merge_requests/40)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged at creation day.

- **Kate - [Bump required version of qt to 5.10](https://invent.kde.org/utilities/kate/-/merge_requests/41)**<br />
Request authored by [Filip Gawin](https://invent.kde.org/gawin) and merged at creation day.

#### Week 40

- **Kate - [Project ctags](https://invent.kde.org/utilities/kate/-/merge_requests/36)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged at creation day.

- **Kate - [External Tools: Translate name and category](https://invent.kde.org/utilities/kate/-/merge_requests/35)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged after 2 days.

- **Kate - [kate: make size of persistent toolview also persistent](https://invent.kde.org/utilities/kate/-/merge_requests/31)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after 6 days.

- **kate-editor.org - [Add language selector and make the homepage translatable](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/7)**<br />
Request authored by [Carl Schwan](https://invent.kde.org/carlschwan) and merged at creation day.

- **Kate - [Projects Plugin: Support variables %{Project:Path} and %{Project:NativePath}](https://invent.kde.org/utilities/kate/-/merge_requests/34)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged at creation day.

- **Kate - [Add new custom value - KDE::windows_store](https://invent.kde.org/utilities/kate/-/merge_requests/33)**<br />
Request authored by [Carl Schwan](https://invent.kde.org/carlschwan) and merged at creation day.

### September 2019

#### Week 39

- **Kate - [fix hi-dpi rendering of tab buttons](https://invent.kde.org/utilities/kate/-/merge_requests/32)**<br />
Request authored by [Christoph Cullmann](https://invent.kde.org/cullmann) and merged at creation day.

- **Kate - [Compile with -DQT_DISABLE_DEPRECATED_BEFORE=0x050d00](https://invent.kde.org/utilities/kate/-/merge_requests/30)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged at creation day.

- **Kate - [Disalow Qt's Q_FOREACH/foreach macros](https://invent.kde.org/utilities/kate/-/merge_requests/29)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged at creation day.

- **Kate - [LSP Client config page: Use ui file](https://invent.kde.org/utilities/kate/-/merge_requests/28)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged at creation day.

- **Kate - [External Tools: Support adding actions to toolbar](https://invent.kde.org/utilities/kate/-/merge_requests/23)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged at creation day.

- **Kate - [External Tools: Extend output to include "Copy to Clipboard"](https://invent.kde.org/utilities/kate/-/merge_requests/27)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged at creation day.

- **Kate - [Properly translate the external tools menu + config widget](https://invent.kde.org/utilities/kate/-/merge_requests/26)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged at creation day.

- **Kate - [Add Tools > External Tools > Configure...](https://invent.kde.org/utilities/kate/-/merge_requests/25)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged at creation day.

- **Kate - [Implement KTextEditor::MainWindow::showPluginConfigPage()](https://invent.kde.org/utilities/kate/-/merge_requests/24)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged at creation day.

#### Week 38

- **Kate - [Remove deprecated plugins](https://invent.kde.org/utilities/kate/-/merge_requests/16)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged after 10 days.

- **Kate - [build-plugin: handle and process ninja stdout](https://invent.kde.org/utilities/kate/-/merge_requests/22)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after one day.

- **kate-editor.org - [Add a script to extract/merge translations](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/6)**<br />
Request authored by [Carl Schwan](https://invent.kde.org/carlschwan) and merged after 2 days.

- **Kate - [reformat: also format source files](https://invent.kde.org/utilities/kate/-/merge_requests/20)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged at creation day.

#### Week 37

- **Kate - [tabswitcher: switch to next-in-line doc when top document is closed](https://invent.kde.org/utilities/kate/-/merge_requests/18)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after one day.

- **Kate - [Added a line to set default sort order for the Symbolviewer.](https://invent.kde.org/utilities/kate/-/merge_requests/17)**<br />
Request authored by [Tore Havn](https://invent.kde.org/torehavn) and merged after one day.

- **Kate - [Revive externaltools plugin](https://invent.kde.org/utilities/kate/-/merge_requests/15)**<br />
Request authored by [Dominik Haumann](https://invent.kde.org/dhaumann) and merged at creation day.

#### Week 36

- **Kate - [search: make item margin configurable](https://invent.kde.org/utilities/kate/-/merge_requests/12)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after 2 days.

- **Kate - [Avoid using `size` for checking emptiness](https://invent.kde.org/utilities/kate/-/merge_requests/13)**<br />
Request authored by [Filip Gawin](https://invent.kde.org/gawin) and merged at creation day.

- **Kate - [Cleanup inheritance specifiers](https://invent.kde.org/utilities/kate/-/merge_requests/11)**<br />
Request authored by [Filip Gawin](https://invent.kde.org/gawin) and merged after one day.

- **kate-editor.org - [Syntax Page: add generated PHP syntax](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/5)**<br />
Request authored by [Nibaldo González](https://invent.kde.org/ngonzalez) and merged after 7 days.

### August 2019

#### Week 35

- **Kate - [Plugin search fixes](https://invent.kde.org/utilities/kate/-/merge_requests/10)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged at creation day.

#### Week 34

- **Kate - [Avoid copying if it's possible](https://invent.kde.org/utilities/kate/-/merge_requests/9)**<br />
Request authored by [Filip Gawin](https://invent.kde.org/gawin) and merged at creation day.

- **Kate - [Add .clang-format file.](https://invent.kde.org/utilities/kate/-/merge_requests/7)**<br />
Request authored by [Daan De Meyer](https://invent.kde.org/daandemeyer) and merged after 3 days.

- **Kate - [Buildplugin enhancements](https://invent.kde.org/utilities/kate/-/merge_requests/6)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged after 3 days.

#### Week 33

- **Kate - [Rename kate target to kate-lib to fix Windows compilation error](https://invent.kde.org/utilities/kate/-/merge_requests/5)**<br />
Request authored by [Daan De Meyer](https://invent.kde.org/daandemeyer) and merged at creation day.

- **Kate - [Main CMake scripts cleanup](https://invent.kde.org/utilities/kate/-/merge_requests/4)**<br />
Request authored by [Daan De Meyer](https://invent.kde.org/daandemeyer) and merged at creation day.

- **Kate - [Further cleanup addons CMake scripts.](https://invent.kde.org/utilities/kate/-/merge_requests/3)**<br />
Request authored by [Daan De Meyer](https://invent.kde.org/daandemeyer) and merged at creation day.

- **Kate - [katequickopen: add config option to present current or all project files](https://invent.kde.org/utilities/kate/-/merge_requests/2)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged at creation day.

#### Week 32

- **Kate - [build-plugin: also accept + in message filename detection](https://invent.kde.org/utilities/kate/-/merge_requests/1)**<br />
Request authored by [Mark Nauwelaerts](https://invent.kde.org/mnauwelaerts) and merged at creation day.

- **kate-editor.org - [Add links to kde.org](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/4)**<br />
Request authored by [Carl Schwan](https://invent.kde.org/carlschwan) and merged after one day.

#### Week 31

- **kate-editor.org - [Fix margin in team page](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/3)**<br />
Request authored by [Carl Schwan](https://invent.kde.org/carlschwan) and merged at creation day.

- **kate-editor.org - [Add more information about kate in the homepage](https://invent.kde.org/websites/kate-editor-org/-/merge_requests/1)**<br />
Request authored by [Carl Schwan](https://invent.kde.org/carlschwan) and merged at creation day.

