---
title: Kate5 on Windows
author: Dominik Haumann

date: 2014-11-30T15:52:10+00:00
url: /2014/11/30/kate5-on-windows/
categories:
  - KDE
  - Users
tags:
  - planet

---
Here it goes, Kate5 running on Windows:[<img class="aligncenter wp-image-3466 size-full" src="/wp-content/uploads/2014/11/kate5-on-windows.png" alt="Kate5 on Windows with Missing Icons and Bad Font" width="896" height="705" srcset="/wp-content/uploads/2014/11/kate5-on-windows.png 896w, /wp-content/uploads/2014/11/kate5-on-windows-300x236.png 300w" sizes="(max-width: 896px) 100vw, 896px" />][1]This is an early version of Kate5 on Windows. It runs just fine but has some glitches, such as the <del>white lines between selected text lines, or wrong margins in the search&replace bar, or showing a &#8216;+1&#8217; in the top right corner, although all documents are visible</del>.

<span style="text-decoration: underline;"><strong>Update:</strong></span> After installing oxygen-icons and switching the font to Consolas (what Visual Studio uses), the glitches above are gone. Here is an updated screenshot:

[<img class="aligncenter wp-image-3470 size-full" src="/wp-content/uploads/2014/11/kate5-on-windows1.png" alt="Kate5 on Windows With Icons and Better Font" width="894" height="708" srcset="/wp-content/uploads/2014/11/kate5-on-windows1.png 894w, /wp-content/uploads/2014/11/kate5-on-windows1-300x237.png 300w" sizes="(max-width: 894px) 100vw, 894px" />][2]

So essentially it works, and if all goes well, we hope to provide a good text editor experience with Kate5 on Windows in the next year(s). To this end, we are currently discussing having a joint Kate/KDevelop/Windows developer sprint early next year.

You can support this also by donating to the <a title="KDE End of Year 2014 Fundraiser" href="https://www.kde.org/fundraisers/yearend2014/" target="_blank">End of Year 2014 fundraiser</a>. Thanks!

 [1]: /wp-content/uploads/2014/11/kate5-on-windows.png
 [2]: /wp-content/uploads/2014/11/kate5-on-windows1.png