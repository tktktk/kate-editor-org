---
title: Kate/KDevelop/Skanlite Sprint Wrap-Up
author: Dominik Haumann

date: 2014-02-02T13:20:42+00:00
url: /2014/02/02/katekdevelop-sprint-wrap-up/
categories:
  - Common
  - Developers
  - Events
  - Users
tags:
  - planet
  - sprint
  - vi input mode

---
From 18th to 25th of January 2014, the Kate, KDevelop and Skanlite developers met in Barcelona to work on these projects full time for a week. Full time usually implies about 14 hours per person a day (yes, besides food, we do nothing but developing). 11 developers working 14 hours a day for 7 days makes a total of 1078 hours. If we divide this by 8 hours, the typical amount of work hours in a day, this makes 134 man-days of work, or about 27 weeks of continued development time. While this calculation is a bit theoretical, it is still very valid to estimate the amount of work that is put into these projects during such a sprint, especially since usually developers have far less time for development in their spare time.

The efforts focus mostly on KDE Frameworks 5, so what is listed next is mostly relevant only for the KF5/Qt5 version of Kate etc. Going through the Kate and Skanlite commits from 18th to the 25th of January, we have (not listing all commits):

January, 18th (arrival day):

  * KTextEditor::Cursor and KTextEditor::Range are declared as <a title="Q_MOVABLE_TYPE" href="http://commits.kde.org/ktexteditor/fec814f1f654bd6c43a13a8b748c9f6ed6b15a54" target="_blank">Q_MOVABLE_TYPE</a>, telling Qt containers that these primitive types can be mem-moved without copy constructor.
  * since KTextEditor and KatePart are now merged into a single list, the <a title="API changes" href="http://commits.kde.org/ktexteditor/f539e1c80c9191bb3a7409473c14bd713696d98e" target="_blank">API was changed</a> such that KTextEditor::Editor::instance() is a singleton. Therefore, KTextEditor::Document::editor() was dropped, as it is not needed anymore.
  * vi input mode: new <a title="Sentence Text Object" href="http://commits.kde.org/kate/50593e34dfa0330e3226c0b24ac709b29ef27a4f" target="_blank">sentence text object</a>
  * <a title="Merged Interfaces" href="http://commits.kde.org/ktexteditor/59b824fded498cc6860f2ded302e71e5255b6af0" target="_blank">merged</a> code completion models
  * some API <a title="API cleanups" href="http://commits.kde.org/ktexteditor/e0f07584dfbde2e8854ce7dea05603904814310f" target="_blank">cleanups</a>
  * <a title="removed ModeInterface" href="http://commits.kde.org/ktexteditor/b8531da93806b953496efc2a166a36a86bad1d49" target="_blank">removed ModeInterface</a>, which was never implemented

January, 19th:

  * vi input mode: enabled <a title="vi input mode numbered registers" href="http://commits.kde.org/kate/5562332777f35d0885c208a03c0c1086b656467f" target="_blank">numbered registers</a> for &#8220;q&#8221; command
  * <a title="less hacks" href="http://commits.kde.org/ktexteditor/1deece7853c5542f70482e6a9487fca001aa5cd5" target="_blank">removed hack</a> setSuppressOpeningErrorDialogs
  * cleaner codeflow for <a title="Default Encoding" href="http://commits.kde.org/ktexteditor/5bf99370ec0ee3dbe2b3e33351d8216029efc6da" target="_blank">default encoding</a>
  * more <a title="API cleanup" href="http://commits.kde.org/ktexteditor/6ad6841e9844ef59772b8a417fb2bb5f390cb5d2" target="_blank">API cleanups</a>
  * merged <a title="Merged Interface" href="http://commits.kde.org/ktexteditor/f60875167fd807866df33d22a5353b7ad497d2c7" target="_blank">HighlightInterface</a> into KTE::Document
  * change <a title="BC Editor" href="http://commits.kde.org/ktexteditor/6c48751ad38e461befe40ae64575268a1d94ea46" target="_blank">Editor implementation</a> such that it can be extended in a binary compatible way
  * make <a title="Find previous button in searchbar in vi mode" href="http://commits.kde.org/kate/001dac5b457fca84ea9872fd9e5ebc8262942630" target="_blank">find previous button</a> in searchbar in vi-mode work
  * change <a title="View implementation" href="http://commits.kde.org/ktexteditor/e9a418bcfb198a78c04667d02e5aab9c4d16f59d" target="_blank">View implementation</a> such that it can be extended in a binary compatible way
  * change <a title="BC Document" href="http://commits.kde.org/ktexteditor/9d94e9b19892e093c188fa650df223b73a79543b" target="_blank">Document implementation</a> such that it can be extended in a binary comptible way
  * vi input mode: <a title="vimode: added the &quot;(&quot; and &quot;)&quot; motions" href="http://commits.kde.org/kate/7c431c5706a9fcd544775e203a6351d6b0d2204c" target="_blank">added &#8220;(&#8221; and &#8220;)&#8221; motions</a>
  * run astyle on <a title="atyle on KTE interfaces" href="http://commits.kde.org/ktexteditor/cdef90c0bcc7e0e6636618f0edfee6940d82114f" target="_blank">KTextEditor interfaces</a>
  * <a title="API cleanup" href="http://commits.kde.org/ktexteditor/984110c67f22fe19bcaf8b17286c92c1551f2103" target="_blank">API cleanup</a>: return strings by value
  * port <a title="Qt5 Accessibility" href="http://commits.kde.org/ktexteditor/dbc4c0709a7832ff7c8a17009ece346fbe610a6c" target="_blank">accessibility interface</a> to Qt5 (<a title="more" href="http://commits.kde.org/ktexteditor/7f0c00fd2ca1813cb62ac405bd10cd833e782186" target="_blank">more</a>, <a title="more" href="http://commits.kde.org/ktexteditor/37a79b2aed45278e7ce9de23fa3759e4584e1178" target="_blank">more</a>)
  * start of <a title="Start porting libksane" href="http://commits.kde.org/libksane/5b8884e016786bcef2ae6430ca612ed2290f1307" target="_blank">porting libksane</a> to Qt5 and KDE Frameworks 5 (<a title="more libksane porting" href="http://commits.kde.org/libksane/14a01a01c3ef90f46df25be3aaac70456670b32b" target="_blank">more</a>)
  * make <a title="KateRenderer improvements" href="http://commits.kde.org/ktexteditor/e53632b0e86a12a3cf1f7b0a49170160eaf211a5" target="_blank">KateRenderer</a> work without a view
  * add back KTextEditor::Document::<a title="KTE::Document::print()" href="http://commits.kde.org/ktexteditor/60c47b178a0409ed7963b40dd927091f77468e97" target="_blank">print/printPreview()</a> interface
  * file tree: add <a title="Print Actions in Context menu of file tree" href="http://commits.kde.org/kate/789d2d8fb07b7803ef58fea65c640da4f40ff39f" target="_blank">print actions</a> to context menu
  * remove complex [KTE::Document::textChanged()][1] signal
  * libksane: [remove compiler warnings][2]
  * port the <a title="port gdb backtrace browser" href="http://commits.kde.org/kate/e6f7b7c8aea55a98852b41dd4f94eca20c5381f7" target="_blank">gdb backtrace browser</a> plugin to Qt5 and the new KTextEditor plugin interfaces
  * adapt the Konsole plugin to <a title="Konsole plugin interface changes" href="http://commits.kde.org/kate/a14a93ee39af2e3d486dc6360ac61eae48d5a625" target="_blank">interface changes</a>
  * gdb backtrace browser: compile <a title="less kde4support" href="http://commits.kde.org/kate/e5356f57dedc9feffa15b52827676f13e43a2ebf" target="_blank">without kde4support</a>
  * <a title="new Qt connect syntax" href="http://commits.kde.org/ktexteditor/3df630b5ec345d4011dad88be2dfef32e976cd72" target="_blank">fix invalid disconnect</a> due to new Qt connect syntax (tricky one!)
  * libksane: <a title="move kde4support from libksane" href="http://commits.kde.org/libksane/3e0f2b1fb01a727aa7f04a02c9fcec04ac9c864a" target="_blank">remove kde4support</a> (<a title="more" href="http://commits.kde.org/libksane/1647f68d9945044c0685f95950c81ecb59a3bbd0" target="_blank">more</a>)
  * KTE::Document: <a title="only emit the textInserted/Removed stuff on editing actions, not file close/reload" href="http://commits.kde.org/ktexteditor/0d8c29d1b4b49e1811ac8b46824043cb65415b4f" target="_blank">do not emit signals</a> on file close
  * gdb backtrace browser <a title="bt browser ported" href="http://commits.kde.org/kate/dad4ca3a6f35ecebe7d9a0ab41c754744f711348" target="_blank">fully ported</a>, ran <a title="astyle" href="http://commits.kde.org/kate/f743187951e5924d4b8b166e9ad6416a9e7af9e6" target="_blank">astyle</a>, add <a title="bt browser unit test" href="http://commits.kde.org/kate/55821d993404ee45109cfd5351b9ee0bc657acc5" target="_blank">unit test</a>
  * delete KTextEditor::Smart* <a title="remove convenience includes" href="http://commits.kde.org/kde4support/8358f77eb3ed8ba9897903e3abb2e0966979be7e" target="_blank">convenience includes</a>
  * KTextEditor::Editor is a <a title="KTE::Editor singleton" href="http://commits.kde.org/kate/ed637dd2ee083efe2cea935309235e8f35a2cdd3" target="_blank">singleton</a> now
  * libksane: make <a title="make install for libksane" href="http://commits.kde.org/libksane/1cb52ebd487421e0f0fcfb1d234673e63109c25b" target="_blank">make install</a> work
  * KWrite: <a title="less singletons" href="http://commits.kde.org/kate/65064825e7f233b4a25c282449bce4c115cd0cff" target="_blank">singleton cleanup</a>
  * refactor KTextEditor module: <a title="no src/src" href="http://commits.kde.org/ktexteditor/6dc4fa82dd8844562f561645aea3d1789c3c8ee4" target="_blank">part 1</a>, <a title="refactor" href="http://commits.kde.org/ktexteditor/e1f1f80e9c7d4a9fecd98f794156fa5991c78ed5" target="_blank">part 2</a>, <a title="refactor" href="http://commits.kde.org/kate/493d2eab17c4ce455df530899addb5fa1f9838fa" target="_blank">part 3</a>,
  * vi input mode: don&#8217;t launch commands <a title="vi-mode: don't launch commands trough commandBar" href="http://commits.kde.org/ktexteditor/01a8dd4edd9d38cb56a0422fa352b297500f4890" target="_blank">through commandBar</a>
  * remove autobrace plugin in favour of <a title="we shall use sanebrace plugin for KF5" href="http://commits.kde.org/kate/6b272c40e49f7566a666671af4a539d8387392b6" target="_blank">sanebrace plugin</a> in KF5
  * vi input mode: add <a title="vimode: added the paragraph text object." href="http://commits.kde.org/kate/8037623d6b42ef52d2d664de7488fc074a919100" target="_blank">paragraph text object</a>
  * KateScriptDocument <a title="use default scheme, no need for view" href="http://commits.kde.org/ktexteditor/79cba8b7ccdb9ec6b89ca7907014256eb1641a70" target="_blank">cleanups</a>
  * more <a title="more cleanups" href="http://commits.kde.org/ktexteditor/ac591aefe0c24341faa02a4686fc42d65e993aef" target="_blank">interface cleanups</a>
  * fix <a title="Fix XML indentation after self-closing tags" href="http://commits.kde.org/kate/98c6e9fd68966278e411a7891a04b5bc0b76fc0b" target="_blank">XML indentation</a> after self-closing tags
  * completion model: properly emit <a title="Emit layoutChanged when the completion model filter changes" href="http://commits.kde.org/kate/cb90c51a152f93e84e914d3e0754a9ac4b5cc22d" target="_blank">layoutChanged</a>
  * SQL plugin: <a title="start SQL plugin port" href="http://commits.kde.org/kate/1e919a19aca1728f8c5c36b1f61545cee99c4003" target="_blank">start port</a>, <a title="more porting" href="http://commits.kde.org/kate/97c3db1dc58460573fa9aa9e71c7a7f7e45e0f7b" target="_blank">more porting</a>, <a title="more ported" href="http://commits.kde.org/kate/ab4e7210cce190515e14c77696882dcf4b4715d7" target="_blank">and more</a>¸<a title="more porting" href="http://commits.kde.org/kate/b1d9bea7951ee8373580e6c18e3379fa6ee5c49c" target="_blank">more</a>, <a title="more porting" href="http://commits.kde.org/kate/6c9dd0ba59c8c0182d3a1666a38074516c87431e" target="_blank">more</a>, <a title="more" href="http://commits.kde.org/kate/1811dea1ddc6bb649f2339d8f40e76dc5b3d7a87" target="_blank">.</a><a title="more" href="http://commits.kde.org/kate/24cc5954bb105e0f6c5a4d8cdf5e10a8fe6a011b" target="_blank">.</a><a title="more" href="http://commits.kde.org/kate/b1ce76ef962b43263e002c1b3ffd84f4c0118c45" target="_blank">.</a><a title="more" href="http://commits.kde.org/kate/f2f646040f5cfe63f80c81465454f63b96158615" target="_blank">.</a><a title="fix xmlgui" href="http://commits.kde.org/kate/357aba70f542244bda86923b4c1e1aa31ebf9c67" target="_blank">.</a>, <a title="kate sql is back" href="http://commits.kde.org/kate/654f8da8260c078ff7773e8af11643b72081995d" target="_blank">done!</a>
  * libksane: adhere to <a title="rename the sourcefolder from libksane to src" href="http://commits.kde.org/libksane/57d41a52e78e15becb51dbedd01c6cead2c057dc" target="_blank">KF5 frameworks template</a>, <a title="libksane, frameworks" href="http://commits.kde.org/libksane/d6f576adcb836cf62e328f34a1181255e1697a91" target="_blank">more</a>, <a title="more" href="http://commits.kde.org/libksane/22229f2373c55c74befc1f654854215a9b782862" target="_blank">more</a>, <a title="libksane works" href="http://commits.kde.org/libksane/0db5e64875a0a0e2bd17432704c7d3de89e2dca5" target="_blank">works</a>
  * Skanlite: <a title="cmake works, but does not compile yet" href="http://commits.kde.org/skanlite/277950c3e30420ab4eafe67b5d2b2042afb26307" target="_blank">start porting</a> to KDE Frameworks 5
  * port <a title="Port the CTags plugin to frameworks and re-enable building" href="http://commits.kde.org/kate/01ee01427750604a80a41510e470f790847cc3e8" target="_blank">CTags plugin</a> to KTextEditor framework
  * vi input mode: <a title="vi-mode: remove cmdLineBar usage from KateViEmulatedCommandBar" href="http://commits.kde.org/ktexteditor/a57d0ee2465b764929fa5a01fb1cf44838500825" target="_blank">cleanups</a>
  * vi input mode: remove vi specifics from <a title="remove vi specific parts from KateCmdLineEdit" href="http://commits.kde.org/ktexteditor/9307fa13b968f664b96904e8b987e24a05b91065" target="_blank">KateCmdLineEdit</a>
  * fix <a title="add target property: EXPORT_NAME &quot;FooBar&quot;" href="http://commits.kde.org/kdeexamples/4ace48993bc2a8cb1891247d4e8ba1bff48ba95e" target="_blank">frameworks template</a>

January, 20th:

  * fix <a title="Fix indentation when an opening bracket is not in code" href="http://commits.kde.org/kate/18ace497e8dfc44f0fdd860a710767379ac3a72c" target="_blank">cstyle indenter bug</a>
  * <a title="cleanup cmake lists" href="http://commits.kde.org/kate/078c08d1b5dd2bcfb225cfc6296a73f9e18981e2" target="_blank">cleanup</a> cmake files
  * port <a title="filebrowser is back" href="http://commits.kde.org/kate/82d1b8daea4285c65600220c67b03319b982da46" target="_blank">File System Browser</a> plugin to new KTextEditor plugin interfaces
  * fix <a title="Fix opening of files by using QUrl::fromUserInput() in stead of QUrl()" href="http://commits.kde.org/kate/f4585e8a37eadac4931c42aa9a1450e7c125d890" target="_blank">porting bugs</a>, <a title="fix porting bugs" href="http://commits.kde.org/kate/9a8c405462a1dd5ac01e3ebdcde74ebd1028aacd" target="_blank">more fixing</a>
  * xml tools: <a title="initial port of xml completion plugin" href="http://commits.kde.org/kate/a23814ba19d6193d482067fdfd79486bdbff258b" target="_blank">port</a> to new KTextEditor interfaces, <a title="xml completion plugin: remove kde4 support" href="http://commits.kde.org/kate/20f692f263491d51731d3c1e706c38811ecb68db" target="_blank">remove kde4support</a>, run <a title="astyle: format code" href="http://commits.kde.org/kate/1170457cb72e03a67c50c2303a3092604ecd0192" target="_blank">astyle</a>
  * huge KWrite <a title="cleanup KWrite even more" href="http://commits.kde.org/kate/c6782b720ee8c5251c0d87adf3bac9a64de4d870" target="_blank">code cleanup</a>
  * let <a title="let automoc do the job" href="http://commits.kde.org/kate/138943e28069d8e8086543a8ac3878c2e1ab588a" target="_blank">automoc</a> do the work
  * move filetree &#8220;Documents&#8221; to <a title="filetree is just a plugin" href="http://commits.kde.org/kate/0c71220477f6a9a33de9b04caf14f383bfbc430e" target="_blank">addons</a> folder, add <a title="add Messages" href="http://commits.kde.org/kate/9d8f091f037ae64b4bbe5f5faa86377e86b417ff" target="_blank">Messages.sh</a>
  * KMessageWidget: <a title="extend KMessageWidget animation info" href="http://commits.kde.org/kwidgetsaddons/c5fe6c274be46d49cb836426463c8af0407258a6" target="_blank">extend API</a>, show <a title="show an example image in the API documentation of KMessageWidget" href="http://commits.kde.org/kwidgetsaddons/56125958066c87bcfe1835fabd143891d46a06f5" target="_blank">example image</a> in docs
  * remove <a title="the world hates the tip of the day for an advanced editor " href="http://commits.kde.org/kate/de6e3f794309c3a01d0f78a4250d51afe9c453fd" target="_blank">tip of the day</a>
  * fix generation of <a title="move up the real main dox file" href="http://commits.kde.org/ktexteditor/e4ca13be334ef2111ee9e953d72573d9da1adec0" target="_blank">API documentaion</a>, extend <a title="add API documentation" href="http://commits.kde.org/kwidgetsaddons/3bb0688b6a8edf95afad4a4f8b7d7ed8f02d23ab" target="_blank">API documentation</a>
  * removed <a title="Removed the KateScriptConsole." href="http://commits.kde.org/kate/55186a6b94135e60cb1ccf6064f26d352b90dce1" target="_blank">KateScriptConsole</a>, was unfortunately not used much
  * <a title="split unit tests" href="http://commits.kde.org/kate/02c94fbb7317ba91ab021e1b9af41cf8579cff96" target="_blank">refactor</a> unit tests, <a title="split all unit tests" href="http://commits.kde.org/kate/90357e3702c38a25c0fbcc1816c69c42f8325185" target="_blank">more</a>
  * vi input mode: <a title="vimode: fixed two crashes related to the undo/redo operations." href="http://commits.kde.org/kate/3539e7687b91de49f7d68b719d27b9f76e4d8379" target="_blank">fix two crashes</a> in undo/redo operations
  * use new KMessageWidget API in KatePart&#8217;s <a title="adapt to KMessageWidget in KWidgetAddons" href="http://commits.kde.org/ktexteditor/0c3b65574284797539197d7b720d2a9a31b21c91" target="_blank">notification system</a>
  * <a title="fix queuing of search wrap messages when holding down F3" href="http://commits.kde.org/ktexteditor/79fd507a4f2ad6b774e25752592db7e9cc0dc4fa" target="_blank">fix queuing</a> of &#8220;Search Wrapped&#8221; messages
  * <a title="Port the GDB plugin to frameworks" href="http://commits.kde.org/kate/34fce7c98b0cfa00ce8debcd3bb2c7db3ab96380" target="_blank">port GDB Plugin</a> to new KTextEditor interfcaes in KDE frameworks 5
  * Skanlite: use <a title="Use frameworks cmake templates. Compiles but runtime error (see FIXMEs)." href="http://commits.kde.org/skanlite/c6543cc5d9fcda986ae34a111fb7cf3bfa5c1423" target="_blank">cmake frameworks template</a>
  * turn <a title="dyn word wrap on per default" href="http://commits.kde.org/ktexteditor/5556c9e8ade33342dea3456eed1862dea3653ff6" target="_blank">dynamic word wrap</a> on by default
  * completion popup: <a title="sort completion items which match the capitalization of what was typed first" href="http://commits.kde.org/kate/40382e49906be035a2a8ae26d0ea8ef416b3027d" target="_blank">better sorting</a>
  * Build Plugin: add support for <a title="build plugin: add support for Intel icpc error messages" href="http://commits.kde.org/kate/33f5deef3f7537d2c4269b912e97df230291e2fc" target="_blank">Intel icpc error messages</a>
  * port <a title="A first rough port" href="http://commits.kde.org/kate/1bfdca1ae15c4f4aff26be7cf3d033a0b8a32e83" target="_blank">Open Header</a> plugin¸ <a title="A littlebit of QLatin1String to QStringLiteral conversion" href="http://commits.kde.org/kate/e22fc431712f2c58a96d0f48b0f40776f952addb" target="_blank">cleanups</a>

January, 21th:

  * Kate App: less <a title="less friends" href="http://commits.kde.org/kate/f81ff78a53eb7a3a76567f1b61bd35671f5de8f5" target="_blank">friend classes</a>, and even <a title="less friends" href="http://commits.kde.org/kate/8141ccd00c867c6a679be79d18fa822e2f8e102c" target="_blank">more less friends</a>
  * Kate App: <a title="KateWaiter in own file" href="http://commits.kde.org/kate/e81f4bd908ed4f60dd9eb0893e761d230d9e9c2a" target="_blank">refactor</a> waiter class
  * <a title="search always for .kateconfig upwards" href="http://commits.kde.org/ktexteditor/33a4e9e9e740b4c351360841c4357bd81af19b72" target="_blank">.kateconfig files</a> are now always searched in ALL parent folders (not configurable anymore)
  * better <a title="better recursion guard, like if have in the project plugin" href="http://commits.kde.org/ktexteditor/162b785e751d56ac125fa568e3c369b0d5f5046b" target="_blank">recursion guard</a> for .kateconfig lookup
  * DocumentPrivate::closeUrl() <a title="small cleanups" href="http://commits.kde.org/ktexteditor/977acc5975ee62ccb1cf6deea1e08649d9d891e2" target="_blank">cleanup</a>
  * Skanlite: <a title="Introduce QDialogButtonBox because the setButtons(KDialog::User1...) etc. is not available anymore" href="http://commits.kde.org/libksane/b60a536fafba1b77a1fcd2ad105d68287de12d5b" target="_blank">port dialogs</a>, use <a title="use QApplication instead of KApplication" href="http://commits.kde.org/skanlite/54b8ff0fa326045578f1af3a2cfd8aa5bae43797" target="_blank">QApplication</a>¸more <a title="fix FIXMEs: button enabling/disabling and rename some widget variables to better see what they are used for" href="http://commits.kde.org/libksane/f462baff7edd7fa9ebb76864661c22ccb44ed344" target="_blank">porting</a>
  * KTextEditor API documentation: <a title="more KDE4 -> KF5 porting notes" href="http://commits.kde.org/ktexteditor/6d4b8dd991d936a6ad30be52dda9544e18aba603" target="_blank">porting notes</a>
  * Search: <a title="ctrl+h: set cursor position to end of selection" href="http://commits.kde.org/ktexteditor/754f23797b176422875c846522e1404618b77c13" target="_blank">Ctrl+H</a> now sets cursor to end of selection
  * KTextEditor: start adding a <a title="integrate a decent statusbar" href="http://commits.kde.org/ktexteditor/2d0f9881d872ec5fbba6dd64a58a00cb98966d35" target="_blank">default status bar</a>
<li style="text-align: left;">
  <a title="use integrated status bar, need to add API to hide it" href="http://commits.kde.org/kate/16c0e4a1be8a53966603b09f307d38b3e74250fa" target="_blank">KWrite</a>: use KTextEditor default status bar
</li>
<li style="text-align: left;">
  <a title="use integrated status bar, need to add API to hide it, for file name we will have extra widget soon  dominik must be faster" href="http://commits.kde.org/kate/334ca9835c6dda622a0b8f2ed79f00a08172b88f" target="_blank">Kate App</a>: use KTextEditor default status bar
</li>
<li style="text-align: left;">
  Status bar: <a title="integrate a decent statusbar" href="http://commits.kde.org/ktexteditor/f5cae065eee28c0dd9ae8446ab92d750bb077b17" target="_blank">finetuning</a>
</li>
<li style="text-align: left;">
  <a title="add MainWindow::closeView/splitView and Application::quit methods" href="http://commits.kde.org/ktexteditor/25c484af021e62d010b92c882029bb2d01ea2b5f" target="_blank">KTE::Application</a>: add closeView/splitView, quit() methods
</li>
<li style="text-align: left;">
  <a title="add Qt style api for disabling the status bar" href="http://commits.kde.org/ktexteditor/ccd7be29a41122857a734dfc5a44a48eb3c2a995" target="_blank">add API</a> to disable default status bar
</li>
<li style="text-align: left;">
  add <a title="finally: sane switch of mode & encoding via status bar" href="http://commits.kde.org/ktexteditor/eac65e431524eea2c2e8fc48b8bb1e0b2378f27f" target="_blank">Encoding and Mode</a> to status bar
</li>
<li style="text-align: left;">
  Status bar: port modified on disk <a title="port modified on disk notification" href="http://commits.kde.org/ktexteditor/63497b565dcfbe7402af97ce1005e38bb3ae9509" target="_blank">notification</a>
</li>
<li style="text-align: left;">
  <a title="seems to work with KF5, except for the save/restoreWindowSize of the dialog" href="http://commits.kde.org/kate/2899f2ab5c2838c6d55737fe51194eb71a0e6977" target="_blank">port</a> close except like plugin
</li>
<li style="text-align: left;">
  <a title="Before stating (especially important for network files) search in the list of opened documents" href="http://commits.kde.org/kate/cc573ab35fc9f08e5f58e1670eeb077a3475d11d" target="_blank">Open Header plugin</a>: search list of open document before stating files
</li>
<li style="text-align: left;">
  Skanlite: start <a title="So first step is to port all KDialog to QDialog. Step one is done. skanlite compiles but there are additional fixmes to fix." href="http://commits.kde.org/skanlite/86780c2e7f0f0b9dad7d5834718048dcd4b6d6df" target="_blank">porting</a> KDialog to QDialog, fix <a title="main dialog: connect Help, About, Settings and Close button. Close still crashes the application." href="http://commits.kde.org/skanlite/67ffca9066e2af8aeee791dd84f0d1737da48b9a" target="_blank">help button</a>, and <a title="Fix Settings dialog: make the Ok and Cancel button work" href="http://commits.kde.org/skanlite/64e10b78dbe2d7796fea02cbcbd6bccd6e443017" target="_blank">more</a>
</li>
<li style="text-align: left;">
  Kate App: add <a title="start adding a tabbar to the view space" href="http://commits.kde.org/kate/f952657a81e8154afa869fbb6e988f30c9255c06" target="_blank">built-in tab bar</a> for switching documents in a LRU fashion
</li>
<li style="text-align: left;">
  Kate config dialog: fix <a title="fix help button" href="http://commits.kde.org/kate/10b6360da8ac1f9bc25f22f66db2402e919bad38" target="_blank">help button</a>
</li>
<li style="text-align: left;">
  template interface, use <a title="port away from plain C to QHostInfo, want windows to work " href="http://commits.kde.org/ktexteditor/7c6ca9880540cbc49085485b764181ba781a2931" target="_blank">QHostInfo</a>
</li>
<li style="text-align: left;">
  port <a title="replacement for not existing delayed menu in QPushButton. Future feature in menu, but not yet there" href="http://commits.kde.org/kate/66e417c8fa9c1ae45b456e891afde32ca7a8605b" target="_blank">session chooser</a>
</li>
<li style="text-align: left;">
  Kate App: <a title="remove unused functions" href="http://commits.kde.org/kate/52eb91b02483c3d14283379e7183a4aa9a980503" target="_blank">ViewManager cleanup</a>
</li>
<li style="text-align: left;">
  port the <a title="Port the build plugin to frameworks" href="http://commits.kde.org/kate/65e53a8bfaf1527c3c1c1a1da6b8db65b1a628ae" target="_blank">Build plugin</a> to the new KTextEditor interfaces
</li>
<li style="text-align: left;">
  word completion: <a title="no beep, no notifications" href="http://commits.kde.org/ktexteditor/3bfbd687d53f9d6125f3ecb8ff7052aa4b76c6e1" target="_blank">remove beep()</a>
</li>
<li style="text-align: left;">
  <a title="Fix skanlite version. Fix AboutData handling. Use QCommandLineParser. Fix main dialog crash after exit." href="http://commits.kde.org/skanlite/92933bebb13bd513ed19bdda269956322bc849b5" target="_blank">Skanlite</a>: fix about data handling, use QCommandLineParser
</li>
<li style="text-align: left;">
  <a title="let automoc do its job" href="http://commits.kde.org/ktexteditor/2e8f3c5ef56debf48be8850b7124485190b1675f" target="_blank">less</a> *.moc <a title="let automoc do its job" href="http://commits.kde.org/ktexteditor/6a2c124e39485f28280d65cdf0faf002d08a6deb" target="_blank">includes</a>
</li>

January, 22th:

<li style="text-align: left;">
  vi input mode: expose more motions in <a title="vimode: added some motions that were missing in visual mode." href="http://commits.kde.org/kate/5c712e5c71526c80486e6593a35f780902472be2" target="_blank">visual mode</a>
</li>
<li style="text-align: left;">
  status bar: <a title="resort statusbar" href="http://commits.kde.org/ktexteditor/8d86bac77dd4e98893cc7c36f828041f2db3347e" target="_blank">better sorting</a>
</li>
<li style="text-align: left;">
  tab bar: add tab also for <a title="tab bar: for documents without view, still add a button (lazy loading)" href="http://commits.kde.org/kate/4c0a321e216d3c96e7608bdc79bcfdb906d91148" target="_blank">documents without view</a>
</li>
<li style="text-align: left;">
  tab tracking: <a title="simplify keeping track of tabs" href="http://commits.kde.org/kate/db0734e5a309bcb26a150e7373dbddc8369bc3bb" target="_blank">simplify</a>, make tab switching <a title="tab bar: make switching work, make quick open work" href="http://commits.kde.org/kate/4a3e0d86762ff8bda3e28d9146ddfc9348171263" target="_blank">work</a>
</li>
<li style="text-align: left;">
  tab bar tabs: <a title="track document name changes" href="http://commits.kde.org/kate/3ff416ea03d1f5e79a15da40793e0fd91c68bf52" target="_blank">track document name</a> changes
</li>
<li style="text-align: left;">
  status bar: fix sizing mess (<a title="start to fix resizing mess of status bar" href="http://commits.kde.org/ktexteditor/86a3dc664cb4f1bb769bec4fdcf0929ff7b6b920" target="_blank">part 1</a>¸ <a title="right hiding of status bar" href="http://commits.kde.org/ktexteditor/a355d261a43c9094691ef2a7b824a538a624420d" target="_blank">part 2</a>, <a title="right showing of status bar" href="http://commits.kde.org/ktexteditor/911abc30e3787a5332c6ede9f5aa6376631917a1" target="_blank">part 3</a>, <a title="fixup pixmap computation" href="http://commits.kde.org/ktexteditor/bb37e9e944394fedeeea9e1e894beffe800348e3" target="_blank">part 4</a>, <a title="fonts too small" href="http://commits.kde.org/ktexteditor/ae67dc7effbc9f31b097bf8bd3ec008f533c60f0" target="_blank">part 5</a>)
</li>
<li style="text-align: left;">
  work around QSpinBox <a title="Work around that QSpinbox suffix is not singular/pluralizable" href="http://commits.kde.org/ktexteditor/5aeb612f80dba8bf7b02c72238b5f7ba7dfc1d7c" target="_blank">pluralizable issues</a>
</li>
<li style="text-align: left;">
  revert quick open <a title="revert: Even more quick way to switch to a previous document" href="http://commits.kde.org/kate/3255ca929dd82e70a907cca66454e3a809c431bc" target="_blank">shortcut</a>
</li>
<li style="text-align: left;">
  fix <a title="fix viewChanged signals" href="http://commits.kde.org/kate/1bed751cd26ac57983d4d15309d5f4f6d41c8db2" target="_blank">viewChanged()</a> signals
</li>
<li style="text-align: left;">
  search & replace plugin: fix <a title="fix plugin signals" href="http://commits.kde.org/kate/1fa1ba07a38c157841c53aa5ca2f71322037a9f5" target="_blank">signals</a>
</li>
<li style="text-align: left;">
  <a title="we include a tabbar soon per default, if people want the other variants, they might port them from master, no need to waste our time" href="http://commits.kde.org/kate/9c2d2d709404058ccaa3ea82b6714f720320c684" target="_blank">remove all tab bar plugins</a> due to the built-in one
</li>
<li style="text-align: left;">
  <a title="sync syntax file changes from master" href="http://commits.kde.org/ktexteditor/6c19b27f84777671d4ab43d5a17df0bab15707c7" target="_blank">sync</a> syntax xml file changes from master
</li>
<li style="text-align: left;">
  Skanlite: fix <a title="SaveLocation: fix layout" href="http://commits.kde.org/skanlite/1ba15a3bc4a0291264b12df766badf5c34d265bc" target="_blank">layout</a>¸more <a title="Fix in SaveLocation. ShowImageDialog: use QDialog instead of KDialog." href="http://commits.kde.org/skanlite/31050787b5ae8edf0677c6c32701f8743d39c239" target="_blank">porting</a>, less <a title="SaveLocation: replace KComboBox with the Q one" href="http://commits.kde.org/skanlite/8b1a074524cdbfe01d1b8551b4d6e7b529462671" target="_blank">K classes</a>
</li>
<li style="text-align: left;">
  status bar: <a title="no resize between status bar, goto line and internal command line" href="http://commits.kde.org/ktexteditor/93a54cfdf076f7cb4826ec8ab9b90d9fbfcd7fe3" target="_blank">no resize </a>between status bar, goto line and internal command line, <a title="no resize between status bar, goto line and internal command line" href="http://commits.kde.org/ktexteditor/cd056e8eda8e197eecffb57bd1b05e3930aad98d" target="_blank">more</a>, <a title="no resize between status bar, goto line and internal command line" href="http://commits.kde.org/ktexteditor/782ec7fd896293b49c92eb3b05acda3410d38e16" target="_blank">more</a>¸<a title="no resize between status bar, goto line and internal command line and vi mode bars" href="http://commits.kde.org/ktexteditor/1b72757fb9fe1c21766235864c448770067decde" target="_blank">more</a>
</li>
<li style="text-align: left;">
  add <a title="add split actions to view space bar" href="http://commits.kde.org/kate/c65410cb03efbf7748aef9da68d16abc8cc358c5" target="_blank">split view actions</a> to view space navigation tab bar
</li>
<li style="text-align: left;">
  no document <a title="backport: no document duplicates in quick open" href="http://commits.kde.org/kate/b3526463273180f85a1574d05f97920b39c39d75" target="_blank">duplicates</a> in quick open
</li>
<li style="text-align: left;">
  <a title="Fix broken scrollbar when opening remote files" href="http://commits.kde.org/kate/807cac0e3f0899eb46f12de4b7b546f6cee9fba8" target="_blank">fix broken scroll bars</a> when opening remove files
</li>
<li style="text-align: left;">
  derive KateViewSpace from <a title="some cleanups" href="http://commits.kde.org/kate/42039be2bb58573afc6fd1544783600875697cc7" target="_blank">QWidget</a> instead of QFrame
</li>
<li style="text-align: left;">
  remove focus rect from <a title="we DON'T want a focus rect, like most other full size widgets" href="http://commits.kde.org/ktexteditor/71de642980b5da57bc871883e76e4b0c75caa4ec" target="_blank">view</a>
</li>
<li style="text-align: left;">
  Fix <a title="fix painting bug" href="http://commits.kde.org/kate/712e671041265164536cce5fda6b3b7bac1a12c4" target="_blank">painting bug </a>when scrolling while floating widgets are shown
</li>
<li style="text-align: left;">
  allow <a title="allow splitters to survive without views" href="http://commits.kde.org/kate/d946b239d0d84acada5aadc20dfdcc6a68cc380c" target="_blank">empty</a> view spaces
</li>
<li style="text-align: left;">
  cleanup of ViewManager and ViewSpace logic (<a title="use right createView, of viewmanager, not viewspace" href="http://commits.kde.org/kate/9830288878e756219dbb41d16ae13b2fc123299b" target="_blank">1</a>, <a title="for delayed update, disable repaints" href="http://commits.kde.org/kate/ac9d02b0267ebbfa85c10cbd624ff67901c9b636" target="_blank">2</a>, 3,
</li>
<li style="text-align: left;">
  extend <a title="Add ConfigIface keys: icon border, folding, lineno, modification markers" href="http://commits.kde.org/kate/2b581a487ed32dfaffc63e6c48df630637c5d41b" target="_blank">ConfigInterface</a>: icon-border-color, folding-marker-color, line-number-color, modification-markers
</li>
<li style="text-align: left;">
  Skanlite: bug finding in <a title="Found bug in QFileDialog KDE integration" href="http://commits.kde.org/skanlite/06ba636dd6b220a5e0f5e49c5e706fbe1e2a81e4" target="_blank">framworksintegration</a> module, fix <a title="SaveImageDialog: fix focus handling" href="http://commits.kde.org/skanlite/1a71f4fc5777ea92a25381e8cf97a14b565888e7" target="_blank">focus</a>
</li>
<li style="text-align: left;">
  status bar: make <a title="Indentation/spaces/tabs can now be configured from the status bar" href="http://commits.kde.org/ktexteditor/b0b1158790779695cb3c51e9a492ef8dfa4da28d" target="_blank">spaces and tabs</a> configurable
</li>
<li style="text-align: left;">
  use <a title="use popup menu in view space bar" href="http://commits.kde.org/kate/81ecf94bcb5964cd6eec21a84aa69669c3513488" target="_blank">popup</a> for split actions in view space bar
</li>
<li style="text-align: left;">
  show short cut in tool tip (<a title="show short cut in tool tips" href="http://commits.kde.org/kate/50b12866e5115b8886fc8def0e4cd8453c7816a2" target="_blank">cool one!</a>)
</li>
<li style="text-align: left;">
  remove KTE::View::<a title="remove not needed information message signal, now done internally with the ktexteditor messages, fixup statusbar" href="http://commits.kde.org/ktexteditor/5d602188905366d7ac6187c7305c20cb0e9320aa" target="_blank">informationMessage</a>() in favour of the MessageInterface
</li>

January, 23th:

<li style="text-align: left;">
  Kate, KWrite: show modification <a title="show the modification asterisk in the titlebar, although I'm unsure about that. The test [modified] would be better I think" href="http://commits.kde.org/kate/ebc06973bfbac3a2769fd4309252459a1c798d89" target="_blank">asterisk</a> in title bar
</li>
<li style="text-align: left;">
  view space tab bar: <a title="rename setTabURL to setTabToolTip" href="http://commits.kde.org/kate/7fa787bf4c3caa1aab9f79f07ca0a0ee6a2ab857" target="_blank">refactor</a>, <a title="rename setTabURL to setTabToolTip" href="http://commits.kde.org/kate/7fa787bf4c3caa1aab9f79f07ca0a0ee6a2ab857" target="_blank">polish</a>, fix <a title="fix sizing of view space bar switchers" href="http://commits.kde.org/kate/baf33965718f20785f3ed073a910b5fe6e51d281" target="_blank">sizing of tabs</a>, <a title="no one pixel border" href="http://commits.kde.org/kate/f8c3de208c11aaa9e616554508f51940c0cd244d" target="_blank">more</a>
</li>
<li style="text-align: left;">
  status bar: <a title="status bar no accels" href="http://commits.kde.org/ktexteditor/bb42531b0d3a730cf0774be03ff2df5097dd41df" target="_blank">polishing</a>, <a title="cleanup status bar once more" href="http://commits.kde.org/ktexteditor/18c71aae37f8404a1e8ab25134e1bf78b0a6dad7" target="_blank">cleanups</a>
</li>
<li style="text-align: left;">
  relicense <a title="relicense to LGPLv2+" href="http://commits.kde.org/kate/e6a42c64dd60ce15d6a9eb36ab6ae0d07eb1cea5" target="_blank">file tree</a>
</li>
<li style="text-align: left;">
  Kate&#8217;s view manager: <a title="small cleanups" href="http://commits.kde.org/kate/08010e22c0fc5e685e869e95463daa4130192dfd" target="_blank">cleanup</a>, more <a title="cleanup internal API" href="http://commits.kde.org/kate/87d5a0fdb44a5569db573f45fcff54b3791143eb" target="_blank">cleanup</a>¸<a title="cleanup internal API" href="http://commits.kde.org/kate/7ce75cee8304c2711fd82ff93ef8be41a9662f90" target="_blank">more</a>, <a title="cleanup internal API" href="http://commits.kde.org/kate/b9a8a8b8512d327b6552e52949f6096c4bb51068" target="_blank">more</a>
</li>
<li style="text-align: left;">
  KWrite: reformat to <a title="kdelibs coding style, like ktexteditor, astyle-kdelibs for the win" href="http://commits.kde.org/kate/8bb2c6d3ebadb6018ee109ba75263d9d667f3bbf" target="_blank">kdelibs coding style</a>
</li>
<li style="text-align: left;">
  Kate: reformat to <a title="kdelibs coding style, like ktexteditor, astyle-kdelibs for the win" href="http://commits.kde.org/kate/5ca1a06dc50572d0ac44d71a1d9a0ea084f58bd6" target="_blank">kdelibs coding style</a>
</li>
<li style="text-align: left;">
  set Document <a title="Set the document really read only if files have too long lines" href="http://commits.kde.org/kate/aa71f2c7105df0662c421b3caf3a7b9c4a0f5d5c" target="_blank">read-only</a> on too long lines wrap
</li>
<li style="text-align: left;">
  view space tab buttons: <a title="elide too long text" href="http://commits.kde.org/kate/430985175bfb26b4d30e7850e2db472baf30c250" target="_blank">elide</a> if too long text
</li>
<li style="text-align: left;">
  view space tab bar: add action &#8220;<a title="close other views action, too" href="http://commits.kde.org/kate/b182d5c0107b0e452acb25a29820b53c76191a5f" target="_blank">Close other Views</a>&#8220;
</li>
<li style="text-align: left;">
  <a title="better way to disable updates to avoid nasty flicker" href="http://commits.kde.org/kate/a355c5a1ba91c1fcff0f4faee0de1d4c880b64fa" target="_blank">avoid flicker</a> in Kate
</li>
<li style="text-align: left;">
  Kate: in fullscreen mode, <a title="KF5: If the window is fullscreen, show a toolbutton as a corner widget for the menubar." href="http://commits.kde.org/kate/dd2af34e0565201b90bba8892bf4e349c55042c5" target="_blank">show a button</a> in the corner of the menu bar to escape the fullscreen mode
</li>
<li style="text-align: left;">
  KTextEditor: <a title="In KF5 the jump to bracket and select to bracket actions are now shown in the Edit menu" href="http://commits.kde.org/ktexteditor/c27cd0eef1957467f0bd7b661cb3001d644481a6" target="_blank">add actions</a> jump to bracket and select to bracket
</li>
<li style="text-align: left;">
  API documentation: <a title="Split Mainpage.dox up" href="http://commits.kde.org/ktexteditor/49f259c728fe1930c38bb940c6f7a9306d879642" target="_blank">split docs</a>
</li>
<li style="text-align: left;">
  kate view space tab bar: implement <a title="implement correct document switching" href="http://commits.kde.org/kate/ab6b678d4d12c0369c37f13becc8b053a92711a3" target="_blank">correct document switching</a>
</li>
<li style="text-align: left;">
  tab bar: attempt for <a title="fix hiding and try better sizing" href="http://commits.kde.org/kate/ed08440085e3e24b3b79a359585b604a22d38a60" target="_blank">better sizing</a>, <a title="try to fix sizing issues" href="http://commits.kde.org/kate/5114139736a2bda7c5b6dac7f1fbb2935a4d1848" target="_blank">again</a>
</li>
<li style="text-align: left;">
  API documentation: update <a title="add note about status bar" href="http://commits.kde.org/ktexteditor/41953bf897906ccd1eef3fac478b461607846171" target="_blank">porting guide</a>
</li>
<li style="text-align: left;">
  better <a title="swap line numbers and mode" href="http://commits.kde.org/ktexteditor/67152a940c10e672be40a7907e740ec027a8b909" target="_blank">order</a> of elements in status bar¸<a title="not bold" href="http://commits.kde.org/ktexteditor/7319cec115df54f0c818fc79d10c1f23ad1fee92" target="_blank">not bold</a>
</li>
<li style="text-align: left;">
  Kate: turn SessionConfigWigdet into <a title="Add sessionsconfigwidge.ui and remove manual UI code. Add stub for &quot;Number of entries in recent file list&quot;" href="http://commits.kde.org/kate/7babcb82097e3823fa0a1eaa609c443a3ac6abc6" target="_blank">.ui-file</a>
</li>
<li style="text-align: left;">
  <a title="app/ => src/" href="http://commits.kde.org/kate/751431df3eaa48b96cd905c7fff11e28da0aad86" target="_blank">refactor</a> Kate source code
</li>
<li style="text-align: left;">
  attempt to make tab bar work as <a title="first fix attempt, still not all corner cases" href="http://commits.kde.org/kate/86ed12852fd2c22362792407d6566fa05ac9cdb5" target="_blank">expected</a>, <a title="more correct coping with for remove a view" href="http://commits.kde.org/kate/81dfc45eb2ee0dbad922064e1ad747ac233d7e3d" target="_blank">another one</a>
</li>
<li style="text-align: left;">
  Python plugin <a title="Solve the problem w/ toolviews destroy (first w/ color_tools)" href="http://commits.kde.org/kate/d8b0da4111325abc0b9657bef8d001e236b68560" target="_blank">updates</a>
</li>
<li style="text-align: left;">
  Kate View Space: allow to <a title="allow to have one splitter full-size" href="http://commits.kde.org/kate/a539ed4b8cb57472407c96e57a055666c07aa97b" target="_blank">maximize</a> a view space (cool!)
</li>
<li style="text-align: left;">
  refactor Kate View Space <a title="fix crash on open document if only one document that is untitled and fresh is around" href="http://commits.kde.org/kate/13917d21f6126df9f4a051926a1f6ceeaa9887e5" target="_blank">a bit</a>
</li>
<li style="text-align: left;">
  small cleanup of <a title="less view list iterations" href="http://commits.kde.org/kate/2d9d29992c9a8fbc9ee8cb7896295c11026fa27f" target="_blank">view manager</a>¸ <a title="less view list iterations" href="http://commits.kde.org/kate/c6cfde60c590b3a5a058d5cbafe3ee65ab5cb1e3" target="_blank">again</a>
</li>
<li style="text-align: left;">
  <a title="no stupid lists in kateviewmanager, still some linear lookups, thought" href="http://commits.kde.org/kate/b25f4c7e86f280645804379320f18fa7940322bb" target="_blank">performance</a>: use QHash instead of QList for lookups
</li>
<li style="text-align: left;">
  more <a title="start to remove more API" href="http://commits.kde.org/kate/6dc03faa59c772dd1b7785daf38cf24a403cf1e3" target="_blank">cleanups</a> with respect to view/document management, <a title="start to remove more API" href="http://commits.kde.org/kate/5b252b9cf92420e4777c4aa1be3410abf616340b" target="_blank">remove more API</a>, and <a title="start to remove more API" href="http://commits.kde.org/kate/1cef23651ee8a7ea68b3bb10b13a866a03e4c094" target="_blank">more removal</a>¸ <a title="start to remove more API" href="http://commits.kde.org/kate/3ed547be75ce9d67c381260d639d082768ef1ed4" target="_blank">more</a>
</li>
<li style="text-align: left;">
  fix bug in <a title="This should fix &quot;length of input parameter&quot; highlighting for bash" href="http://commits.kde.org/ktexteditor/5a5c7f37c061bf8b68346861057848df65268b7a" target="_blank">bash.xml</a>
</li>
<li style="text-align: left;">
  loading message for file with too long lines: provide button to reload <a title="The message that appears after loading, if lines have been wrapped " href="http://commits.kde.org/ktexteditor/abdfa809c6056941f0194911ac6a41c035919703" target="_blank">without limit</a>
</li>

January, 24th:

  * <a title="start to cleanup the view activation mess, less direct function calls, more relying on the signals we have anyway" href="http://commits.kde.org/kate/9b959ab779e2d329b833cde75aecc9241aa71581" target="_blank">huge refactor</a> of how document management works
  * fix <a title="start to cleanup the view activation mess, less direct function calls, more relying on the signals we have anyway" href="http://commits.kde.org/kate/0be717bce2493999df2c4e89801fc924a0e96503" target="_blank">view activation</a> workflow
  * avoid <a title="avoid empty view spaces" href="http://commits.kde.org/kate/dac9f442b343cc8de427dcd9916f290b908bc5fd" target="_blank">empty view spaces</a>
  * view manager: <a title="less public API" href="http://commits.kde.org/kate/caf582f34c6348226e1aa45151b4af32396602fa" target="_blank">less public api</a>
  * avoid <a title="deleteDoc itself shall not be ever called, else hell breaks loose " href="http://commits.kde.org/kate/ff4c8b11415ad88302eb59cf9c3d0a20539742a6" target="_blank">hell</a> breaking loose
  * document manager: <a title="less public API" href="http://commits.kde.org/kate/aada429e1f6824cd30644135f3034ea88ab47cba" target="_blank">less public api</a>, same for <a title="less public API" href="http://commits.kde.org/kate/0d0ff493c4e874367d987005886ba34f39bbe497" target="_blank">view manager</a>
  * <a title="cleanups" href="http://commits.kde.org/kate/842975244173a8fd455fc3740607020a77a267e7" target="_blank">include</a> cleanups
  * <a title="move plugins one level up" href="http://commits.kde.org/kate/9c3f3829aac6ce0f9ca83d673bfaefcb6f02d729" target="_blank">move plugins</a>, same for <a title="move plasma stuff up, too" href="http://commits.kde.org/kate/8a1f8af8656d866528a3364fcaa6a05ae344cf25" target="_blank">plasma launcher</a>
  * improve <a title="expanding tree widget: take the TextAlignment data role into account" href="http://commits.kde.org/ktexteditor/86f26238e78399a1bbff4583de29d21459b032ec" target="_blank">expanding tree</a> in code completion
  * tab bar: <a title="remove button id from buttons" href="http://commits.kde.org/kate/93ad7b68a1d86b469963eadd897c999155a92c90" target="_blank">huge</a> <a title="remove unused code, rename index to id" href="http://commits.kde.org/kate/6b58e3d4bf590d1030e58b107f01ab67fd04156f" target="_blank">code</a> <a title="do not tolerate wrong input: add asserts" href="http://commits.kde.org/kate/1876076d6f37d82258317556ae0352fb83c5451d" target="_blank">cleanup</a>, close action as <a title="allow to close a document via context menu" href="http://commits.kde.org/kate/016a61ecc4d83014506e356b77278a734bc20ee9" target="_blank">context menu</a>
  * <a title="ignore pure url drops" href="http://commits.kde.org/ktexteditor/91f48579ca146db20f221217a66a95e90c951996" target="_blank">avoid</a> recursive opening of folders
  * Kate: add Recent File List count <a title="Recent File List Entry Count: persist setting" href="http://commits.kde.org/kate/231f78a0cb7049da9ceea92e8a1c923ed7cbd133" target="_blank">setting</a>, <a title="apply RecentFilesMaxCount to m_fileOpenRecent" href="http://commits.kde.org/kate/a635753b302f754a27ea2ea911353b55e15e502e" target="_blank">more</a>¸and <a title="apply coding suggestions from REVIEW:115291" href="http://commits.kde.org/kate/f05f03556d44dd17c169b4eb31df33d452154519" target="_blank">done</a>
  * double click on Line/Column in status bar: open <a title="Statusbar: doubleclick on line/column label opens GoToLine" href="http://commits.kde.org/ktexteditor/e8978ecc0d6d3162d07aea135e956f794b70dbb1" target="_blank">goto bar</a>
  * status bar: double click toggles <a title="Statusbar: doubleclick on insert mode label toggles between INSERT and OVERWRITE." href="http://commits.kde.org/ktexteditor/37e61d64087158fee24855b0b58cd0b01da44483" target="_blank">INSERT/OVR</a>¸and <a title="clean code" href="http://commits.kde.org/ktexteditor/d95f9b016bced2c8687f5a7d65923d7742aa8198" target="_blank">cleanup</a>
  * Kate: <a title="remove even more flicker" href="http://commits.kde.org/kate/32d900bf9de6e6ff9ba791dc580fe11ef7786984" target="_blank">less flicker</a> caused by doc manager
  * <a title="remove useless API" href="http://commits.kde.org/ktexteditor/a2d7536689bdeb50780c3f0af7ece6c4488cc1d0" target="_blank">remove api</a> from editor
  * KateGlobal: <a title="less linear search in lists" href="http://commits.kde.org/ktexteditor/a1de48d6fd0dcde23da5d8bbbb4cbff9378a6996" target="_blank">less linear searches</a> in lists
  * view space tab bar: add <a title="add close buttons, better highlight for active tab" href="http://commits.kde.org/kate/2ab8be72e1ee900be298709f41567f947d03f286" target="_blank">close button</a>
  * no hard-coded <a title="no hardcoded plugins" href="http://commits.kde.org/kate/6f38002e2e12bbc6fa7ca7a783dfec7727ec1e51" target="_blank">plugin loading</a>
  * resort status bar <a title="resort status bar once more" href="http://commits.kde.org/ktexteditor/efeebc587fcbba68ff78d0e36493e5e1277bb060" target="_blank">once more</a> :-)
  * Kate Part&#8217;s default indentation is now <a title="more sane defaults for indentation" href="http://commits.kde.org/ktexteditor/ecd2deb5e32379d37ab4d55712da35dfde8f91d1" target="_blank">4 spaces</a> (fix <a title="adjust to new indentation defaults" href="http://commits.kde.org/ktexteditor/16c44229a4974087cba8c0921d1fa4a930582841" target="_blank">unit test</a>)
  * Kate View Space: <a title="merge tabs of closed viewspace into new active viewspace" href="http://commits.kde.org/kate/b2009c8357b56e1bcfd3c623352da96c25178f15" target="_blank">merge tabs</a> of closed view space into active view space
  * <a title="fix indent options" href="http://commits.kde.org/ktexteditor/a30476dc528cfcbd806906f8e0c5bf1cce22c490" target="_blank">fix</a> indent <a title="fix indent options" href="http://commits.kde.org/ktexteditor/668f9005a58316f555692d3982fff0a1bd04edf9" target="_blank">option</a> caused by indent width change
  * adapt message catalog to <a title="katepart4.pot => ktexteditor5.pot" href="http://commits.kde.org/ktexteditor/5e5ea3bf69e62f9a09324ebb385af88f20477413" target="_blank">ktexteditor5.pot</a>
  * cleanup <a title="sync" href="http://commits.kde.org/ktexteditor/cdf6a760f4897fc99001150feae8e780ba7128c5" target="_blank">TODO</a> list
  * Kate: <a title="merge show/hide single-view-mode" href="http://commits.kde.org/kate/b2f42a5150adbc3fda4e9c76c706f90c8d7eccd2" target="_blank">merge</a> show/hide single-view-mode
  * KTE::Document: use a <a title="use a hash" href="http://commits.kde.org/ktexteditor/e0d265d276c14a14932c141610be3d8706c7baec" target="_blank">hash</a>
  * Kate view space tab bar: allow to <a title="allow to show/hide tabs" href="http://commits.kde.org/kate/b7cffaee7e064f9f03ddc6f871b5988a5b8b13ff" target="_blank">hide tab bar</a>
  * KTextEditor plugins: allow with <a title="allow to list all apps that shall load the plugin per default" href="http://commits.kde.org/ktexteditor/675a750b1322c1cdae96fd94b713e508165c8e2e" target="_blank">X-KTextEditor-Load-Default</a> to add apps that load a plugin by default, implement in <a title="honor the 'load me per default if my app name is mentioned in the list' X-KTextEditor-Load-Default attribute" href="http://commits.kde.org/kate/e573c502f4297ffce0f71f7d17fa41517e3f4d22" target="_blank">Kate</a>
  * load Search & Replace plugin and File Tree by <a title="filelist and search in files per default" href="http://commits.kde.org/kate/654a1fa3e1c8089bbd9acd01eaf608c37dfc800c" target="_blank">default</a>
  * cleanup <a title="fix KTextEditor::TextHintInterface" href="http://commits.kde.org/ktexteditor/aff96872b052deea12beada0816db1e246751035" target="_blank">KTextEditor::TextHintInterface</a>
  * avoid crash on <a title="activate one stupid view at least or kate will fail only on the FIRST time ever started" href="http://commits.kde.org/kate/6d70b7b4300c99ea3719cfc9378d3230b9ad38ec" target="_blank">first</a> Kate start
  * status bar indent settings: <a title="fix event filter + Other -> &quot;Other...&quot;" href="http://commits.kde.org/ktexteditor/9d06984a613e5827c8fedb7ed3caea4f5d90f2ea" target="_blank">indicate dialog</a> with &#8230;

January, 25th (departure day):

  * nice breakfast with croissants, then to the airport :-)
  * double click in empty space in tab bar creates <a title="double click in empty space of tab bar crates new empty document" href="http://commits.kde.org/kate/06168866c65fa65267dd00a8cd8321230371968c" target="_blank">new document</a>
  * <a title="swap quick open and window splitting button" href="http://commits.kde.org/kate/bc992a9527c0c2d6c9b978849ec1d0d75fdfe2af" target="_blank">swap</a> quick open and window splitting buttons in view space tab bar
  * Projects plugin: only create tool views <a title="create tool views on demand only" href="http://commits.kde.org/kate/47fc7d5cbf377d9f1ebd7244710836bb5165561d" target="_blank">if applicable</a>
  * port some commits from <a title="build plugin: add support for Intel icpc error messages" href="http://commits.kde.org/kate/ae58159832573fdf70edc12e39827cff1ac5710a" target="_blank">KDE 4</a> branch to <a title="build plugin: fix stuff broken recently" href="http://commits.kde.org/kate/4bfbcf0bbf3c6e4375d1745c210a2d811e9f7fb9" target="_blank">KF5 branch</a>
  * allow detection of <a title="allow detection of encoding by HTML encoding" href="http://commits.kde.org/ktexteditor/91e1030a512910d120175bf519c1662a35cff68c" target="_blank">HTML encoding</a>
  * vi input mode: fix some <a title="vi-mode: fix visual mode tests" href="http://commits.kde.org/ktexteditor/b0e0f4550cc2ef0d0f7109ccea79283879190208" target="_blank">unit tests</a>¸more <a title="vi-mode: enable more tests" href="http://commits.kde.org/ktexteditor/83923f69d923c1fe443a3d12ab09cd9177d603bd" target="_blank">tests</a>
  * same for file tree <a title="try to fix filetreemodel listMode tests" href="http://commits.kde.org/kate/a3c1034f0ba0717c510f917b3c5244c192cdf687" target="_blank">unit tests</a>
  * Skanlite: <a title="more porting" href="http://commits.kde.org/skanlite/f586c4ef9200b217e869605fbf4b026cf669b73d" target="_blank">more porting</a>

In the days after the sprint we did a lot more fine tuning and cleanups with respect to the changes we did during the sprint. So let&#8217;s have a look at Kate before the sprint:

[<img class="aligncenter size-full wp-image-3044" alt="Kate on KF5" src="/wp-content/uploads/2013/12/kate_on_kf5.png" width="762" height="527" srcset="/wp-content/uploads/2013/12/kate_on_kf5.png 762w, /wp-content/uploads/2013/12/kate_on_kf5-300x207.png 300w" sizes="(max-width: 762px) 100vw, 762px" />][3]

&nbsp;

Kate after the sprint:

[<img class="aligncenter size-full wp-image-3164" alt="Kate after the Developer Sprint" src="/wp-content/uploads/2014/02/kate-2014-01.png" width="760" height="528" srcset="/wp-content/uploads/2014/02/kate-2014-01.png 760w, /wp-content/uploads/2014/02/kate-2014-01-300x208.png 300w" sizes="(max-width: 760px) 100vw, 760px" />][4]

So Kate changed in several ways:

  * New status bar: The status bar is in the KTextEditor interfaces now. That implies that KDevelop, Kile, and all other applications using the KTextEditor framework will have the same status bar.
  * It is now possible to change the indent settings (tabs, spaces) through the status bar. The same holds for the encoding and the current highlighting.
  * Double click on &#8220;Line: &#8230;, Column: &#8230;&#8221; switches into goto-mode (Ctrl+G).
  * Double click on INSERT changes to OVERWRITE mode, if not in vi input mode.
  * New Tab Bar in each view space: This tab bar shows the documents you are working on in a least recently used (LRU) fashion. It only shows as many tabs as fit into the tab bar, since we want to avoid horizonal scrolling (it does not scale). If not all documents fit into the tab bar, just use the Documents tab on the left, or the quick open icon in the view space tab bar bar on the right to to launch quick open.
  * Since we now have a tab bar, we can now show the splitting actions at a more prominent place on the very right. New features include to hide inactive views, which equals maximizing the current view space.
  * Yes, no worries, the tab bar can be disabled.

We&#8217;ll cover the workflow of the tab bar in a separate blog post.

&#8230;oh, and we have much more in the pipe (not related to the sprint) :-)

 [1]: http://commits.kde.org/ktexteditor/60a2de20ccf0ed98a1c39c2b9f65c04ade46e1e8 "remove complex KTE signal"
 [2]: http://commits.kde.org/libksane/43e238d2028c6a8e7b4e6ca42878356c81c95009 "remove compiler warnings"
 [3]: /wp-content/uploads/2013/12/kate_on_kf5.png
 [4]: /wp-content/uploads/2014/02/kate-2014-01.png