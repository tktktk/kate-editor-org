---
title: Interfacing Lua With Templates in C++11
author: Dominik Haumann

date: 2014-01-17T15:13:41+00:00
url: /2014/01/17/interfacing-lua-with-templates-in-c11/
categories:
  - Developers
tags:
  - planet

---
I thought I&#8217;d share a very interesting read about how to wrap the lua C bindings with C++11 variadic templates: <a title="Interfacing Lua With Templates in C++11" href="http://www.jeremyong.com/blog/2014/01/10/interfacing-lua-with-templates-in-c-plus-plus-11/" target="_blank">part 1</a>, <a title="Interfacing Lua With Templates in C++11 Continued" href="http://www.jeremyong.com/blog/2014/01/14/interfacing-lua-with-templates-in-c-plus-plus-11-continued/" target="_blank">part 2</a>, <a title="Interfacing Lua with C++11" href="http://www.jeremyong.com/blog/2014/01/21/interfacing-lua-with-templates-in-c-plus-plus-11-conclusion/" target="_blank">part 3</a>.

These type of cool articles pop up from time to time on <a title="Reddit C++" href="http://www.reddit.com/r/cpp" target="_blank">reddit/r/cpp</a>, so if you are interested in C++ topics, this is definitely worth to follow.