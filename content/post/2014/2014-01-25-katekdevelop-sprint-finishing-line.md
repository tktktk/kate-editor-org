---
title: Kate/KDevelop Sprint – Finishing Line
author: Christoph Cullmann

date: 2014-01-25T01:47:16+00:00
url: /2014/01/25/katekdevelop-sprint-finishing-line/
categories:
  - Common
  - Developers
  - Events
  - KDE
  - Users
tags:
  - planet

---
Saturday most Kate developers will depart, therefore now the most work has been finalized (or at least brought to some intermediate state that works OK).

Below, the mandatory screenshot, Kate started without any configuration set in the framework branch, new and shiny:

[<img class="size-full wp-image-3138 alignnone" alt="KF5 Kate after Sprint" src="/wp-content/uploads/2014/01/kate_kf5.png" width="890" height="677" srcset="/wp-content/uploads/2014/01/kate_kf5.png 890w, /wp-content/uploads/2014/01/kate_kf5-300x228.png 300w" sizes="(max-width: 890px) 100vw, 890px" />][1]Find the differences :P

And one screenshot with some files open:

[<img class="size-full wp-image-3144 alignnone" alt="KF5 Kate with Files" src="/wp-content/uploads/2014/01/kate_kf5_files.png" width="890" height="677" srcset="/wp-content/uploads/2014/01/kate_kf5_files.png 890w, /wp-content/uploads/2014/01/kate_kf5_files-300x228.png 300w" sizes="(max-width: 890px) 100vw, 890px" />][2]

 [1]: /wp-content/uploads/2014/01/kate_kf5.png
 [2]: /wp-content/uploads/2014/01/kate_kf5_files.png