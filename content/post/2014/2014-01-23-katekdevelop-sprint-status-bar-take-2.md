---
title: Kate/KDevelop Sprint – Status Bar Take 2
author: Christoph Cullmann

date: 2014-01-23T16:51:06+00:00
url: /2014/01/23/katekdevelop-sprint-status-bar-take-2/
categories:
  - Common
  - Developers
  - Events
  - KDE
  - Users
tags:
  - planet

---
After the first initial status bar integration into the KatePart, we thought a bit more about what users might expect. Looking at the competition, one common feature is to allow quick-switching the indentation settings.

Joseph gave that a try and we now have an even better version available in the KF5 KTextEditor framework.

Mandatory screenshot (KWrite, Kate or other applications using it will look the same), updated version after feedback below:[<img class="aligncenter size-full wp-image-3130" alt="KF5 Status Bar Take 2" src="/wp-content/uploads/2014/01/kwrite1.png" width="781" height="493" srcset="/wp-content/uploads/2014/01/kwrite1.png 781w, /wp-content/uploads/2014/01/kwrite1-300x189.png 300w" sizes="(max-width: 781px) 100vw, 781px" />][1](Update: Using now again non-bold + Line/Column swapped with status)[<img class="aligncenter size-full wp-image-3134" alt="KF5 Status Bar Take 2-2" src="/wp-content/uploads/2014/01/kwrite2.png" width="751" height="466" srcset="/wp-content/uploads/2014/01/kwrite2.png 751w, /wp-content/uploads/2014/01/kwrite2-300x186.png 300w" sizes="(max-width: 751px) 100vw, 751px" />][2]

 [1]: /wp-content/uploads/2014/01/kwrite1.png
 [2]: /wp-content/uploads/2014/01/kwrite2.png