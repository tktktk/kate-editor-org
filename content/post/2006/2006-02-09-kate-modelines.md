---
title: Kate Modelines
author: Christoph Cullmann

date: 2006-02-09T12:41:07+00:00
url: /2006/02/09/kate-modelines/
pw_single_layout:
  - "1"
categories:
  - Users

---
Kate Part’s modelines &#8211; also called **Kate document variables** &#8211; are Kate Part’s implementation of document variables, similar to <a title="Emacs Modelines" href="http://www.emacswiki.org/cgi-bin/wiki/FileLocalVariables" target="_blank">Emacs</a> and <a title="Vim Modelines" href="http://vimdoc.sourceforge.net/htmldoc/options.html#%27modelines%27" target="_blank">vim</a> modelines.

Document variables can be used to set Kate settings local to a file. This is for example for using another indenter than the one defined in the GUI settings. Or assume you work on two projects, the first indents with spaces and the second with tabs, then you can simply add a _modeline_ that contains the specific document variables that set the specific indentation properties.

The following contents is mainly from the <a title="Kate Handbook" href="http://docs.kde.org/stable/en/applications/kate/config-variables.html" target="_blank"><em>Kate Handbook</em></a>.

## Modeline Format

A modeline in the Kate Part has the following format:

    kate: VARIABLENAME VALUE; [ VARIABLENAME VALUE; ... ] 

The lines can be in a comment, in _C++_ for example in `/*...*/` sections. Multiple modelines are supported, i.e. modelines can be split over several lines for better visibility. _Variable_ names are single words (no whitespace), and anything up to the next semicolon is the _value_. The trailing semicolon (**;**) is _required_.

Here is an example of how a modeline could look like, forcing indentation settings for a C++, Java or JavaScript file:

    // kate: space-indent on; indent-width 4; mixedindent off; indent-mode cstyle; 

NOTE: Only **the first and last 10 lines** in a file are searched for modelines.

There are variables to support almost all options in Kate Part, and additionally plugins can use variables, in which case it should be documented in the plugin’s documentation.

## How Kate Part uses Variables

When reading settings, Kate Part looks in the following places in the order

  1. the global configuration, i.e. settings made in the user interface
  2. the <a title="Modes and Filetypes" href="/2006/02/09/kate-modes-filetypes/" target="_self">Modes & Filetypes</a> configuration
  3. the <a title=".kateconfig Files" href="/2006/02/09/kateconfig-files/" target="_self">.kateconfig file</a>
  4. the modelines in the document itself

This means Kate Part’s modelines have actually the highest precedence. Whenever a document is saved, the _document variables_ are reread, and will overwrite changes made using menu items or the <a title="The Command Line in the Kate Handbook" href="http://docs.kde.org/stable/en/kdesdk/kate/advanced-editing-tools-commandline.html" target="_blank">command line</a>.  
Any variable not listed below is stored in the document and can be queried by other objects such as plugins, which can use them for their own purpose.

## Available Modelines

The variables listed here documents Kate version 2.4. More variables may be added in the future. There are defied 3 types of values for variables, with the following valid expressions:

  * **BOOL** &#8211; on|off|true|false|1|0
  * **INTEGER** &#8211; any integer number
  * **STRING** &#8211; anything else

Available modelines are, in detail:

  * **auto-brackets** [BOOL]  
    Set auto insertion of brackets on or off.
  * **auto-center-lines** [INT]  
    Set the number of autocenter lines.
  * **auto-insert-doxygen** [BOOL]  
    Turn insertion of the leading asterisk in doxygen comments on or off. This has no effect unless you use the cstyle auto-indenter.
  * **background-color** [STRING]  
    Sets the document background color. The value must be something that can be evaluated to a valid color, for example _#ff0000_.
  * **backspace-indents** [BOOL]Turn backspace indenting on or off.
  * **block-selection** [BOOL]  
    Turn block selection on or off.
  * **bom** | **byte-order-marker** \[BOOL\] (new in Kate 3.4/KDE 4.4)  
    Enable/disable the <a title="Byte Order Mark on Wikipedia" href="http://en.wikipedia.org/wiki/Byte-order_mark" target="_blank">byte order marker</a> when saving files in unicode format (utf-8/utf-16/utf-32).
  * **bracket-highlight-color** [STRING]  
    Sets the color for the bracket highlight. The value must be something that can be evaluated to a valid color, for example _#ff0000_.
  * **current-line-color** [STRING]  
    Sets the color for the current line. The value must be something that can be evaluated to a valid color, for example _#ff0000_.
  * **default-dictionary** \[STRING\] (new in Kate 3.4/KDE 4.4)Sets the default dictionary used for spell checking.
  * **dynamic-word-wrap** [BOOL]  
    Turns dynamic word wrap on or off.
  * **eol** | **end-of-line** [STRING]  
    Sets the end of line mode. Valid settings are _unix_, _mac_ and _dos_
  * **folding-markers** [BOOL]  
    Set the display of folding markers on or off.
  * **font-size** [INT]Sets the point size of the document font.
  * **font** [STRING]  
    Sets the font of the document. The value should be a valid font name, for example _courier_.
  * **hl** | **syntax** [STRING]Set the syntax highlighting. Valid strings are all the names you can find in the menus. For instance, for C++ you write _C++_.
  * **icon-bar-color** [STRING]  
    Sets the icon bar color. The value must be something that can be evaluated to a valid color, for example _#ff0000_.
  * **icon-border** [BOOL]  
    Set the display of the icon border on or off.
  * **indent-mode** [STRING]  
    Set the auto-indentation mode. The options _none_, _normal_, _cstyle_, _haskell_, _python_, _lilypond_, _lisp_ are recognized. See the section Using Automatic Indenting for details.
  * **indent-width** [INT]  
    Sets the indentation width.
  * **keep-extra-spaces** [BOOL]  
    Set weather to keep extra spaces when calculating indentation width.
  * **keep-indent-profile** \[BOOL\] (Removed in Kate 3, i.e. since KDE4)  
    If enabled, prevents unindenting a block if at least one line has no indentation.
  * **line-numbers** [BOOL]  
    Set the display of line numbers on or off.
  * **mixed-indent** [BOOL]  
    Set mixed indentation ala Emacs on or off.  
    **DEPRECATED for Kate 3.0** From version 3.0 this variable is ignored, set `replace-tabs on;` instead for that version.
  * **overwrite-mode** [BOOL]  
    Set overwrite mode on or off.
  * **persistent-selection** [BOOL]  
    Set persistent selection on or off.
  * **remove-trailing-space** [BOOL]  
    Set dynamic end of line cleanup on or off.
  * **replace-tabs-save** [BOOL]  
    Set tab->space conversion on save on or off.
  * **replace-tabs** [BOOL]  
    Set dynamic tab->space conversion on or off.
  * **replace-trailing-space-save** [BOOL]  
    Set end of line cleanup on save on or off.
  * **scheme** [STRING]  
    Set the color scheme. The string must be the name of a color scheme that exists in your configuration to have any effect.
  * **selection-color** [STRING]  
    Set the selection color. The value must be something that can be evaluated to a valid color, for example _#ff0000_.
  * **show-tabs** [BOOL]Set the visual TAB character on or off.
  * **smart-home** [BOOL]  
    Set smart home navigation on or off.
  * **space-indent** [BOOL]  
    Set indentation with spaces on or off.  
    **DEPRECATED for Kate 3.0** — From Kate 3.0 mixed-indent is the default behavior, set `replace-tabs on;` to achieve space-only indentation.
  * **tab-indents** [BOOL]  
    Set the TAB key indentation on or off.
  * **tab-width** [INT]  
    Set the tab display width.
  * **undo-steps** [INT]  
    Set the number of undo steps to remember.
  * **word-wrap-column** [INT]  
    Set the hard word wrap width.
  * **word-wrap-marker-color** [STRING]  
    Set the work wrap marker color. The value must be something that can be evaluated to a valid color, for example `#ff0000`.
  * **word-wrap** [BOOL]  
    Set hard word wrapping on or off.
  * **wrap-cursor** [BOOL]  
    Set cursor wrapping on or off.