---
title: 'API documentation & refactored kdelibs/kdeui'
author: Dominik Haumann

date: 2006-12-27T23:05:00+00:00
url: /2006/12/28/api-documentation-refactored-kdelibskdeui/
blogger_blog:
  - dhaumann.blogspot.com
blogger_author:
  - dhaumannhttp://www.blogger.com/profile/06242913572752671774noreply@blogger.com
blogger_permalink:
  - /2006/12/api-documentation-refactored.html
categories:
  - KDE

---
During the last weeks/days the directory structure of especially kdelibs/kdeui got a major overhaul: in KDE 3 all files of a module were in the same directory which was more or less a mess as you did not know immediately which files belonged to the same category. [kdelibs/kdeui in KDE4][1] has a rather clean structure now (similar to the one in Qt) by using subfolders like

  * actions
  * dialogs
  * widgets
  * xmlgui
  * several others&#8230;

Compare this to [KDE3&#8217;s kdelibs/kdeui][2] structure. For KDE4 this is a huge benefit, as we have clearly defined groups. We already had lots of discussions in the past about API documentation and this is exactly where the new structure is important: In Qt every class usually belongs to a group ([example][3]). Our API documentation tool [doxygen][4] of course supports [grouping][5], and now it is even easy to know which class should be in which group. For instance, all classes in the widgets directory should be in the &#8216;<span style="font-style: italic;">Widgets</span>&#8216; group, and then maybe even more fine-grained divided into sub-groups.  
By the way, we have a policy that every widget in kdelibs has a screenshot to immediately see how it looks like &#8211; another nice way to get involved :)  
All in all this is really awesome and I&#8217;d like to thank all involved developers. Next prey is [kdelibs/kdecore][6]? =)

 [1]: http://websvn.kde.org/trunk/KDE/kdelibs/kdeui/
 [2]: http://websvn.kde.org/branches/KDE/3.5/kdelibs/kdeui
 [3]: http://doc.trolltech.com/4.2/groups.html
 [4]: http://www.stack.nl/%7Edimitri/doxygen/
 [5]: http://www.stack.nl/%7Edimitri/doxygen/grouping.html
 [6]: http://websvn.kde.org/trunk/KDE/kdelibs/kdecore/