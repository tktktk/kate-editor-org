---
title: .kateconfig Files
author: Christoph Cullmann

date: 2006-02-09T12:45:52+00:00
url: /2006/02/09/kateconfig-files/
categories:
  - Common
  - Users

---
Kate allows you to specify local _document variables_ by setting the corresponding [modelines][1]. The hidden file **.kateconfig** provides exactly the same functionality, but for all files in the directory.

### Enable the Feature

To use a _.kateconfig_ file you first have to enable the feature by invoking **Settings > Configure Kate**. In the config page **Open/Save** you can find the option _Search depth for config file_, which defaults to _Do not use a config file_. Change the value to an appropriate number,

  * __ means _look in the current folder_
  * _1_ means _look in the current folder, and if there is no .kateconfig file, look in the parent folder_
  * etc…

In short &#8211; Kate will search the number of given folder levels upwards for a _.kateconfig_ file and load the settings from it.

### Fill the .kateconfig File

The _.kateconfig_ file simply contains [modelines][1].

The following example will cause all documents to indent with 4 spaces with a tab width of 4. Tabs will be replaced during editing text and the end-of-line symbol is a linefeed (\n).

<pre>kate: space-indent on; tab-width 4; indent-width 4; replace-tabs on; eol unix;</pre>

### Extended Options in KDE 3.5.x, KDE 4 and Kate 5

Kate in KDE 4 as well as later versions of Kate always search for a .kateconfig file for local files (not remote files). In addition, it is now possible to set options based on wildcards (file extensions) as follows:

<pre>kate: tab-width 4; indent-width 4; replace-tabs on;
kate-wildcard(*.xml): indent-width 2;
kate-wildcard(Makefile): replace-tabs off;</pre>

In this example, all files use a tab-width of 4 spaces, an indent-width of 4 spaces, and tabs are replaced expanded to spaces. However, for all *.xml files, the indent width is set to 2 spaces. And Makefiles use tabs, i.e. tabs are not replaced with spaces. Wildcards are semicolon separated, i.e. you can also specify multiple file extensions as follows:

<pre>kate-wildcard(*.json;*.xml): indent-width 2;</pre>

Further, you can also use the mimetype to match certain files, e.g. to indent all C++ source files with 4 spaces, you can write :

<pre>kate-mimetype(text/x-c++src): indent-width 4;</pre>

Note: Next to the support in .kateconfig files, wildcard and mimetype dependent document variables are also supported in the files itself as comments.

 [1]: /?p=236