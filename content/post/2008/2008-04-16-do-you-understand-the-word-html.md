---
title: Do you understand the word HTML?
author: Dominik Haumann

date: 2008-04-16T09:39:00+00:00
url: /2008/04/16/do-you-understand-the-word-html/
blogger_blog:
  - dhaumann.blogspot.com
blogger_author:
  - dhaumannhttp://www.blogger.com/profile/06242913572752671774noreply@blogger.com
blogger_permalink:
  - /2008/04/do-you-understand-word-html.html
categories:
  - Users

---
During the [Kate developer meeting][1] we also thought about simplifying KWrite and how to make the decision whether KWrite should be launched in full featured mode or in a stripped version. &#8230;well, and we found a really funny idea:

<a onblur="try {parent.deselectBloggerImageGracefully();} catch(e) {}" href="http://3.bp.blogspot.com/_JcjnuQSFzjw/SAXLaO1HblI/AAAAAAAAABE/bKJ9-I4nb6k/s1600-h/P1020901.JPG"><img style="margin: 0px auto 10px; display: block; text-align: center; cursor: pointer;" src="http://3.bp.blogspot.com/_JcjnuQSFzjw/SAXLaO1HblI/AAAAAAAAABE/bKJ9-I4nb6k/s400/P1020901.JPG" alt="" id="BLOGGER_PHOTO_ID_5189777797132283474" border="0" /></a>  
Note, that this would even work, the question would be rather annoying, though :) The solution right now is to always start KWrite in a simple mode. Mostly only actions are hidden in the menus (@distributors: <span style="font-family: courier new;">kdelibs/kate/data/katepartsimpleui.rc</span>), but you can also change c++ code at Kate part level, as there are some functions:

  * bool KateDocument::simpleMode() (kate part internal), and
  * bool KTextEditor::Editor::simpleMode() (along with setSimpleMode())

This way config dialogs can be adapted depending on the mode as well. Ah, and if you want the normal mode back, go into the KWrite settings and enable <span style="font-family: courier new;">[x] Enable Developer Mode</span>. Then restart KWrite.

PS: Tackat, we came up with this image before our phone call. That&#8217;s why it was really funny when you said HTML is something that should not be removed. hehe&#8230; :)

 [1]: http://dot.kde.org/1208269318/