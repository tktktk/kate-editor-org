---
title: 'Kate: Fast backtrace navigation'
author: Dominik Haumann

date: 2008-08-12T15:13:00+00:00
url: /2008/08/12/kate-fast-backtrace-navigation/
blogger_blog:
  - dhaumann.blogspot.com
blogger_author:
  - dhaumannhttp://www.blogger.com/profile/06242913572752671774noreply@blogger.com
blogger_permalink:
  - /2008/08/kate-fast-backtrace-navigation.html
categories:
  - Common

---
I&#8217;ve added a new plugin to kdesdk/kate/plugin: a backtrace browser. It&#8217;s meant for developers and probably of no use for users. What does it do? It shows a backtrace delivered by gdb in a listview in a Kate toolview. Clicking on an item opens the selected file and jumps to the correct line number. It works for backtraces generated on your own machine, but it will also work for backtraces from other people, i.e. with /home/dummy/qt-copy/&#8230;/qwidget.cpp will still be found on other machines. For that to work, you have to index the directories where the source code is located.  
Sometimes there are several files with the same name, e.g.

  * trunk/kdegraphics/okular/generators/dvi/config.h
  * trunk/kdepim/mimelib/mimelib/config.h

To pick the right choice, the plugin picks the last two parts of the url, in this case this would be

  * dvi/config.h
  * mimelib/config.h

and then usually finds the correct one. Indexing trunk/KDE and branches/KDE/4.1 of course will lead to a clash, now way to fix it. Maybe I could present a list of valid files to the user and let the user pick the right one. I don&#8217;t think that&#8217;s necessary though for now.

<span style="font-weight: bold;">How to configure</span>

  1. Enable the plugin: go to Settings > Configure Kate > Application Plugins and enable &#8216;Kate Backtrace Browser&#8217;<a onblur="try {parent.deselectBloggerImageGracefully();} catch(e) {}" href="http://4.bp.blogspot.com/_JcjnuQSFzjw/SKGpTC4siKI/AAAAAAAAABU/RSgBUgutmPU/s1600-h/btbrowser-enable.jpg"><img style="margin: 0px auto 10px; display: block; text-align: center; cursor: pointer;" src="http://4.bp.blogspot.com/_JcjnuQSFzjw/SKGpTC4siKI/AAAAAAAAABU/RSgBUgutmPU/s400/btbrowser-enable.jpg" alt="" id="BLOGGER_PHOTO_ID_5233650386638506146" border="0" /></a>
  2. A config page appeared, so click on it and add the directories containing the source code<a onblur="try {parent.deselectBloggerImageGracefully();} catch(e) {}" href="http://2.bp.blogspot.com/_JcjnuQSFzjw/SKGpf0SzDyI/AAAAAAAAABc/LY7RWRi9Wd0/s1600-h/btbrowser-configure.jpg"><img style="margin: 0px auto 10px; display: block; text-align: center; cursor: pointer;" src="http://2.bp.blogspot.com/_JcjnuQSFzjw/SKGpf0SzDyI/AAAAAAAAABc/LY7RWRi9Wd0/s400/btbrowser-configure.jpg" alt="" id="BLOGGER_PHOTO_ID_5233650606059753250" border="0" /></a>
  3. Clicking OK will start indexing. It will take some time (the index of kdesupport + kdelibs + kdepimlibs + kdebase + kdesdk + playground/plasma + plasma-addons + kdevplatform + kdegraphics is about 6MB)

When indexing is finished, open the toolview &#8220;Backtrace Browser&#8221;. Now you can load a backtrace from the clipboard (e.g. when you clicked &#8220;Copy to Clipboard&#8221; in Dr. Konqi) or from a file.  
<a onblur="try {parent.deselectBloggerImageGracefully();} catch(e) {}" href="http://1.bp.blogspot.com/_JcjnuQSFzjw/SKGpsTLocuI/AAAAAAAAABk/dj-7kU8BV2E/s1600-h/btbrowser-usage.jpg"><img style="margin: 0px auto 10px; display: block; text-align: center; cursor: pointer;" src="http://1.bp.blogspot.com/_JcjnuQSFzjw/SKGpsTLocuI/AAAAAAAAABk/dj-7kU8BV2E/s400/btbrowser-usage.jpg" alt="" id="BLOGGER_PHOTO_ID_5233650820509627106" border="0" /></a>  
Hope it&#8217;s useful :)