---
title: Improving Syntax Highlighting Files
author: Dominik Haumann

date: 2018-03-10T20:54:47+00:00
url: /2018/03/10/improving-syntax-highlighting-files/
categories:
  - Developers
  - Users
tags:
  - planet

---
When building the KSyntaxHighlighting framework, the syntax highlighting xml files are compiled into the KSyntaxHighlighting library. In order to do so, we have a small little helper program that generates an index of all xml files. This indexer also validates the xml files against the XML Schema, and performs some more sanity checks.

[Review request D10621][1] tries to extend the indexer even further and suggest optimizations for our highlighting files. For instance, the rule

<pre>&lt;<strong>AnyChar</strong> context="#stay" <strong>String</strong>="&space;" attribute="Normal Text" /&gt;</pre>

should be replaced by the faster rule

<pre>&lt;<strong>DetectChar</strong> context="#stay" <strong>char</strong>="&space;" attribute="Normal Text" /&gt;</pre>

Similarly, the rule

<pre>&lt;RegExpr attribute="Normal" context="conditionNot" String="<strong>\b</strong>not<strong>\b</strong>" lookAhead="true" insensitive="true"/&gt;</pre>

should be replaced by the much faster rule

<pre>&lt;<strong>WordDetect</strong> attribute="Normal" context="conditionNot" String="not" lookAhead="true" insensitive="true"/&gt;</pre>

The proposed patch above generates [more than 1500 suggestions to improve our highlighting files,][2] so a lot of work. Help would be very much appreciated. So if you would like to contribute to KDE and are looking for simple work to do, then feel free to get started by sending improved highlighting files to us via [phabricator.kde.org ][3](click &#8220;Code Review&#8221; on the left, and then &#8220;Create Diff&#8221; on the top right &#8211; or even better [use arc][4] to automatically manage your patches). Oh, and please increase the version number in the xml files whenever you provide a patch :-)

 [1]: https://phabricator.kde.org/D10621
 [2]: https://paste.kde.org/p7iareuxc#line-1
 [3]: https://phabricator.kde.org/
 [4]: https://community.kde.org/Infrastructure/Phabricator