---
title: Kate - Color Themes with Frameworks 5.75
author: Christoph Cullmann
date: 2020-09-13T14:57:00+02:00
url: /post/2020/2020-09-13-kate-color-themes-5.75/
---

### Frameworks 5.75 => KSyntaxHighlighting Themes

[Last week](/post/2020/2020-09-06-kate-color-themes/) I reported about the currently done changes to the color themes in KTextEditor (Kate/KWrite/KDevelop/...).
As this was just the half finished job, I felt somehow motivated to work on the remaining parts.

I had more time this week to work on it than thought and finalized the transition from the old "schema" stuff we had to pure use of KSyntaxHighlighting themes.
This is [a rather large change](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests/21/diffs), I guess one of the largest code changes I did in KTextEditor in the last years beside the porting of the highlighting itself to KSyntaxHighlighting.
This change touches a lot stuff that is more than one decade old.

Important:
This port needs some small fixes in KSyntaxHighlighting.
If you want to try current KTextEditor master branch, please ensure you have a up-to-date KSyntaxHighlighting master copy, too!

### What does that mean for users?

From Frameworks 5.75 on, KTextEditor will not use any KConfig based schema config.
It will rely on the JSON based theme format of KSyntaxHighlighting for the color themes.

You will need to adjust your configuration once after this update.
The configuration dialog is fixed to now allow the manipulation of the new theme JSON files.
As can be seen below, bundled themes are now marked as read-only and you will need to copy them to change their settings.

<p align="center">
    <a href="/post/2020/2020-09-13-kate-color-themes-5.75/images/kate-configure-themes.png" target="_blank"><img width=700 src="/post/2020/2020-09-13-kate-color-themes-5.75/images/kate-configure-themes.png"></a>
</p>

The new configuration default is to choose some theme that matches your currently set application palette.
This means finally per default if you e.g. switch over to some dark theme in KDE or your other favorite desktop environment, KTextEditor based applications will follow and select some dark color theme.

At the moment there is no automatic conversion code for any old configuration, that is unfortunately non-trivial.
The old configuration will not be purged and will stay e.g. on Linux in *~/.config/kateschemarc* and *~/.config/katesyntaxhighlightingrc*.
KTextEditor will just no longer touch these files.
That means there can be still converters implemented to recover stuff, [patches are very welcome](/post/2020/2020-07-18-contributing-via-gitlab-merge-requests/).

As a side note:
If you miss the font configuration in the *Color Themes* section:
This got decoupled, themes don't contain the font, that is now configured on the *Appearance* configuration page as can be seen below.

<p align="center">
    <a href="/post/2020/2020-09-13-kate-color-themes-5.75/images/kate-configure-font.png" target="_blank"><img width=700 src="/post/2020/2020-09-13-kate-color-themes-5.75/images/kate-configure-font.png"></a>
</p>

This has the nice side effect that color theme changes no longer change the current font or font zooming like before.
This was rather unintended.

### JSON theme format?

KSyntaxHighlighting reads color themes from some simple JSON format.

At the moment there is no extensive documentation beside the bundled themes we provide in [this folder](https://invent.kde.org/frameworks/syntax-highlighting/-/tree/master/data/themes).

The format should be easy to understand, beside this, as mentioned above, the KTextEditor configuration dialog will now allow to just configure this stuff via the UI.

As the new theme format does per default store one theme in one file, this is much easier to archive/version/distribute than the old storage that mixed all configured stuff in two large configuration files.

Local themes are per default stored e.g. on Linux in *~/.local/share/org.kde.syntax-highlighting/themes*.

### More bundled themes wanted?

With 5.75 (at least as of today), we will ship seven default themes with KSyntaxHighlighting that can be used out of the box with any KTextEditor based application.

<p align="center">
    <a href="/post/2020/2020-09-13-kate-color-themes-5.75/images/kate-bundled-themes.png" target="_blank"><img width=700 src="/post/2020/2020-09-13-kate-color-themes-5.75/images/kate-bundled-themes.png"></a>
</p>

For more details see [the themes overview page](/themes/) with some show case rendering of all provided themes.

I think that is already a good start but for sure many users would be happy to have more themes available per default.

If you want to help out, please submit MIT licensed themes to us.
There are a lot of popular themes available under MIT that just need to be converted into our format.

See e.g. the [contributing via merge requests post](/post/2020/2020-07-18-contributing-via-gitlab-merge-requests/) how to contribute to our stuff.

If you have feedback, [use this thread on reddit](https://www.reddit.com/r/kde/comments/irxttq/kate_color_themes_with_frameworks_575/).
