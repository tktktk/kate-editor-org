---
title: Kate is 20 years old!
author: Christoph Cullmann
date: 2020-12-14T18:38:42+01:00
url: /post/2020/2020-12-14-kate-is-20-years-old/
---

## Kate just turned 20 today!

As [announced before](/post/2020/2020-11-08-kate-is-soon-20-years-old/), Kate turned twenty today.

## When did it start?

Here again my initial request to the KWrite maintainer about my work on a 'MDI KWrite' later called Kate (sorry, German & spelling mistakes):

<pre>From: Cullmann Christoph &lt;crossfire@babylon2k.de&gt;
To: digisnap@cs.tu-berlin.de
Subject: KWrite - Verbesserungsvorschläge
Date: Thu, 14 Dec 2000 18:38:42 +0100

Hallo
Ich benutze KWrite regelmässig um Quellcode zu bearbeiten und das
Syntaxhighlighting ist sehr praktisch.
Es wäre jedoch schön wenn KWrite eine MDI-Oberfläche hätte.
Ich baue gerade eine und falls jemand Interesse hat können sie sich ja melden.

Danke und Tschö
Christoph Cullmann</pre>

Funny enough, if you look at the timestamp of the mail, it got mailed out at "18:38:42".
I was 18 then, now I am 38 and 42 is always a good number, I couldn't have faked that timestamp better!

## A lot of people helped!

A long time passed since that mail and a lot of contributors did join and leave the team around KCEdit/Kant/Kate/KTextEditor/KSyntaxHighlighting/... during that time.

[Our incomplete auto-generated list](/the-team/) contains more than 550 people that contributed stuff in these past 20 years.

Thanks to all of them (and to everybody that got forgotten & the wider KDE community).

## Recent contributions?

Since we got our GitLab [invent.kde.org](https://invent.kde.org) instance, we already merge:

* 115 patches for [Kate](https://invent.kde.org/utilities/kate/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
* 41 patches for [KTextEditor](https://invent.kde.org/frameworks/ktexteditor/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)
* 113 patches for [KSyntaxHighlighting](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged)

And yes, these numbers did increase since my [pre-announcement of the birthday in November](/post/2020/2020-11-08-kate-is-soon-20-years-old/).

Let's keep the patches arriving, improvements are always welcome!

But yes, we shall be truthful, stuff might be rejected, you can look at the non-merged closed stuff on the above pages, too.

## Supported Platforms?

For large parts of its lifetime, Kate worked nicely on Unices with X11 (or now Wayland), but not so well on anything else.

Since some years now, we have actually "good" support for Windows, too.
Since more than at [least 4 years](/2016/01/28/kate-on-windows/) we provide usable Windows installers for Kate and since more than one years we are in the [official store](/post/2019/2019-09-12-kate-in-the-windows-store/).

Even for macOS there are usable builds, but help to maintain & polish them would be very welcome.
Their current state is unfortunately far away from what we have on Windows.

You can get information about where to get the current releases [here](/get-it/).

## Bugs, Bugs, Bugs :(

Not all is well in Kate's world, we have "A LOT" of bugs that need love.

* [172 open bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&list_id=1821013&product=frameworks-ktexteditor&product=kate)
* [120 open wishlist items](https://bugs.kde.org/buglist.cgi?bug_severity=wishlist&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&list_id=1821014&product=frameworks-ktexteditor&product=kate)

If you have time and want to help out but you have no own itch to scratch => take a look at these lists.

Unfortunately a lot of these issues/enhancements are not a 5 minute job.

Btw., that we actually get regular new bug reports shows that people actually really use the stuff we create ;=)
If Kate & Co. would be some dead project, we would get neither new bugs nor wishes.

## Who cares, show colorful pictures!

Yep, who cares, look at Kant aka Kate in 2001, KDE 2.2.x:

<p align="center">
    <a href="/wp-content/uploads/2011/08/kate-in-kde2.2.png" target="_blank"><img width=700 src="/wp-content/uploads/2011/08/kate-in-kde2.2.png"></a>
</p>

Yep, no anti-aliasing of fonts, pixels everywhere.
Btw., I your read the [article](/2011/08/11/kate-turning-10-years-old/) from that time, we actually postulated a different birthday for Kate there.
But I think backdating it to the initial mail is actually more "correct".
We should accept that Kate went through some renames in her past ;)

Some KDE 4.x version in 2010, after ~10 years of evolution (show-casing the [SQL plugin](/2010/07/29/katesql-a-new-plugin-for-kate/)):

<p align="center">
    <a href="/wp-content/uploads/2010/07/katesql-screenshot-1.png" target="_blank"><img width=700 src="/wp-content/uploads/2010/07/katesql-screenshot-1.png"></a>
</p>

And now, 2020:

<p align="center">
    <a href="/post/2020/2020-11-08-kate-is-soon-20-years-old/images/kate-2020.png" target="_blank"><img width=700 src="/post/2020/2020-11-08-kate-is-soon-20-years-old/images/kate-2020.png"/></a>
</p>

The future? That's not yet written. => That's up to your contributions!

## Summary?

I am astonished that the project I started is still there and I stayed with it, too.

I would not have thought that something I began working on at the age of 18 is still used daily at work by my 20 years older version ;=)

I learned a lot during my work on Kate and other stuff inside the KDE community.

I think the trips to [KDE e.V.](https://ev.kde.org/) meetings and [KDE conferences](https://akademy.kde.org/) were actually some of the first travels I went on "alone" as a student after I finished school.
I guess one of them was my first flight, too.
And they all were very nice experiences.

I hope that others that contributed to the project had a good time as well.

Let's try together to keep Kate (and it's underlying Framework parts) useful for a lot of people in the future, too.

Stay healthy, have a nice Christmas (if you celebrate that) and a good start in the new year.
Might the next year bring better fortune than this one.

## Comments?

Matching threads for this are here on [r/KDE](https://www.reddit.com/r/kde/comments/kd2b2t/kate_is_20_years_old/) & [r/Linux](https://www.reddit.com/r/linux/comments/kd2alx/the_kate_text_editor_is_20_years_old/).
