---
title: Kate - Telemetry / User Feedback
author: Christoph Cullmann
date: 2020-02-09T17:33:00+02:00
url: /post/2020/2020-02-09-kate-telemetry/
---

Starting with the upcoming 20.04 release (or current master branch builds), Kate allows the user to turn on some telemetry submission.
This is a purely *opt-in* approach.
We will not submit any data if you not *actively enable* this feature!

Like [Plasma](http://www.proli.net/2020/01/17/learning-about-our-users/) we use the [KUserFeedback](https://api.kde.org/frameworks/kuserfeedback/html/index.html) framework for this task.

For details about how we handle the data, refer to [KDE’s Applications Privacy Policy](https://kde.org/privacypolicy-apps.php) and specifically the [Telemetry Policy](https://community.kde.org/Policies/Telemetry_Policy).

We documented what kind of info we submit on the [Telemetry Use](https://community.kde.org/Telemetry_Use) page.

At the moment we collect maximal this information:

* Application version: to know which application versions are still in use
* Qt version: to know the underlying Qt version (e.g. to see how important workarounds for old issues are)
* Platform information: to know the operating system we are running on
* Screen parameters: to find out how common multi-screen usage is
* Start count: to find out how frequent users use our editor
* Usage time: to find out how many users are using the software heavily vs in passing

If you want to review the code that was used to add this, take a look at our [merge request](https://invent.kde.org/utilities/kate/merge_requests/60) for the initial addition of this feature.

If you spot errors in the code, please inform us, either via mail to [kwrite-devel@kde.org](mailto:kwrite-devel@kde.org) or via a bug report at [bugs.kde.org](https://bugs.kde.org).

I myself activated now the telemetry submission for my installations of the master branch version, this is possible via the *Settings -> Configure Kate... -> User Feedback* dialog page.

<p align="center">
    <a href="/post/2020/2020-02-09-kate-telemetry/images/kate-telemetry-user-feedback.png" target="_blank"><img width=500 src="/post/2020/2020-02-09-kate-telemetry/images/kate-telemetry-user-feedback.png"></a>
</p>

I encourage you to do the same, if you want to provide us feedback which Kate versions are out in the wild and a bit about how often and long they are used.

In the future we might add some hint somewhere in the UI to ask once to take a look at the telemetry config page in a *non-intrusive* way.
As we still need to think about how to do this in the least annoying way, at the moment no such hint is given at all.

I hope our very conservative approach to this shows that we value the privacy of our users and are not branded as *"yet another spyware application"* or get plenty of *"Kate editor spies on users"* stories.
