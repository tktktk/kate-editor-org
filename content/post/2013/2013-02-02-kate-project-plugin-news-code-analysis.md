---
title: Kate Project Plugin News – Code Analysis
author: Christoph Cullmann

date: 2013-02-02T20:28:58+00:00
url: /2013/02/02/kate-project-plugin-news-code-analysis/
pw_single_layout:
  - "1"
categories:
  - Common
  - Developers
  - KDE
  - Users
tags:
  - planet

---
The project plugin in kate.git master has now the ability to call <a title="cppcheck" href="http://cppcheck.sourceforge.net/" target="_blank">cppcheck</a>.

This is just hacked in at the moment and needs more love, but it works.  
Feel free to contribute integration of other code checkers, cppcheck is only the first, I hope ;)

[<img class="aligncenter size-large wp-image-2218" title="kate_project_code_analysis" src="/wp-content/uploads/2013/02/kate_project_code_analysis-1024x586.png" alt="" width="770" height="440" srcset="/wp-content/uploads/2013/02/kate_project_code_analysis-1024x586.png 1024w, /wp-content/uploads/2013/02/kate_project_code_analysis-300x171.png 300w, /wp-content/uploads/2013/02/kate_project_code_analysis.png 1047w" sizes="(max-width: 770px) 100vw, 770px" />][1]

 [1]: /wp-content/uploads/2013/02/kate_project_code_analysis.png