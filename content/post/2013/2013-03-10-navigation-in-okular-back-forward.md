---
title: 'Navigation in Okular: Back & Forward'
author: Dominik Haumann

date: 2013-03-10T15:18:19+00:00
url: /2013/03/10/navigation-in-okular-back-forward/
pw_single_layout:
  - "1"
categories:
  - Users
tags:
  - planet

---
<a title="The official Okular homepage" href="http://okular.kde.org/" target="_blank">Okular</a>, KDE&#8217;s <a title="Okular on UserBase" href="http://userbase.kde.org/Okular" target="_blank">universal document viewer</a>, has a really cool feature I&#8217;m using for years already: &#8220;Go > Forward&#8221; and &#8220;Go > Backward&#8221;. These two actions allow to quickly jump to positions in the document where you came from in a chronological order. Consider e.g. reading the phrase &#8220;As shown in [15], &#8230;&#8221;, and you want to know quickly lookup reference [15]. So you click on it, and okular will jump to the list of references. And &#8220;Go > Back&#8221; will bring you back to exactly the position where you came from.

Awesome :-D

In fact, I removed the &#8220;Previous&#8221; and &#8220;Next&#8221;-page buttons from the toolbar in favor of the &#8220;Back&#8221; and &#8220;Forward&#8221; feature, which is much more useful to me :) Given that my colleagues were not aware of this feature (all experts in LaTeX  and all using Kile and Okular), I thought to praise it here in a dedicated blog. <span style="text-decoration: line-through;">Interestingly, the <a title="Okular Handbook" href="http://docs.kde.org/stable/en/kdegraphics/okular/navigating.html" target="_blank">Okular Handbook</a> is missing this feature. Any takers to fix this? :-)</span> **Update:** This is also mentioned in the <a title="Go-Menu in Okular" href="http://docs.kde.org/stable/en/kdegraphics/okular/menugo.html" target="_blank">Okular handbook</a> :-)