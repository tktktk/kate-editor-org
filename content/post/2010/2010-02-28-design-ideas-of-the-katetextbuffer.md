---
title: Design Ideas of the Kate::TextBuffer
author: Christoph Cullmann

date: 2010-02-28T13:11:16+00:00
url: /2010/02/28/design-ideas-of-the-katetextbuffer/
categories:
  - Developers

---
## Storage

  * Basic idea: text stored as lines 
  * Lines hold text and additional data (like for highlighting) 
  * Advanced Concept: Stores lines in blocks of xxx lines, to have better performance

## Cursor Support

  * They will move on editing 
  * Cursors can be combined to ranges 
  * KateTextCursor will be stored in the buffer blocks

## Transactions

  * Each edit action must be encapsuled in a transaction 
  * startEdit/endEdit will do this 
  * Signals for starting and ending this transactions

## Revisions

  * After loading, buffer has revision 0 
  * Each edit action which is no nop will lead to increment in revision number 
  * Successful saving will reset revision back to 0

## Editing Operations (only 4 different editing primitives)

  * Insert Text (inside one line) 
  * Remove Text (inside one line) 
  * Wrap Line (wrap a line at given position, create a new line with content behind wrap position) 
  * Unwrap Line (unites the line with its predecessor) 
  * Signals for all of these actions, containing the change (to allow to layer undo/swap file support/&hellip;) on top of the buffer

## Load/Save & Encodings

  * Loading will try to use given codec, if that doesn&rsquo;t work, try to detect the codec by the BOM at start of file or use fallback codec, if that again doesn&rsquo;t work, given codec will be used again, and errors reported 
  * Saving will just use the given codec

## Unit Tests

  * Each of the above implementation details will be covered by unit tests 
  * KateTextBuffer (+KateTextLine and Cursor) must therefor be usable without other components