---
title: The Mission of KDE’s Wikis
author: Dominik Haumann

date: 2010-07-25T22:21:36+00:00
url: /2010/07/26/the-mission-of-kdes-wikis/
categories:
  - KDE
tags:
  - planet

---
KDE has three wikis: <a title="KDE TechBase" href="http://techbase.kde.org" target="_blank">TechBase</a>, <a title="KDE Community Wiki" href="http://community.kde.org" target="_blank">Community</a> and <a title="KDE UserBase" href="http://userbase.kde.org" target="_blank">UserBase</a>. The separation has the following meaning according to <a href="http://wiki.kde.org" target="_blank">http://wiki.kde.org</a>:

  * **TechBase**: The primary place for high quality technical information about KDE targeted at 3rd party developers, ISVs and system administrators.
  * **Community**: The working area for the KDE community. It provides a place for sharing information within the community and coordinating community teams.
  * **UserBase**: The home for KDE users and enthusiasts. It provides high quality information for end users on how to use KDE applications.

So TechBase is a source of mostly technical information. This includes step-by-step howtos for all sorts of KDE development as well as the feature plans and schedules for KDE releases and so forth. It&#8217;s _mainly static content_. Think of a howto for a Plasma Widget or a howto for building KDE. The content usually is valid for a long time, mostly even for _years_. For those of you longer in the KDE project, TechBase is the same as our good old developer.kde.org page (and we&#8217;ve never put arbitrary content there). The only difference is, that it&#8217;s now maintained as wiki.

UserBase is the same thing for users. It provides information about tips and tricks for KDE application, links to further pages an so on. It&#8217;s also rather static content. This is also why TechBase and UserBase both feature the &#8220;high quality&#8221; term.

This leaves us with the youngest of our wikis: the Community wiki. The purpose of the Community wiki is to give the KDE community a place where to coordinate. Think of leaving notes for thoughts. Or a list of contributors attending a conference. The current todo-list of a KDE project. In other words: **The community wiki is a scratch pad for all sorts of content that does not really fit into TechBase or UserBase. If you are unsure, the community wiki is most likely the right place.  
** 

Currently, there is a lot of content on TechBase that never was supposed to be put there. This is because the Community wiki did not exist earlier. So what should be moved from TechBase to the Community wiki? **All the articles in the <a title="Projects on TechBase" href="http://techbase.kde.org/Projects" target="_blank">Projects page</a> without exception belong in the Community wiki**. Just read through the list and you&#8217;ll get the impression that it&#8217;s more a dumping ground for some project related content. This is exactly what we NOT want on TechBase. However, people keep putting content there. So this is an appeal to the KDE community: **Please use the community wiki! You are free to do there whatever you want :)** We have to take care to keep TechBase maintainable (this is also why techbase uses the subpages to structure the content). Sometimes less is more, so please take the time to remove Projects pages no longer needed, or move them to the community wiki. And especially put new content to the Community wiki instead of TechBase. Thanks!!!