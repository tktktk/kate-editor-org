---
title: kate-editor.org moving
author: Christoph Cullmann

date: 2010-08-13T15:31:44+00:00
url: /2010/08/13/kate-editor-org-moving/
categories:
  - Common
tags:
  - planet

---
In the last weeks, the homepage was sometimes down, as the server it is on is slowly dying. I just negotiate the transfer to a new one. That one will allow additional stuff to be run, guess we than even have nightly unit test runs (at least I hope so) ;)

Therefor it may be, that the site is not reachable, as DNS transfer might always take some days.