---
title: 'Kate: Code Folding Crash'
author: Dominik Haumann

date: 2010-06-10T20:55:00+00:00
url: /2010/06/10/kate-code-folding-crash/
blogger_blog:
  - dhaumann.blogspot.com
blogger_author:
  - dhaumannhttp://www.blogger.com/profile/06242913572752671774noreply@blogger.com
blogger_permalink:
  - /2010/06/kate-code-folding-crash.html
categories:
  - Developers

---
We still have a [crash in Kate&#8217;s code folding code][1]; no one was able to find a proper fix, yet. So if you want to get your hands dirty, [just build Kate][2], find a fix and be the hero of all Kate developers :)

<span style="font-weight: bold;">Update: </span>Fixed by Stefan Schenk [in this commit for KDE 4.5][3]. Awesome! :)

 [1]: http://bugs.kde.org/show_bug.cgi?id=213964
 [2]: http://gitorious.org/kate/pages/Building%20Kate
 [3]: http://gitorious.org/kate/kate/commit/8db89fcffbb18a9001eedbc4d33256026fd5ae85