---
title: 'Developer Meeting: More on scripting Kate'
author: Dominik Haumann

date: 2010-02-17T13:57:00+00:00
url: /2010/02/17/developer-meeting-more-on-scripting-kate/
blogger_blog:
  - dhaumann.blogspot.com
blogger_author:
  - dhaumannhttp://www.blogger.com/profile/06242913572752671774noreply@blogger.com
blogger_permalink:
  - /2010/02/developer-meeting-more-on-scripting.html
categories:
  - Developers
  - Events

---
We are 10 people here at the developer meeting in Berlin. Kate, KDevelop as well as Okteta see a lot of commits. I&#8217;ll mainly talk about what&#8217;s happening in the holy Kate land, though :-)  
Yesterday I closed a bug requesting an &#8220;unwrap&#8221; feature in Kate that works like &#8220;Tools > Join Lines&#8221; but maintains paragraph separation, i.e., empty lines are not removed. This feature is implemented now in javascript. Further infos:

  * [the bug report][1]
  * [the javascript implementation][2]

To run the script simply switch to the command line (F7) and write &#8220;unwrap&#8221;. If you have further ideas about useful scripts, don&#8217;t hesitate to start hacking right away, see also

  * [how to extend Kate with scripting][3]

Fixes with regard to the scripting support in the last days are

  * [make reload-scripts work properly][4]
  * [fix command line completion for new scripts without restarting kate][5]
  * [support quoting in arguments for scripts][6]
  * [fix crash for invalid commands in scripts][7]

Those fixes will be in KDE 4.4.1. More to come in other blog entries :-)

 [1]: https://bugs.kde.org/show_bug.cgi?id=125104
 [2]: http://websvn.kde.org/trunk/KDE/kdelibs/kate/script/data/utils.js?r1=1091040&r2=1091039&pathrev=1091040
 [3]: http://docs.kde.org/stable/en/kdesdk/kate/advanced-editing-tools-scripting.html
 [4]: http://websvn.kde.org/?revision=1089691&view=revision
 [5]: http://websvn.kde.org/?revision=1089690&view=revision
 [6]: http://websvn.kde.org/?revision=1090086&view=revision
 [7]: http://websvn.kde.org/?revision=1091093&view=revision