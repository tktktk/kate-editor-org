---
title: KTextEditor depends on KSyntaxHighlighting
author: Dominik Haumann

date: 2017-01-23T21:02:51+00:00
url: /2017/01/23/ktexteditor-depends-on-ksyntaxhighlighting/
categories:
  - Developers
  - KDE
tags:
  - planet

---
Recently, the [KSyntaxHighlighting framework][1] was added to the KDE Frameworks 5.29 release. And starting with KDE Frameworks 5.29, KTextEditor depends on KSyntaxHighlighting. This also means that KTextEditor now queries KSyntaxHighlighting for available xml highlighting files. As such, the location for syntax highlighting files changed from <tt>$HOME/.local/share/katepart5/syntax</tt> to

<pre><strong>$HOME/.local/share/org.kde.syntax-highlighting/syntax</strong></pre>

So if you want to add your own syntax highlighting files to Kate/KDevelop, then you have to use the new location.

By the way, in former times, all syntax highlighting files were located somewhere in /usr/share/. However, since some time, there are no xml highlighting files anymore, since all xml files are compiled into the KSyntaxHighlighting library by default. This leads to much faster startup times for KTextEditor-based applications.

#### Running Unit Tests

If you build Kate (or KTextEditor, or KSyntaxHighlighting) from sources and run the unit tests (\``make test`\`), then the location typically is <tt>/$HOME/.qttest/share/org.kde.syntax-highlighting/syntax</tt>.

 [1]: /2016/11/15/ksyntaxhighlighting-a-new-syntax-highlighting-framework/