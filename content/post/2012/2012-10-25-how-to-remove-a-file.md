---
title: How to remove a file with Kate
author: Dominik Haumann

date: 2012-10-25T21:51:05+00:00
url: /2012/10/25/how-to-remove-a-file/
pw_single_layout:
  - "1"
categories:
  - Users
tags:
  - planet

---
KDE has all these little nifty features, and &#8211; guess what &#8211; Kate has them, too. Here, we&#8217;ll have a look at how to delete a file with Kate. We&#8217;ll start with opening the file we want to delete. Example:

<img class="aligncenter size-full wp-image-2050" title="Step 1: Open a file" src="/wp-content/uploads/2012/10/step1.png" alt="" width="582" height="402" srcset="/wp-content/uploads/2012/10/step1.png 582w, /wp-content/uploads/2012/10/step1-300x207.png 300w" sizes="(max-width: 582px) 100vw, 582px" /> 

Next, we open the menu File > Open With and choose Other&#8230; as follows:

<img class="aligncenter size-full wp-image-2051" title="Step 2: Open With..." src="/wp-content/uploads/2012/10/step2.png" alt="" width="560" height="506" srcset="/wp-content/uploads/2012/10/step2.png 560w, /wp-content/uploads/2012/10/step2-300x271.png 300w" sizes="(max-width: 560px) 100vw, 560px" /> 

We&#8217;re almost done: Type &#8216;rm&#8217; in the Open With dialog:

<img class="aligncenter size-full wp-image-2052" title="Step 3: Type rm" src="/wp-content/uploads/2012/10/step3.png" alt="" width="557" height="480" srcset="/wp-content/uploads/2012/10/step3.png 557w, /wp-content/uploads/2012/10/step3-300x258.png 300w" sizes="(max-width: 557px) 100vw, 557px" /> 

Now click OK to perform the action, et voila:

<img class="aligncenter size-full wp-image-2053" title="Step 4: File deletion complete" src="/wp-content/uploads/2012/10/step4.png" alt="" width="557" height="411" srcset="/wp-content/uploads/2012/10/step4.png 557w, /wp-content/uploads/2012/10/step4-300x221.png 300w" sizes="(max-width: 557px) 100vw, 557px" /> 

Now as you can see, Kate notifies you about the successful deletion. In case you change your mind, you can write it to disk again by clicking Overwrite. Reloading will tell you, that the file can indeed not be found, proving that Kate did exactly what you want. You can also ignore what you just did, which is probably the best choice. Don&#8217;t tell anyone :-)

PS: This tip comes from the KDevelop developers. So all Kudos and Cake go to them!  
PPS: We are working hard at the Kate/KDevelop sprint in Vienna to provide the best tools for our users!