---
title: On Removing an Element from a List
author: Dominik Haumann

date: 2012-03-17T12:10:11+00:00
url: /2012/03/17/on-removing-an-element-from-a-list/
pw_single_layout:
  - "1"
categories:
  - Developers
tags:
  - planet

---
Recently, there was a very good blog about <a title="The Importance of Mentoring" href="http://blog.martin-graesslin.com/blog/2012/01/the-importance-of-mentoring/" target="_blank">The Importance of Mentoring</a>. It was mentioned that the 3200 slocs of the gsoc projects could be cut down to 500 slocs, doing exactly the same thing.

While hunting some crashes in the new code folding code from the last gsoc project, I obviously had a closer look on how things are implemented. The code folding uses a simple tree structure. Nodes can be added, removed or moved around. So I stumbled over the following code:

> <pre>01 // removes it from children list (QVector)
02 bool SomeClass::removeItem(Item* it)
03 {
04   bool found = false;
05   int i;
06
07   for (i = 0 ; i &lt; children.size(); ++i) {
08     if (children[i] == it) {
09       found = true;
10       break;
11     }
12   }
13
14   if (found) {
15     children.remove(i);
16     return true;
17   }
18
19   return false;
20 }</pre>

This code appears several times in different locations. It can easily be rewritten as

> <pre>01 // removes it from children list (QVector)
02 bool SomeClass::removeItem(Item* it)
03 {
04   int i = children.indexOf(it);
05   if (i != -1) {
06     children.remove(i);
07   }
08
09   return i != -1;
10 }</pre>

So without much work, the code is reduced to the half. Diving further into the code, I stumbled over a class KateDocumentPosition. This class is a line/column tuple representing a position in the document. It features operators like <, >, for convenience. Now you may guess it: Kate Part is a _text editor_ using &#8220;document positions&#8221;all over the place, e.g. for the cursor, the text selection, bracket matching, search & replace and what not. In fact, there is no way around using line/column tuples as position markers. Thus, it should not be surprising that we have a public class called <a title="KTextEditor::Cursor" href="http://api.kde.org/4.x-api/kdelibs-apidocs/interfaces/ktexteditor/html/classKTextEditor_1_1Cursor.html" target="_blank">KTextEditor::Cursor</a>, featuring everything what KateDocumentPosition implements (and more). The Cursor class is basically used everywhere, and it works as expected (our unit test rely on it, too). There is no need to duplicate the code. This probably happened because the student was not aware of it.

Martin writes &#8220;_Be prepared for the worst_&#8220;. Well, true. In this case, the project was successful and the new code folding works better than the old one (after fixing the crashes). Now if you are a gsoc student reading this blog, don&#8217;t feel discouraged. Rather feel encouraged to communicate with the developers _a lot_, e.g. by discussions on the mailing list :-)