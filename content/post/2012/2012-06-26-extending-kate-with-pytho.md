---
title: Extending Kate with Python
author: shaheed

date: 2012-06-25T23:18:44+00:00
url: /2012/06/26/extending-kate-with-pytho/
pw_single_layout:
  - "1"
categories:
  - Common
tags:
  - python

---
So there I am, a [confirmed Kate user][1], and now I need to find my way around a codebase of over 100,000 files using [GNU ID utils][2]. The rather crude answer for Kate 3 was to write a wrapper around the lid(1) utility and the DCOP scripting interface of Kate, all driven from the built-in Konsole. The result was clunky but somewhat usable. But Kate 3? In 2010? Because the version of KDE on our primary development environment is the venerable KDE 3.3!

Eventually though, I decide the time had come for something a little nicer, and the answer seemed to lie in Kate 4 and scripting, except that the Javascript API didn&#8217;t seem to offer a way to get to the ID database. That had me stumped for a while until I came across [Pâté][3], which allowed Kate 4 to be extended in Python. Sadly, it was unmaintained and there things stalled until, inexorably, I got to the point where the itch needed scratching.

That most basic driver of Open Source usually comes with strings attached, like the need to find a way to work in a new codebase, and with a new team of hackers. Plus, in the this case, the minor issue of being pretty much a Python newbie. Luckily it turns out that Kate has one of the most responsive teams I&#8217;ve encountered, the Pâté code seemed reasonably tractable, and the Python C API solidly documented. What could possibly go wrong? :-) Not that much, because we now have:

  * A Kate plugin based on Pâté
  * A Python debugger console Pâté plugin
  * And oh yes, an ID file integration Pâté plugin

The Kate plugin has a configuration page that allows individual Python plugins to be enabled or disabled, and if need be, system installed plugins to be overridden. Like the original Pâté, Python plugins can hook into Kate&#8217;s menus and shortcuts. Unlike the original, this version:

  * Allows Python plugins to supply Kate configuration pages just like native Kate plugins
  * Uses Python pickling to save configuration data, so arbitrary Python objects can be part of a plugin&#8217;s configuration
  * Provides for reloading Python plugins, which is really handy when writing new plugins
  * Supported threaded Python
  * Provides direct access to the documentation for Python plugins (though if anybody can figure out how to be information for variables, as well as classes and function, that would be wonderful!)
  * The sample Console and ID plugins try to show a selection of techniques

But help is always welcome to do better (How \*should\* i18n be done? What might Python 3 involve? What about some decent developer docs?). Any takers or clues much appreciated!

 [1]: /2012/06/21/why-kde-and-kate/ "confirmed Kate user"
 [2]: http://www.gnu.org/software/idutils/ "GNU ID utils"
 [3]: http://paul.giannaros.org/pate/ "Pâté"