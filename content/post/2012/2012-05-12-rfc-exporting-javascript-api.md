---
title: 'RFC: Exporting JavaScript API'
author: Dominik Haumann

date: 2012-05-12T13:36:10+00:00
url: /2012/05/12/rfc-exporting-javascript-api/
pw_single_layout:
  - "1"
categories:
  - Developers
tags:
  - planet

---
Since quite some time, Kate Part has <a title="Kate Part JavaScript Support" href="http://docs.kde.org/stable/en/kde-baseapps/kate/advanced-editing-tools-scripting.html" target="_blank">build-in scripting support</a> through JavaScript. Our plan is to make this API public, so other applications like Kile, Kate App and KDevelop can use it. However, we are currently unsure how to best implement it, so this is a rfc to get feedback.

The bindings for a Kate Document are for instance located in part/script/katescriptdocument.h (<a title="KateScriptDocument header file" href="https://projects.kde.org/projects/kde/kde-baseapps/kate/repository/revisions/96db438c4ffe7b4b0c6439013defaf4a20892799/entry/part/script/katescriptdocument.h" target="_blank">header</a>, <a title="KateScriptDocument source file" href="https://projects.kde.org/projects/kde/kde-baseapps/kate/repository/revisions/96db438c4ffe7b4b0c6439013defaf4a20892799/entry/part/script/katescriptdocument.cpp" target="_blank">implementation</a>). As you can see, there are functions like

<pre>Q_INVOKABLE bool insertLine(int line, const QString &s),</pre>

which can be invoked in our scripting by a call of &#8216;document.insertLine(5, &#8220;hello world&#8221;)&#8217;. The API only contains basic functions. But for instance Kile maybe also wants to provide a function called &#8216;document.insertSection()&#8217; or similar LaTeX related functions. The question now is as follows: **How can Kile extend our QObject based prototype with their own QObject based classes?**

We do not want to make the class KateScriptDocument public. Instead, we just want to return a QScriptValue containing a QObject based KateScriptDocument. You can think of the problem also as follows:

<pre>// in Kile:
QScriptEngine *engine = ...;
KTextEditor::Document *kteDocument = ...;

QObject* kateScriptDocument = kteDocument-&gt;scriptDocument();
engine-&gt;globalObject().setProperty("document", engine-&gt;newQObject(kateScriptDocument));
// at this point, the JavaScript object 'document' contains all <a title="KateScriptDocument functions" href="https://projects.kde.org/projects/kde/kde-baseapps/kate/repository/revisions/96db438c4ffe7b4b0c6439013defaf4a20892799/entry/part/script/katescriptdocument.h" target="_blank">KateScriptDocument functions</a>

// next, we want to add the Kile related document functions
KileTextDocument* kileDocument = ...;
QObject* kileScriptDocument = kileDocument-&gt;...(); // some function that returns the binginds object

// now: how can we populate the 'document' property with the functions in kileScriptDocument?
engine-&gt;globalObject().setProperty("document", ? );</pre>

If you have any idea or other solutions how to do it right, please let us know!