---
title: Kate Banner Wanted
author: Dominik Haumann

date: 2011-08-17T16:39:59+00:00
url: /2011/08/17/kate-banner-wanted/
pw_single_layout:
  - "1"
categories:
  - Users
tags:
  - planet

---
As part of Christophs idea to [give Kate a new face][1], it would be nice also to have a Kate banner, that every Kate fan can use to show in the Web. Something like the following, but any ideas are welcome!

[<img class="aligncenter size-full wp-image-1371" title="Kate Banner" src="/wp-content/uploads/2011/08/kate-banner.png" alt="" width="304" height="92" srcset="/wp-content/uploads/2011/08/kate-banner.png 304w, /wp-content/uploads/2011/08/kate-banner-300x90.png 300w" sizes="(max-width: 304px) 100vw, 304px" />][2]

 [1]: /2011/08/15/kate-needs-a-face-artists-wanted/ "Kate's new Face?"
 [2]: / "Kate - KDE's Advanced Text Editor"