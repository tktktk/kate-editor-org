---
title: Dart / Flutter support in Kate
author: Waqar Ahmed
date: 2021-06-21T16:00:00+03:00
url: /post/2021/2021-06-21-dart-support/
---

So about a couple of weeks ago I decided it was time to try out [Flutter](https://flutter.dev) for desktop app development. If you don't know about Flutter, it's a UI framework for cross-platform app development by Google. After reading a little bit about Dart, I decided to dive in directly and write a basic app in Dart. To my surprise Kate didn't support Dart at all, there was no syntax-highlighting, no anything, not even Dart file detection. However, Kate does have an LSP Client which supports semantic highlighting but to my disappointment it turned out that our LSP Client didn't work too well with Dart's LSP server. This was enough, something needed to be done immediately. Here's the short plan I made to support Dart:

1. Add Dart support in our [syntax highlighting framework](https://invent.kde.org/frameworks/syntax-highlighting)
2. Add Dart server to our supported list of LSP servers
3. Fix the semantic highlighting for Dart

The first point was the most daunting one for me but it turned out that it was not that difficult after all. After a few hours I ended up with a basic syntax file for Dart which can provide basic syntax highlighting and allowed kate to detect `*.dart` files as Dart files. Soon after I submitted a [merge request](https://invent.kde.org/frameworks/syntax-highlighting/-/merge_requests/200) for it which was accepted within a few hours. However, I must say, this syntax highlighting is very basic. If you know Dart well, we would love your help in improving the syntax highlighting :)

So, after getting the syntax highlighting done Kate was at least able to recognize Dart files ;) Now we needed to get the LSP working. In almost all of the LSP servers that we support, a user usually doesn't need to provide any manual "initialization options" to the server but in case of Dart things were a bit different. As soon as I started the Dart server it would start analyzing my home directory which as you can imagine would freeze everything. After a bit of reading and discussion with Mark, I added the following option to the server which fixed the issue:

```json
    "initializationOptions": {
        "onlyAnalyzeProjectsWithOpenFiles": true
    }
```

Now almost everything i.e., completion, diagnostics, go to definition / references was working as expected *except* semantic highlighting. Syntax highlighting is something I care about very deeply so I needed to fix this asap. The LSP spec specifies two major ways to request a server for semantic highlighting, "range" or "full delta". Up to this point we only supported "full delta" but not "range" and the Dart server only supported "range" requests. The difference between "range" and "full delta" is that in range, you can specify a range inside a document for which you want highlighting but for full requests you get the highlighting for the full document always. In this way "range" requests are more efficient because of less amount of work. So, I went ahead and implemented the `textDocument/SemanticTokens/range` request and submitted a [merge request](https://invent.kde.org/utilities/kate/-/merge_requests/429). If you are wondering, what it looks like, here's a screenshot (*please ignore my poor Dart skills ;p*):

![img](/post/2021/2021-06-21-dart-support/images/dart.png)

---

## Configuring Kate for Dart language

Lastly, to make Dart lsp server work correctly in Kate, you may need to specify the path to server in settings. To do that, go to "**Settings**" -> "**Configure Kate**". Then choose "**LSP Client**" from the list on the left and under the "**User Server Settings**" tab, add the following:

```json
{
    "servers": {
        "dart": {
            "command": ["dart", "/path/to/dart/analysis_server.dart.snapshot", "--lsp"]
        }
    }
}
```

What the above does is *merge* this setting with "**Default Server Settings**". Of couse `dart` executable needs to be in your `$PATH`. If `analysis_server.dart.snapshot` is already in your path you can skip the above step. If you want to try this out immediately, you can get the nightly version [here](https://kate-editor.org/get-it/).

----

Are you a Dart developer? We would love your feedback. Also, we are actively trying to improve Kate always so, if you are interested in contributing or have ideas to share, you can find us [here](https://invent.kde.org/utilities/kate).
