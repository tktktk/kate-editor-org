---
title: KDE e.V. Windows Store Statistics
author: Christoph Cullmann
date: 2021-06-19T11:19:00+03:00
url: /post/2021/2021-06-19-kde-ev-windows-store-statistics/
---

Now that [Akademy 2021](https://akademy.kde.org/2021) has started, let's take a look at the current statistics of all applications in the Windows Store published with the KDE e.V. account.

### Last 30 days statistics

Here are the number of acquisitions for the last 30 days (roughly equal to the number of installations, not mere downloads) for our applications:

* [Okular - More than a reader](https://www.microsoft.com/store/apps/9N41MSQ1WNM8) - 5,674 acquisitions

* [Kate - Advanced Text Editor](https://www.microsoft.com/store/apps/9NWMW7BB59HW) - 3,701 acquisitions

* [Filelight - Disk Usage Visualizer](https://www.microsoft.com/store/apps/9PFXCD722M2C) - 1,390 acquisitions

* [Kile - A user-friendly TeX/LaTeX editor](https://www.microsoft.com/store/apps/9PMBNG78PFK3) - 573 acquisitions

* [Elisa - Modern Music Player](https://www.microsoft.com/store/apps/9PB5MD7ZH8TL) - 419 acquisitions

* [KStars - Astronomy Software](https://www.microsoft.com/store/apps/9PPRZ2QHLXTG) - 210 acquisitions

* [LabPlot - Scientific plotting and data analysis](https://www.microsoft.com/store/apps/9NGXFC68925L) - 104 acquisitions

A nice stream of new users for our software on the Windows platform.

### Overall statistics

The overall acquisitions since the applications are in the store:

* [Okular - More than a reader](https://www.microsoft.com/store/apps/9N41MSQ1WNM8) - 103,307 acquisitions

* [Kate - Advanced Text Editor](https://www.microsoft.com/store/apps/9NWMW7BB59HW) - 100,528 acquisitions

* [Filelight - Disk Usage Visualizer](https://www.microsoft.com/store/apps/9PFXCD722M2C) - 24,418 acquisitions

* [Kile - A user-friendly TeX/LaTeX editor](https://www.microsoft.com/store/apps/9PMBNG78PFK3) - 12,749 acquisitions

* [Elisa - Modern Music Player](https://www.microsoft.com/store/apps/9PB5MD7ZH8TL) - 5,796 acquisitions

* [KStars - Astronomy Software](https://www.microsoft.com/store/apps/9PPRZ2QHLXTG) - 5,759 acquisitions

* [LabPlot - Scientific plotting and data analysis](https://www.microsoft.com/store/apps/9NGXFC68925L) - 803 acquisitions

Okular and Kate finally went over the 100,000 acquisitions :=)

### Help us!

If you want to help to bring more stuff KDE develops on Windows, we have some meta [Phabricator task](https://phabricator.kde.org/T9575) were you can show up and tell for which parts you want to do work on.

A guide how to submit stuff later can be found [on our blog](/post/2019/2019-11-03-windows-store-submission-guide/).

Thanks to all the people that help out with submissions & updates & fixes!

If you encounter issues on Windows and are a developer that wants to help out, all KDE projects really appreciate patches for Windows related issues.

Just contact the developer team of the corresponding application and help us to make the experience better on any operating system.
