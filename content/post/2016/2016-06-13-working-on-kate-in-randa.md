---
title: Working on Kate in Randa
author: Dominik Haumann

date: 2016-06-13T16:12:59+00:00
url: /2016/06/13/working-on-kate-in-randa/
categories:
  - Developers
  - Events
  - Users
tags:
  - planet
  - sprint

---
Right now, around 40 developers are working together on bringing KDE to other platforms in Randa (Switzerland) for an entire week.

So far, I just fixed some bugs, e.g.:

  * <span class="im">Kate: Correctly restore view config of all split views <a href="http://commits.kde.org/kate/ced09c823aa5832ea5e56c5708a564a9767a225d">(commit</a>, </span>[bug][1], [review][2])
  * <span class="im">Kate: </span>Sort Kate Session Menu alphabetically ([commit][3], [bug][4], [review][5])
  * <span class="im">Kate: </span>Avoid hidden view spaces on start ([commit][6], [bug][7], [review][8])
  * <span class="im">Kate, KDevelop: </span>Tab Switcher Plugin: Make sure the height fits all items ([commit][9], [bug][10])

Much more work is going on, especially also with bringing Kate to [Mac OS][11], or making it work better on Windows. For instance, thanks to [Kåre and David][12], renaming documents already works now, and soon the filesystem browser plugin will be available in the next Kate on Windows release :)

If you want to support us, please [donate][13] to help us keeping up these developer meetings.

[<img class="alignnone" src="https://www.kde.org/fundraisers/randameetings2016/images/banner-fundraising2016.png" width="1400" height="200" />][13]

 [1]: https://bugs.kde.org/show_bug.cgi?id=353852
 [2]: https://git.reviewboard.kde.org/r/128163/
 [3]: http://commits.kde.org/kate/f5b909227941419f736cfd99bd13a46eb7d3eaa0
 [4]: https://bugs.kde.org/show_bug.cgi?id=364089
 [5]: https://git.reviewboard.kde.org/r/128165/
 [6]: http://commits.kde.org/kate/863e98662b584c137bb0f224d43bba724108667c
 [7]: https://bugs.kde.org/show_bug.cgi?id=358266
 [8]: https://git.reviewboard.kde.org/r/128173/
 [9]: http://commits.kde.org/kate/60fb10d22f17513f8179d4960fb4b2c5d765dea4
 [10]: https://bugs.kde.org/show_bug.cgi?id=354019
 [11]: /2016/06/13/katekwrite-application-bundle-update/
 [12]: http://commits.kde.org/kate/ad116d95aee1a193570931d21fb2069559f4feb9
 [13]: https://www.kde.org/fundraisers/randameetings2016/