---
title: 'Bug Triaging Guest Blog: The wandering bug triager was here'
author: Christoph Cullmann

date: 2016-08-12T20:10:15+00:00
url: /2016/08/12/bug-triaging-guest-blog-the-wandering-bug-triager-was-here/
categories:
  - Common
  - KDE
tags:
  - planet

---
I was contacted by Buovjaga from the [LibreOffice QA team][1] to spread the word about the current bug triaging efforts going on.

Therefore now a short guest block about that ;=)

Thanks to Buovjaga for trying to get some public interest in this important aspect of open source work: handling our flood of bugs!

<snip>

Free software needs more contributors from the non-coding user community. The problem is that there is no scientifically proven method for attracting them.

After dealing with thousands of LibreOffice bug reports and simultaneously trying to recruit more testers through various cunning plans, I realized the topic of free software quality assurance needs to become more prominent. I came up with the idea for a publicity stunt where I camp out at different bug trackers while telling the world about it. I chose KDE and Kate for my first month of triaging.

Bug triagers act as guardians of the bug tracker and save the developers from a lot of wasted time. If the number of unconfirmed reports in a project is not close to zero, it needs more triagers.

There were about 120 reports I deemed suitable for testing. I skipped crash debugging, testing of advanced features and only focused on the basic stuff. My take on the stats is that there is currently no one doing this sort of thing regularly for Kate.

To get started in triaging KDE bugs, you

&#8211; start testing unconfirmed reports and ancient confirmed ones while keeping the Bug triage guide at hand <span id="OBJ_PREFIX_DWT1325_com_zimbra_url" class="Object"><span id="OBJ_PREFIX_DWT1330_com_zimbra_url" class="Object"><a href="https://community.kde.org/Bugsquad/Guide_To_BugTriaging" target="_blank">https://community.kde.org/Bugsquad/Guide_To_BugTriaging</a></span></span>

&#8211; join #kde-bugs IRC channel after having commented on a bunch of reports and request rights to edit report details

&#8211; join the IRC channels of the products you are helping and coordinate testing with others <span id="OBJ_PREFIX_DWT1326_com_zimbra_url" class="Object"><span id="OBJ_PREFIX_DWT1331_com_zimbra_url" class="Object"><a href="https://userbase.kde.org/IRC_Channels#Applications" target="_blank">https://userbase.kde.org/IRC_Channels#Applications</a></span></span>

I believe a goal should be set for recruiting one person to deal with triaging per every actively-developed KDE product. After achieving this goal, it will quickly become apparent to each product team just how many triagers are needed in order for the work to be sustainable. However, the teams should not remain in isolation. The whole of KDE needs to become transparent. An “Everything about KDE” web dashboard would enable contributors to quickly see, which teams are in need of assistance. Tools for this are available in the Grimoire Lab suite <span id="OBJ_PREFIX_DWT1327_com_zimbra_url" class="Object"><span id="OBJ_PREFIX_DWT1332_com_zimbra_url" class="Object"><a href="http://grimoirelab.github.io/" target="_blank">http://grimoirelab.github.io/</a></span></span>. KDE sysadmin team does not have the resources to set them up, so if you have the necessary skills and want to make this happen, please contact them on the #kde-sysadmin IRC channel.

If you want to contact me, you can find me on the #libreoffice-qa channel @ Freenode network, nickname: buovjaga.

</snip>

 [1]: https://wiki.documentfoundation.org/QA/Team