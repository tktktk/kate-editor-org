---
title: Privacy and Hugo Themes
author: Christoph Cullmann

date: 2019-04-07T15:10:00+00:00
excerpt: |
  I use the &ldquo;Hugo Coder&rdquo; theme for this page.
  After creating the privacy page (Impressum), I did take a closer look at the generated HTML code to confirm I didn&rsquo;t miss to state things there.
  I thought, given Hugo generates plain static ...
url: /posts/privacy-and-hugo-themes/
enclosure:
  - |
    
    
    
syndication_source:
  - Posts on cullmann.io
syndication_source_uri:
  - https://cullmann.io/posts/
syndication_source_id:
  - https://cullmann.io/posts/index.xml
syndication_feed:
  - https://cullmann.io/posts/index.xml
syndication_feed_id:
  - "11"
syndication_permalink:
  - https://cullmann.io/posts/privacy-and-hugo-themes/
syndication_item_hash:
  - d7fb76caa30ae53f6c34516594709698
  - b217cf730dc221eaf74656efa2fb12a2
  - db279bd79b4152e5a2b0efb58cc77a9b
categories:
  - Common

---
I use the &ldquo;Hugo Coder&rdquo; theme for this page. After creating the privacy page (Impressum), I did take a closer look at the generated HTML code to confirm I didn&rsquo;t miss to state things there. I thought, given Hugo generates plain static HTML pages, I would be on the safe side. But unfortunately, the theme I use includes some external resources, like the Google web fonts&hellip; I now patched that out in my fork of the theme and provide local copies on my own server.