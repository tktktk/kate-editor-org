---
title: Add yourself to the relicense checker script
author: Dominik Haumann

date: 2015-02-21T10:28:54+00:00
url: /2015/02/21/add-yourself-to-the-relicense-checker-script/
categories:
  - Developers
tags:
  - planet

---
From the <a title="KDE Relicensing" href="https://techbase.kde.org/Projects/KDE_Relicensing" target="_blank">KDE relicensing page</a>:

> <p style="padding-left: 30px;">
>   A couple of KDE dependent projects or even libraries have moved or are going to move to GPLv3.
> </p>
> 
> <p style="padding-left: 30px;">
>   Unfortunately, GPL v3 is incompatible with GPL v2. This means that it is not possible to create a project linking GPL v2 and v3 code together. There is no problem for projects which are licensed GPLv2+ (version 2 or above).
> </p>
> 
> <p style="padding-left: 30px;">
>   A few parts of KDE are currently licensed as GPLv2 only. So far we have no reason to believe that this was something other than an oversight. However, we still need to validate with the individual copyright holders that a relicense to GPLv2+ or GPLv2+v3 is okay with them.
> </p>
> 
> <p style="padding-left: 30px;">
>   Therefore, in an effort we&#8217;re trying to identify the contributors that have contributed under the terms of GPLv2 and where the &#8220;+&#8221; part was not explicitly mentioned. If we know that all contributors agreed to a relicense, we can go ahead and flip the license of the individual source file.
> </p>

Short story: If you have not done so yet, **please add yourself to the kde <a title="KDE Relicense Checker" href="https://projects.kde.org/projects/kde/kdesdk/kde-dev-scripts/repository/revisions/master/entry/relicensecheck.pl" target="_blank">relicensecheck.pl</a> file!**