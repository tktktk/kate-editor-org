---
author: Christoph Cullmann
date: 2021-04-29 18:13:00+00:00
hideMeta: true
menu:
  main:
    parent: menu
    weight: 100
title: Mascote do Kate
---
A mascote do Kate - Kate, o Pica-pau Cibernético - foi desenhada por [Tyson Tan](https://www.tysontan.com/).

A evolução actual da nossa mascote foi revelada pela primeira vez [em Abril de 2021](/post/2021/2021-04-28-lets-welcome-kate-the-cyber-woodpecker/).

## Kate, o Pica-pau Cibernético

![Kate, o Pica-pau Cibernético](/images/mascot/electrichearts_20210103_kate_normal.png)

## Versão sem texto de rodapé

![Kate, o Pica-pau Cibernético - Sem Texto](/images/mascot/electrichearts_20210103_kate_notext.png)

## Versão sem fundo

![Kate, o Pica-pau Cibernético - Sem Fundo](/images/mascot/electrichearts_20210103_kate_transparent.png)

## Licenças

Em relação ao licenciamento, citando o Tyson:

> Faço aqui a doação do novo Kate, o Pica-pau Cibernético ao projecto do Editor Kate. Os gráficos têm uma licença dupla de LGPL (ou igual à do Editor Kate) e Creative Commons BY-SA. Desta forma, não precisam de me pedir autorização antes de usar/modificar a mascote.

## História do desenho do Kate, o Pica-pau Cibernético

Perguntámos ao Tyson se era possível partilhar as versões intermédias que fomos trocando para mostrar a evolução e ele concordou e ainda escreveu alguns breves resumos sobre os passos individuais. Isto inclui a mascote inicial e o ícone que se ajusta a esta evolução do desenho. Como tal, iremos citar o que ele escreveu sobre os diferentes passos em baixo.

### 2014 - Kate, o Pica-pau

![Versão de 2014 - Kate, o Pica-pau](/images/mascot/history/Kate_editor_mascot_woodpecker.png)

> A versão de 2014 foi desenvolvida talvez na minha pior fase como artista. Já não desenhava muito há anos, e estava quase à beira de desistir por completo. Surgiram algumas ideias interessantes nesta versão, nomeadamente as chavetas {} no peito dele, assim como o esquema de cores inspirado no realce de código em Doxygen. Contudo, a execução foi muito simplista -- nessa altura, parei deliberadamente de usar técnicas elaboradas que expunham as minhas competências básicas inadequadas. Nos anos seguintes, iria passar por um processo árduo de recompor as minhas bases artísticas auto-didactas do zero. Fiquei grato pela equipa do Kate ter mantido esta versão na sua página Web durante muitos anos.

### 2020 - O novo ícone do Kate

![2020 - O novo ícone do Kate](/images/mascot/history/kate-3000px.png)

> O novo ícone do Kate foi a única vez em que tentei usar gráficos vectoriais reais. Foi nessa altura que comecei a desenvolver algum sentido artístico ligeiro. Não tinha qualquer experiência no desenho de ícones, nem tinha qualquer disciplina em áreas como as proporções matemáticas. Cada forma e curva foi ajustada graças ao meu instinto como artista. O conceito era muito simples: é um pássaro com a forma da letra "K", e tinha de ser nítido. O pássaro era mais confuso até o Christoph referir isso, pelo que depois optei por uma forma mais arrumada.

### 2021 - Esboço da Mascote

![Versão de 2021 - Esboço da nova mascote](/images/mascot/history/electrichearts_20210103_kate_sketch.png)

> Neste esboço inicial, fui tentado a eliminar o conceito robótico e desenhar simplesmente um pássaro com bom aspecto.

### 2021 - Desenho da mascote

![Versão de 2021 - Remodelação da nova mascote](/images/mascot/history/electrichearts_20210103_kate_design.png)

> A remodelação da mascote do Kate prosseguiu após eu ter terminado o desenho do novo ecrã inicial para a próxima versão 5.0 do Krita. Tive a possibilidade de me esforçar ainda mais de cada vez que experimentava uma imagem nova nessa altura, pelo que continuei a manter o conceito robótico.. Ainda que as proporções ainda estivessem descontroladas, os elementos do desenho mecânico estavam mais organizados desta vez.

### 2021 - Mascote Final

![Versão de 2021 - Kate, o Pica-pau Cibernético Final](/images/mascot/electrichearts_20210103_kate_normal.png)

> A proporção foi ajustada depois de o Christoph mencionar esse ponto. A pose foi ajustada para ficar mais confortável. As asas forma desenhadas após estudar as asas de pássaros a sério. Foi desenhado um teclado bonito e detalhado à mão para reforçar a utilização principal da aplicação. Foi adicionado um fundo abstracto com um desenho tipográfico em 2D adequado. A ideia geral é: actualizado e artificial de forma inequívoca, mas também cheio de vida e humanidade.
