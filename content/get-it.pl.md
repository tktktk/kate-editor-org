---
author: Christoph Cullmann
date: 2010-07-09 14:40:05+00:00
discoverData:
  kate:
    alt: Wgraj Kate poprzez Odkrywcę lub inne sklepy za aplikacjami AppStream
    subtitle: przez Odkrywcę
    title: Wgraj Kate
  kwrite:
    alt: Wgraj KWrite poprzez Odkrywcę lub inne sklepy za aplikacjami AppStream
    subtitle: przez Odkrywcę
    title: Wgraj KWrite
hideMeta: true
menu:
  main:
    weight: 3
sassFiles:
- /sass/get-it.scss
title: Pobierz Kate
---
{{< get-it src="/wp-content/uploads/2010/07/Tux.svg_-254x300.png" >}}

### Linux i Uniksy

+ Wgraj [Kate](https://apps.kde.org/en/kate) lub [KWrite](https://apps.kde.org/en/kwrite) ze [swojej dystrybucji](https://kde.org/distributions/)

{{< get-it-button-discover >}}

Te przyciski działają tylko z [Odkrywcą](https://apps.kde.org/en/discover) i innymi sklepami z aplikacjami AppStream, takimi jak [Oprogramowanie GNOME](https://wiki.gnome.org/Apps/Software). Możesz także użyć programu do zarządzania pakietami swojej dystrybucji, takiego jak [Muon](https://apps.kde.org/en/muon) lub [Synaptic](https://wiki.debian.org/Synaptic).

{{< /get-it-button-discover >}}

+ [Pakiet Snap Kate w Snapcraft](https://snapcraft.io/kate)

{{< get-it-button link="https://snapcraft.io/kate" img="/images/snap-store-badge-en.png" width="200" alt=`Pobierz Kate ze sklepu Snap` />}}

+ [Stabilne Kate (64bit) jako AppImage](https://binary-factory.kde.org/job/Kate_Release_appimage/) *
+ [Rozwojowe Kate (64bit) jako AppImage](https://binary-factory.kde.org/job/Kate_Nightly_appimage/) *
+ [Zbuduj to](/build-it/#linux) ze źródła.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/Windows_logo_–_2012_dark_blue.svg_.png" >}}

### Windows

+ [Kate w Sklepie Microsoft](https://www.microsoft.com/store/apps/9NWMW7BB59HW)

{{< get-it-button link="https://www.microsoft.com/store/apps/9NWMW7BB59HW" img="/images/get-it-from-ms.png" width="200" alt=`Pobierz Kate ze sklepu Microsoftu` />}}

+ [Kate przez Chocolatey](https://chocolatey.org/packages/kate)
+ [Stabilna Kate (64bit) jako instalator](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Release_win64/) *
+ [Rozwojowa Kate (64bit) jako instalator](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Nightly_win64/) **
+ [Zbuduj to](/build-it/#windows) ze źródła.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/macOS-logo-2017.png" >}}

### macOS

+ [Stabilna Kate jako instalator](https://binary-factory.kde.org/view/MacOS/job/Kate_Release_macos/) *
+ [Rozwojowa Kate jako instalator](https://binary-factory.kde.org/view/MacOS/job/Kate_Nightly_macos/) **
+ [Zbuduj to](/build-it/#mac) ze źródła.

{{< /get-it >}}


{{< get-it-info >}}

**O wydaniach:** <br> [Kate](https://apps.kde.org/en/kate) oraz[KWrite](https://apps.kde.org/en/kwrite) są częścią [Aplikacji KDE](https://apps.kde.org), które są [wydawane zazwyczaj 3 razy do roku wszystkie razem](https://community.kde.org/Schedules). Silniki [edytowania tekstu](https://api.kde.org/frameworks/ktexteditor/html/) oraz [podświetlania składni](https://api.kde.org/frameworks/syntax-highlighting/html/) są dostarczane przez[Szkielety KDE](https://kde.org/announcements/kde-frameworks-5.0/), które są [uaktualniane co miesiąc](https://community.kde.org/Schedules/Frameworks). Nowe wydania są ogłaszane [tutaj](https://kde.org/announcements/).

\* **Stabilne** pakiety zawierają najświeższe wersje Kate oraz Szkieletów KDE.

\*\* **Rozwojowe** pakiety są codziennie kompilują się same z kodu źródłowego, stąd mogą się okazać niestabilne i zawierać błędy lub niedokończone funkcje. Są zalecane tylko do prób.

{{< /get-it-info >}}
