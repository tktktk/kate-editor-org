---
author: Christoph Cullmann
date: 2010-07-09 14:40:05+00:00
discoverData:
  kate:
    alt: 通过 Discover 或其他 AppStream 应用程序商店安装 Kate
    subtitle: 通过 Discover
    title: 安装 Kate
  kwrite:
    alt: 通过 Discover 或其他 AppStream 应用程序商店安装 KWrite
    subtitle: 通过 Discover
    title: 安装 KWrite
hideMeta: true
menu:
  main:
    weight: 3
sassFiles:
- /sass/get-it.scss
title: 获取 Kate
---
{{< get-it src="/wp-content/uploads/2010/07/Tux.svg_-254x300.png" >}}

### Linux 与 Unix

+ 从 [您的发行版](https://kde.org/distributions/) 安装 [Kate](https://apps.kde.org/en/kate) 或 [KWrite] (https://apps.kde.org/en/kwrite)

{{< get-it-button-discover >}}

这些按钮只适用于 [Discover](https://apps.kde.org/en/discover) 和其他 AppStream 应用程序商店，例如 [GNOME Software](https://wiki.gnome.org/Apps/Software)。您也可以使用您发行的软件包管理器，例如：[Muon](https://apps.kde.org/en/muon) 或 [Synaptic](https://wiki.debian.org/Synaptic)。

{{< /get-it-button-discover >}}

+ [Kate 在 Snapcraft 上的 Snap 软件包](https://snapcraft.io/kate)

{{< get-it-button link="https://snapcraft.io/kate" img="/images/snap-store-badge-en.png" width="200" alt=`在 Snap 商店获取 Kate` />}}

+ [Kate 的 release (64位) AppImage 软件包](https://binary-factory.kde.org/job/Kate_Release_appimage/) *
+ [Kate 的 nightly (64位) AppImage 软件包](https://binary-factory.kde.org/job/Kate_Nightly_appimage/) **
+ 从源码[构建](/build-it/#linux)。

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/Windows_logo_–_2012_dark_blue.svg_.png" >}}

### Windows

+ [微软商店上的 Kate](https://www.microsoft.com/store/apps/9NWMW7BB59HW)

{{< get-it-button link="https://www.microsoft.com/store/apps/9NWMW7BB59HW" img="/images/get-it-from-ms.png" width="200" alt=`从微软应用商店下载 Kate` />}}

+ [Chocolatey 上的 Kate](https://chocolatey.org/packages/kate)
+ [Kate release (64位) 安装包](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Release_win64/) *
+ [Kate nightly (64位) 安装包](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Nightly_win64/) **
+ 从源码[构建](/build-it/#windows)。

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/macOS-logo-2017.png" >}}

### macOS

+ [Kate release 安装包](https://binary-factory.kde.org/view/MacOS/job/Kate_Release_macos/) *
+ [Kate nightly 安装包](https://binary-factory.kde.org/view/MacOS/job/Kate_Nightly_macos/) **
+ 从源码[构建](/build-it/#mac)。

{{< /get-it >}}


{{< get-it-info >}}

**关于发布：** <br> [Kate](https://apps.kde.org/en/kate) 与 [KWrite](https://apps.kde.org/en/kwrite) 是 [KDE Applications](https://apps.kde.org) 的一部分，通常 [每年集体发布三次](https://community.kde.org/Schedules)。[文本编辑](https://api.kde.org/frameworks/ktexteditor/html/) 和 [语法高亮](https://api.kde.org/frameworks/syntax-highlighting/html/) 引擎由 [KDE 框架](https://kde.org/announcements/kde-frameworks-5.0/) 提供，[每月更新](https://community.kde.org/Schedules/Frameworks)。新发布将公布 [于此](https://kde.org/announcements/)。

\* **release** 软件包包含最新版本的 Kate 和 KDE 框架。

\*\* **nightly** 软件包每日自动从源代码编译，因此可能不是很稳定，并且包含缺陷或不完整的功能。仅推荐用于测试用途。

{{< /get-it-info >}}
