---
author: Christoph Cullmann
date: 2021-04-29 18:13:00+00:00
hideMeta: true
menu:
  main:
    parent: menu
    weight: 100
title: Kate 的吉祥物
---
Kate 的吉祥物叫做电子啄木鸟 Kate。她的设计者是 [Tyson Tan (谭代山/钛山)](https://www.tysontan.com/)。

吉祥物的当前版本发表于 [2021 年 4 月](/post/2021/2021-04-28-lets-welcome-kate-the-cyber-woodpecker/)。

## 电子啄木鸟 Kate

![电子啄木鸟 Kate](/images/mascot/electrichearts_20210103_kate_normal.png)

## 无注释文字版本

![电子啄木鸟 Kate - 无注释文字版本](/images/mascot/electrichearts_20210103_kate_notext.png)

## 无背景版本

![电子啄木鸟 Kate - 无背景版本](/images/mascot/electrichearts_20210103_kate_transparent.png)

## 许可证

吉祥物的许可证，引用 Tyson Tan 的原话：

> 我在此将新版的电子啄木鸟 Kate 捐赠给 Kate 文本编辑器项目。相关图像使用 LGPL (或者任意与 Kate 文本编辑器一致的许可证) 和知识共享署名-相同方式共享双重许可证发布。在对吉祥物进行使用和修改时无需征求我的同意。

## 电子啄木鸟 Kate 的设计过程

我们询问 Tyson 能否分享设计过程中的不同版本，他不但同意，还为每一版本撰写了简短说明，包括吉祥物的最初版本还有与吉祥物设计过程密切相关的新图标等。我们将在下文引用他的原话。

### 2014 年 - 电子啄木鸟 Kate

![电子啄木鸟 Kate - 2014 年版](/images/mascot/history/Kate_editor_mascot_woodpecker.png)

> 在设计 2014 年版本时，我恰逢处于画师生涯的创作低谷。当时我已经有相当长的时间没有怎么画画，处于放弃此项技能的边缘。此版设计也有一些有趣的设计元素，例如吉祥物胸口的花括号、基于 doxygen 代码高亮色的配色等。然而它的美术风格显得刻意的朴素——我当时有意地放弃了所有花哨的技法，专注于暴露自己美术功底的不足。在接下来的日子里，我会从零开始经历一段漫长而痛苦的重新学习和练习历程，以求打下更好的美术基础。我非常感激多年来 Kate 团队依然将此旧版设计保留在网站上。

### 2020 年 - 新版 Kate 图标

![2020 年 - 新版 Kate 图标](/images/mascot/history/kate-3000px.png)

> 新版 Kate 程序图标是我一生中首次真正绘制矢量图。我当时开始具备一些模糊的美术直觉。在没有任何图标设计经验，也没有遵循任何数学比例设计规范的情况下，我完全依靠自己的直觉去调整此图标的每一条曲线和每一块形状。图标的设计概念非常简单：它是一只鸟，它看起来像字母“K”，它要显得锋利、醒目、清爽。图标中鸟的形状曾经比最终版本要肥满许多，我听取 Christoph 的建议将它改瘦了。

### 2021 年 - 吉祥物草图

![电子啄木鸟 Kate - 2021 年草图](/images/mascot/history/electrichearts_20210103_kate_sketch.png)

> 新版吉祥物的最初草图中，我原本打算放弃机械化的设计理念，单纯绘制一只比较好看可爱的鸟儿。

### 2021 年 - 吉祥物设计

![电子啄木鸟 Kate - 2021 年设计途中](/images/mascot/history/electrichearts_20210103_kate_design.png)

> 在完成了 Krita 5.0 启动图的绘制后，我重新回到 Kate 吉祥物的设计工作上来。在这段日子里，我每画一张图都尽可能去突破自己当前的极限，于是我对于自己的能力更有自信，决心保留机械风格的设计。此设计过程图的比例不太舒服，但机设的基本面已经就位。

### 2021 年 - 吉祥物的最终设计

![电子啄木鸟 Kate - 2021 年最终版本](/images/mascot/electrichearts_20210103_kate_normal.png)

> 我在 Christoph 的建议下调整了角色的整体比例，并将姿势调整得更加舒服。在研究了真实鸟翼的构造后我重新绘制了翅膀。我还手绘了一个款式好看的键盘，用于关联 Kate 编辑器的主要用途，并制作了一个抽象化的、具有排版趣味的简易背景。整体的设计在呈现人工感和科技感的同时，也同时强调人物的生命力和人性化的元素。
