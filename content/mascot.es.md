---
author: Christoph Cullmann
date: 2021-04-29 18:13:00+00:00
hideMeta: true
menu:
  main:
    parent: menu
    weight: 100
title: Mascota de Kate
---
Kate, la ciberpájaro carpintero, la mascota de Kate, fue diseñada por [Tyson Tan](https://www.tysontan.com/).

La evolución actual de nuestra mascota se dio a conocer por primera vez [en abril de 2021](/post/2021/2021-04-28-lets-welcome-kate-the-cyber-woodpecker/).

## Kate, la ciberpájaro carpintero

![Kate, la ciberpájaro carpintero](/images/mascot/electrichearts_20210103_kate_normal.png)

## Versión sin texto de pie de página

![Kate, la ciberpájaro carpintero - Sin texto](/images/mascot/electrichearts_20210103_kate_notext.png)

## Versión sin fondo

![Kate, la ciberpájaro carpintero - Sin fondo](/images/mascot/electrichearts_20210103_kate_transparent.png)

## Licencia

Para la licencia, citando a Tyson:

> Por la presente, dono la nueva «Kate, la ciberpájaro carpintero» al proyecto del editor Kate. Los trabajos artísticos tienen doble licencia: LGPL (o la misma que tenga el editor Kate) y Creative Commons BY-SA. De este modo no es necesario que me solicite permiso antes de usar/modificar la mascota.

## Historia del diseño de Kate, la ciberpájaro carpintero

Le pregunté a Tyson si podía compartir las versiones intermedias que habíamos intercambiado para mostrar la evolución y estuvo de acuerdo, e incluso escribió breves resúmenes sobre los pasos individuales. Esto incluye la mascota inicial y el icono que encaja en esta evolución del diseño. Por lo tanto, a continuación solo cito lo que escribió sobre los diferentes pasos.

### 2014 - Kate, la pájaro carpintero

![Versión de 2014 - Kate, la pájaro carpintero](/images/mascot/history/Kate_editor_mascot_woodpecker.png)

> La versión de 2014 se realizó tal vez durante mi punto más bajo como artista. No había dibujado mucho desde hacía años y estuve a punto de rendirme por completo. Hubo algunas ideas interesantes en esta versión, como los corchetes {} en su pecho y el esquema de color inspirado en el resaltado de código de doxygen. Sin embargo, la ejecución fue muy simplista: en ese momento, dejé de usar deliberadamente técnicas sofisticadas para exponer mis inadecuadas habilidades básicas. En los años siguientes, atravesé un arduo proceso para reconstruir desde cero mi rota y autodidáctica base artística. Agradezco al equipo de Kate que haya mantenido esta versión en su sitio web durante tantos años.

### 2020 - El nuevo icono de Kate

![Versión de 2014 - Kate, la pájaro carpintero](/images/mascot/history/kate-3000px.png)

> La única vez que probé técnicas de dibujo vectorial reales fue para el nuevo icono de Kate. Fue en ese momento cuando comencé a desarrollar un leve sentido artístico. No tenía experiencia previa en el diseño de iconos y no había ninguna disciplina como la proporción guiada por las matemáticas. Modifiqué cada forma y cada curva usando mi instinto artístico. El concepto era muy sencillo: es un pájaro con la forma de la letra «K» y tiene que ser agudo. El pájaro era más robusto hasta que Christoph lo indicó, y después le di una forma más elegante.

### 2021 - Boceto de la mascota

![Versión de 2014 - Kate, la pájaro carpintero](/images/mascot/history/electrichearts_20210103_kate_sketch.png)

> En este boceto inicial, tuve la tentación de deshacerme del concepto robótico y dibujar simplemente un bonito y atractivo pájaro en su lugar.

### 2021 - Diseño de la mascota

![Versión de 2014 - Kate, la pájaro carpintero](/images/mascot/history/electrichearts_20210103_kate_design.png)

> El rediseño de la mascota de Kate se reanudó cuando terminé de dibujar la nueva imagen de la pantalla de bienvenida para el próximo Krita 5.0. Pude esforzarme más cada vez que creaba una nueva imagen en ese momento, por lo que estaba decidido a mantener el concepto robótico. Aunque la proporción estaba fuera de lugar, los elementos de diseño mecánicos se acuñaron más o menos en esta versión.

### 2021 - La mascota final

![Versión de 2014 - Kate, la pájaro carpintero](/images/mascot/electrichearts_20210103_kate_normal.png)

> La proporción se ajustó cuando Christoph lo indicó. También se ajustó la pose para que se sintiera más confortable. Las alas se volvieron a dibujar tras estudiar las alas de pájaro reales. Se dibujó a mano un vistoso y detallado teclado para enfatizar el principal uso de la aplicación. Se le añadió un fondo abstracto con un diseño adecuado con tipografía 2D. La idea general es: inconfundiblemente vanguardista y artificial, pero también inconfundiblemente lleno de vida y humanidad.
