---
title: The Team
hideMeta: true
author: Christoph Cullmann
date: 2010-07-09T09:03:57+00:00
menu:
  main:
    weight: 90
    parent: menu
---

## Who are the Kate & KWrite contributors?

[Kate & KWrite](https://invent.kde.org/utilities/kate) and the underlying [KTextEditor](https://invent.kde.org/frameworks/ktexteditor) & [KSyntaxHighlighting](https://invent.kde.org/frameworks/syntax-highlighting) frameworks are created by a group of volunteers around the world.
The same is true for [this website](https://invent.kde.org/websites/kate-editor-org).

Below is a periodically updated list of the contributors to the Git repositories containing the above named software components & website.
As this list is purely based on the Git history of our repositories, it will be incomplete for otherwise submitted patches, etc.

The list is sorted by number of commits done by the individual contributors.
This is by no means the best measure of the impact of their contributions, but gives some rough estimation about their level of involvement.

## Don't forget the KDE Community!

Beside these explicitly named contributors to our text editor related components, a large portion of work is done by other members of the much broader [KDE community](https://kde.org/).

This includes crucial work like:

* developing the foundations we use, like KDE Frameworks
* doing all internationalization and translation work for our projects
* releasing our stuff
* maintaining our infrastructure (GitLab, CI, ...)
* supporting developer sprints

If you are not interested in [joining our text editor related team](/join-us/), please take a look if you want to [contribute to KDE](https://community.kde.org/Get_Involved).
The KDE community is a very welcoming bunch of people.

If you are not able to contribute, you might be interested to [donate](https://kde.org/donations).
Whereas these donations are not directly targeted at specific development, they help to keep the overall KDE community going.
For example some Kate related coding sprints were funded by the [KDE e.V.](https://ev.kde.org) based on these donations.

## Contributors during the last year
<b>105 people</b> contributed during the last year.
New contributors are highlighted, thanks for joining our team!

<table>
<tr>
<td>Christoph Cullmann <!-- 833 --></td>
<td><b>Waqar Ahmed</b> <!-- 598 --></td>
</tr>
<tr>
<td>Jonathan Poelen <!-- 115 --></td>
<td>Jan Paul Batrina <!-- 87 --></td>
</tr>
<tr>
<td>Mark Nauwelaerts <!-- 77 --></td>
<td>Kåre Särs <!-- 75 --></td>
</tr>
<tr>
<td>Nibaldo González <!-- 68 --></td>
<td>Ahmad Samir <!-- 45 --></td>
</tr>
<tr>
<td>Dominik Haumann <!-- 39 --></td>
<td><b>Alexander Lohnau</b> <!-- 38 --></td>
</tr>
<tr>
<td>Friedrich W. H. Kossebau <!-- 32 --></td>
<td>Yuri Chornoivan <!-- 28 --></td>
</tr>
<tr>
<td>Laurent Montel <!-- 26 --></td>
<td><b>Phu Nguyen</b> <!-- 25 --></td>
</tr>
<tr>
<td>Carl Schwan <!-- 24 --></td>
<td>Méven Car <!-- 20 --></td>
</tr>
<tr>
<td><b>Krzysztof Stokop</b> <!-- 19 --></td>
<td><b>Marcell Fülöp</b> <!-- 19 --></td>
</tr>
<tr>
<td>Alex Turbov <!-- 18 --></td>
<td><b>Fabian Wunsch</b> <!-- 17 --></td>
</tr>
<tr>
<td>Nicolas Fella <!-- 14 --></td>
<td>David Faure <!-- 10 --></td>
</tr>
<tr>
<td>Albert Astals Cid <!-- 9 --></td>
<td>Andreas Cord-Landwehr <!-- 9 --></td>
</tr>
<tr>
<td>Christoph Feck <!-- 8 --></td>
<td>Tomaz Canabrava <!-- 8 --></td>
</tr>
<tr>
<td><b>David Bryant</b> <!-- 7 --></td>
<td>Milian Wolff <!-- 7 --></td>
</tr>
<tr>
<td><b>Andreas Gratzer</b> <!-- 6 --></td>
<td><b>Andrzej Dabrowski</b> <!-- 6 --></td>
</tr>
<tr>
<td><b>Samuel Gaist</b> <!-- 6 --></td>
<td>Heiko Becker <!-- 5 --></td>
</tr>
<tr>
<td><b>Ilia Kats</b> <!-- 5 --></td>
<td>Michal Humpula <!-- 4 --></td>
</tr>
<tr>
<td>Nate Graham <!-- 4 --></td>
<td>Pino Toscano <!-- 4 --></td>
</tr>
<tr>
<td><b>Xaver Hugl</b> <!-- 4 --></td>
<td><b>Daniele Scasciafratte</b> <!-- 3 --></td>
</tr>
<tr>
<td><b>Denis Lisov</b> <!-- 3 --></td>
<td><b>Ernesto Castellotti</b> <!-- 3 --></td>
</tr>
<tr>
<td><b>Francis Laniel</b> <!-- 3 --></td>
<td>Héctor Mesa Jiménez <!-- 3 --></td>
</tr>
<tr>
<td>Luigi Toscano <!-- 3 --></td>
<td>Thomas Friedrichsmeier <!-- 3 --></td>
</tr>
<tr>
<td><b>Ada Sauce</b> <!-- 2 --></td>
<td><b>Amaury Bouchra Pilet</b> <!-- 2 --></td>
</tr>
<tr>
<td>Antonio Rojas <!-- 2 --></td>
<td><b>Arek Koz (Arusekk)</b> <!-- 2 --></td>
</tr>
<tr>
<td>Christoph Roick <!-- 2 --></td>
<td>Daniel Levin <!-- 2 --></td>
</tr>
<tr>
<td><b>Daniel Tang</b> <!-- 2 --></td>
<td><b>Fabian Vogt</b> <!-- 2 --></td>
</tr>
<tr>
<td><b>George Florea Bănuș</b> <!-- 2 --></td>
<td><b>Mario Aichinger</b> <!-- 2 --></td>
</tr>
<tr>
<td><b>Markus Slopianka</b> <!-- 2 --></td>
<td>Martin Walch <!-- 2 --></td>
</tr>
<tr>
<td><b>Thiago Sueto</b> <!-- 2 --></td>
<td><b>Vincenzo Buttazzo</b> <!-- 2 --></td>
</tr>
<tr>
<td><b>Wes H</b> <!-- 2 --></td>
<td><b>William Wold</b> <!-- 2 --></td>
</tr>
<tr>
<td>Ömer Fadıl Usta <!-- 2 --></td>
<td><b>Alberto Salvia Novella</b> <!-- 1 --></td>
</tr>
<tr>
<td>Alexander Potashev <!-- 1 --></td>
<td><b>Alexander Schlarb</b> <!-- 1 --></td>
</tr>
<tr>
<td>Andreas Hartmetz <!-- 1 --></td>
<td>Andreas Sturmlechner <!-- 1 --></td>
</tr>
<tr>
<td><b>Andrey Karepin</b> <!-- 1 --></td>
<td>Anthony Fieroni <!-- 1 --></td>
</tr>
<tr>
<td><b>Antoni Bella Pérez</b> <!-- 1 --></td>
<td><b>Ayushmaan jangid</b> <!-- 1 --></td>
</tr>
<tr>
<td>Daan De Meyer <!-- 1 --></td>
<td><b>Daniel Sonck</b> <!-- 1 --></td>
</tr>
<tr>
<td>David Redondo <!-- 1 --></td>
<td>David Schulz <!-- 1 --></td>
</tr>
<tr>
<td><b>Felipe Kinoshita</b> <!-- 1 --></td>
<td><b>Felix Yan</b> <!-- 1 --></td>
</tr>
<tr>
<td><b>Frederik Banning</b> <!-- 1 --></td>
<td><b>Gary Wang</b> <!-- 1 --></td>
</tr>
<tr>
<td>Gleb Popov <!-- 1 --></td>
<td>Hannah von Reth <!-- 1 --></td>
</tr>
<tr>
<td><b>Jakub Benda</b> <!-- 1 --></td>
<td><b>Johnny Jazeix</b> <!-- 1 --></td>
</tr>
<tr>
<td>Kai Uwe Broulik <!-- 1 --></td>
<td>Luca Beltrame <!-- 1 --></td>
</tr>
<tr>
<td>Markus Brenneis <!-- 1 --></td>
<td><b>Marvin Ahlgrimm</b> <!-- 1 --></td>
</tr>
<tr>
<td><b>Matheus C. França</b> <!-- 1 --></td>
<td><b>Momo Cao</b> <!-- 1 --></td>
</tr>
<tr>
<td><b>Nazar Kalinowski</b> <!-- 1 --></td>
<td>Nicolás Alvarez <!-- 1 --></td>
</tr>
<tr>
<td>Olivier Goffart <!-- 1 --></td>
<td><b>Pani Ram</b> <!-- 1 --></td>
</tr>
<tr>
<td><b>Pat Brown</b> <!-- 1 --></td>
<td><b>Paul Brown</b> <!-- 1 --></td>
</tr>
<tr>
<td><b>Peter J. Mello</b> <!-- 1 --></td>
<td><b>Peter Mello</b> <!-- 1 --></td>
</tr>
<tr>
<td>Philipp A <!-- 1 --></td>
<td><b>Robert-André Mauchin</b> <!-- 1 --></td>
</tr>
<tr>
<td><b>Roman Hujer</b> <!-- 1 --></td>
<td><b>Samu Voutilainen</b> <!-- 1 --></td>
</tr>
<tr>
<td>Simon Persson <!-- 1 --></td>
<td><b>Wale Afolabi</b> <!-- 1 --></td>
</tr>
<tr>
<td><b>Yunhe Guo</b> <!-- 1 --></td>
<td><b>hololeap hololeap</b> <!-- 1 --></td>
</tr>
<tr>
<td>Ömer Fadıl USTA <!-- 1 --></td>
</table>

## Contributors during the project lifetime
During the full project lifetime <b>593 people</b> contributed.
Thanks for making Kate possible!

<table>
<tr>
<td>Christoph Cullmann <!-- 5834 --></td>
<td>Dominik Haumann <!-- 2582 --></td>
</tr>
<tr>
<td>Waqar Ahmed <!-- 598 --></td>
<td>Joseph Wenninger <!-- 538 --></td>
</tr>
<tr>
<td>Erlend Hamberg <!-- 514 --></td>
<td>Anders Lund <!-- 513 --></td>
</tr>
<tr>
<td>Kåre Särs <!-- 506 --></td>
<td>Hamish Rodda <!-- 482 --></td>
</tr>
<tr>
<td>Simon St James <!-- 456 --></td>
<td>Alex Turbov <!-- 400 --></td>
</tr>
<tr>
<td>Volker Krause <!-- 363 --></td>
<td>Milian Wolff <!-- 361 --></td>
</tr>
<tr>
<td>Laurent Montel <!-- 357 --></td>
<td>Michal Humpula <!-- 336 --></td>
</tr>
<tr>
<td>David Nolden <!-- 269 --></td>
<td>Mark Nauwelaerts <!-- 227 --></td>
</tr>
<tr>
<td>Bernhard Beschow <!-- 219 --></td>
<td>Shaheed Haque <!-- 208 --></td>
</tr>
<tr>
<td>Nibaldo González <!-- 183 --></td>
<td>David Faure <!-- 178 --></td>
</tr>
<tr>
<td>Albert Astals Cid <!-- 167 --></td>
<td>Pascal Létourneau <!-- 165 --></td>
</tr>
<tr>
<td>Miquel Sabaté <!-- 163 --></td>
<td>Jonathan Poelen <!-- 159 --></td>
</tr>
<tr>
<td>Sven Brauch <!-- 155 --></td>
<td>Friedrich W. H. Kossebau <!-- 145 --></td>
</tr>
<tr>
<td>Burkhard Lück <!-- 134 --></td>
<td>Yuri Chornoivan <!-- 126 --></td>
</tr>
<tr>
<td>Sebastian Pipping <!-- 121 --></td>
<td>Pino Toscano <!-- 109 --></td>
</tr>
<tr>
<td>T.C. Hollingsworth <!-- 96 --></td>
<td>Christian Ehrlicher <!-- 93 --></td>
</tr>
<tr>
<td>Dirk Mueller <!-- 93 --></td>
<td>Robin Pedersen <!-- 93 --></td>
</tr>
<tr>
<td>Wilbert Berendsen <!-- 93 --></td>
<td>Jan Paul Batrina <!-- 90 --></td>
</tr>
<tr>
<td>Pablo Martín <!-- 88 --></td>
<td>Michel Ludwig <!-- 87 --></td>
</tr>
<tr>
<td>Matthew Woehlke <!-- 84 --></td>
<td>loh tar <!-- 81 --></td>
</tr>
<tr>
<td>John Firebaugh <!-- 80 --></td>
<td>Alex Neundorf <!-- 74 --></td>
</tr>
<tr>
<td>Kevin Funk <!-- 72 --></td>
<td>Svyatoslav Kuzmich <!-- 66 --></td>
</tr>
<tr>
<td>Adrian Lungu <!-- 62 --></td>
<td>Ahmad Samir <!-- 60 --></td>
</tr>
<tr>
<td>Christoph Feck <!-- 57 --></td>
<td>Christian Couder <!-- 56 --></td>
</tr>
<tr>
<td>Stephan Binner <!-- 56 --></td>
<td>Stephan Kulow <!-- 55 --></td>
</tr>
<tr>
<td>Carl Schwan <!-- 54 --></td>
<td>Mirko Stocker <!-- 54 --></td>
</tr>
<tr>
<td>Jesse Yurkovich <!-- 52 --></td>
<td>Alex Merry <!-- 51 --></td>
</tr>
<tr>
<td>Thomas Friedrichsmeier <!-- 51 --></td>
<td>Vegard Øye <!-- 49 --></td>
</tr>
<tr>
<td>Malcolm Hunter <!-- 46 --></td>
<td>Alexander Neundorf <!-- 44 --></td>
</tr>
<tr>
<td>Gregor Mi <!-- 42 --></td>
<td>Montel Laurent <!-- 42 --></td>
</tr>
<tr>
<td>Alexander Lohnau <!-- 38 --></td>
<td>Rafael Fernández López <!-- 38 --></td>
</tr>
<tr>
<td>Marco Mentasti <!-- 35 --></td>
<td>Simon Huerlimann <!-- 35 --></td>
</tr>
<tr>
<td>Urs Wolfer <!-- 33 --></td>
<td>Allen Winter <!-- 32 --></td>
</tr>
<tr>
<td>Aleix Pol <!-- 31 --></td>
<td>Phil Schaf <!-- 30 --></td>
</tr>
<tr>
<td>Jakob Petsovits <!-- 29 --></td>
<td>Andrew Coles <!-- 28 --></td>
</tr>
<tr>
<td>Gerald Senarclens de Grancy <!-- 28 --></td>
<td>Adriaan de Groot <!-- 27 --></td>
</tr>
<tr>
<td>Chusslove Illich <!-- 27 --></td>
<td>Thomas Fjellstrom <!-- 27 --></td>
</tr>
<tr>
<td>Andreas Hartmetz <!-- 26 --></td>
<td>Martijn Klingens <!-- 26 --></td>
</tr>
<tr>
<td>Méven Car <!-- 26 --></td>
<td>Phu Nguyen <!-- 25 --></td>
</tr>
<tr>
<td>André Wöbbeking <!-- 24 --></td>
<td>Alex Richardson <!-- 22 --></td>
</tr>
<tr>
<td>Jarosław Staniek <!-- 22 --></td>
<td>Patrick Spendrin <!-- 22 --></td>
</tr>
<tr>
<td>Frederik Schwarzer <!-- 21 --></td>
<td>Nicolas Goutte <!-- 21 --></td>
</tr>
<tr>
<td>Bram Schoenmakers <!-- 20 --></td>
<td>Simon Hausmann <!-- 20 --></td>
</tr>
<tr>
<td>Tomáš Trnka <!-- 20 --></td>
<td>Adrián Chaves Fernández (Gallaecio) <!-- 19 --></td>
</tr>
<tr>
<td>Aurélien Gâteau <!-- 19 --></td>
<td>Krzysztof Stokop <!-- 19 --></td>
</tr>
<tr>
<td>Marcell Fülöp <!-- 19 --></td>
<td>Aaron J. Seigo <!-- 18 --></td>
</tr>
<tr>
<td>Andreas Kling <!-- 18 --></td>
<td>Arto Hytönen <!-- 17 --></td>
</tr>
<tr>
<td>Clarence Dang <!-- 17 --></td>
<td>Fabian Wunsch <!-- 17 --></td>
</tr>
<tr>
<td>Niko Sams <!-- 17 --></td>
<td>Paul Giannaros <!-- 17 --></td>
</tr>
<tr>
<td>Andrew Paseltiner <!-- 16 --></td>
<td>Brian Anderson <!-- 16 --></td>
</tr>
<tr>
<td>Ian Reinhart Geiser <!-- 16 --></td>
<td>Nicolas Fella <!-- 16 --></td>
</tr>
<tr>
<td>Frederik Gladhorn <!-- 15 --></td>
<td>Heiko Becker <!-- 15 --></td>
</tr>
<tr>
<td>Kevin Ottens <!-- 15 --></td>
<td>Matt Rogers <!-- 15 --></td>
</tr>
<tr>
<td>Kai Uwe Broulik <!-- 14 --></td>
<td>Luigi Toscano <!-- 14 --></td>
</tr>
<tr>
<td>Thiago Macieira <!-- 14 --></td>
<td>Daniel Naber <!-- 13 --></td>
</tr>
<tr>
<td>Eike Hein <!-- 13 --></td>
<td>Jan Villat <!-- 13 --></td>
</tr>
<tr>
<td>Luca Beltrame <!-- 13 --></td>
<td>Waldo Bastian <!-- 13 --></td>
</tr>
<tr>
<td>Andreas Cord-Landwehr <!-- 12 --></td>
<td>Christoph Roick <!-- 12 --></td>
</tr>
<tr>
<td>Daan De Meyer <!-- 12 --></td>
<td>Johannes Sixt <!-- 12 --></td>
</tr>
<tr>
<td>Jonathan Riddell <!-- 12 --></td>
<td>José Pablo Ezequiel Fernández <!-- 12 --></td>
</tr>
<tr>
<td>Leo Savernik <!-- 12 --></td>
<td>Andreas Pakulat <!-- 11 --></td>
</tr>
<tr>
<td>Andrey Matveyakin <!-- 11 --></td>
<td>Anthony Fieroni <!-- 11 --></td>
</tr>
<tr>
<td>Chusslove Illich (Часлав Илић) <!-- 11 --></td>
<td>Diego Iastrubni <!-- 11 --></td>
</tr>
<tr>
<td>Philipp A <!-- 11 --></td>
<td>Tobias Koenig <!-- 11 --></td>
</tr>
<tr>
<td>Alexander Potashev <!-- 10 --></td>
<td>Eduardo Robles Elvira <!-- 10 --></td>
</tr>
<tr>
<td>Harald Fernengel <!-- 10 --></td>
<td>Jiri Pinkava <!-- 10 --></td>
</tr>
<tr>
<td>Jonathan Schmidt-Dominé <!-- 10 --></td>
<td>Matthias Kretz <!-- 10 --></td>
</tr>
<tr>
<td>Tomaz Canabrava <!-- 10 --></td>
<td>Trevor Blight <!-- 10 --></td>
</tr>
<tr>
<td>Wang Kai <!-- 10 --></td>
<td>Christophe Giboudeaux <!-- 9 --></td>
</tr>
<tr>
<td>Ellis Whitehead <!-- 9 --></td>
<td>Harsh Kumar <!-- 9 --></td>
</tr>
<tr>
<td>Jaime Torres <!-- 9 --></td>
<td>Lasse Liehu <!-- 9 --></td>
</tr>
<tr>
<td>Maks Orlovich <!-- 9 --></td>
<td>Martin Walch <!-- 9 --></td>
</tr>
<tr>
<td>Nate Graham <!-- 9 --></td>
<td>Oswald Buddenhagen <!-- 9 --></td>
</tr>
<tr>
<td>Andras Mantia <!-- 8 --></td>
<td>Christian Loose <!-- 8 --></td>
</tr>
<tr>
<td>David Edmundson <!-- 8 --></td>
<td>David Palser <!-- 8 --></td>
</tr>
<tr>
<td>Dmitry Risenberg <!-- 8 --></td>
<td>Frank Osterfeld <!-- 8 --></td>
</tr>
<tr>
<td>John Layt <!-- 8 --></td>
<td>John Tapsell <!-- 8 --></td>
</tr>
<tr>
<td>Oliver Kellogg <!-- 8 --></td>
<td>Primoz Anzur <!-- 8 --></td>
</tr>
<tr>
<td>Ralf Habacker <!-- 8 --></td>
<td>René J.V. Bertin <!-- 8 --></td>
</tr>
<tr>
<td>flying sheep <!-- 8 --></td>
<td>Abhishek Patil <!-- 7 --></td>
</tr>
<tr>
<td>Anne-Marie Mahfouf <!-- 7 --></td>
<td>Bastian Holst <!-- 7 --></td>
</tr>
<tr>
<td>David Bryant <!-- 7 --></td>
<td>David Jarvie <!-- 7 --></td>
</tr>
<tr>
<td>David Schulz <!-- 7 --></td>
<td>Filip Gawin <!-- 7 --></td>
</tr>
<tr>
<td>George Staikos <!-- 7 --></td>
<td>Hannah von Reth <!-- 7 --></td>
</tr>
<tr>
<td>Jeroen Wijnhout <!-- 7 --></td>
<td>John Salatas <!-- 7 --></td>
</tr>
<tr>
<td>Kazuki Ohta <!-- 7 --></td>
<td>Matt Broadstone <!-- 7 --></td>
</tr>
<tr>
<td>Orgad Shaneh <!-- 7 --></td>
<td>Peter Oberndorfer <!-- 7 --></td>
</tr>
<tr>
<td>Ryan Cumming <!-- 7 --></td>
<td>Yury G. Kudryashov <!-- 7 --></td>
</tr>
<tr>
<td>Adam Treat <!-- 6 --></td>
<td>Andreas Gratzer <!-- 6 --></td>
</tr>
<tr>
<td>Andrzej Dabrowski <!-- 6 --></td>
<td>Charles Samuels <!-- 6 --></td>
</tr>
<tr>
<td>Daniel Levin <!-- 6 --></td>
<td>David Redondo <!-- 6 --></td>
</tr>
<tr>
<td>Dawit Alemayehu <!-- 6 --></td>
<td>Gleb Popov <!-- 6 --></td>
</tr>
<tr>
<td>Ian Wakeling <!-- 6 --></td>
<td>Jaison Lee <!-- 6 --></td>
</tr>
<tr>
<td>Jos van den Oever <!-- 6 --></td>
<td>Lukáš Tinkl <!-- 6 --></td>
</tr>
<tr>
<td>Martin T. H. Sandsmark <!-- 6 --></td>
<td>Massimo Callegari <!-- 6 --></td>
</tr>
<tr>
<td>Michael Jansen <!-- 6 --></td>
<td>Nathaniel Graham <!-- 6 --></td>
</tr>
<tr>
<td>Nick Shaforostoff <!-- 6 --></td>
<td>Nicolás Alvarez <!-- 6 --></td>
</tr>
<tr>
<td>Olivier Goffart <!-- 6 --></td>
<td>R.J.V. Bertin <!-- 6 --></td>
</tr>
<tr>
<td>Rafał Rzepecki <!-- 6 --></td>
<td>Safa AlFulaij <!-- 6 --></td>
</tr>
<tr>
<td>Samuel Gaist <!-- 6 --></td>
<td>Shubham Jangra <!-- 6 --></td>
</tr>
<tr>
<td>Weng Xuetian <!-- 6 --></td>
<td>York Xiang <!-- 6 --></td>
</tr>
<tr>
<td>Allan Sandfeld Jensen <!-- 5 --></td>
<td>Andreas Holzammer <!-- 5 --></td>
</tr>
<tr>
<td>Arno Rehn <!-- 5 --></td>
<td>Bernd Gehrmann <!-- 5 --></td>
</tr>
<tr>
<td>Carlo Segato <!-- 5 --></td>
<td>Carsten Pfeiffer <!-- 5 --></td>
</tr>
<tr>
<td>Charles Vejnar <!-- 5 --></td>
<td>David Herberth <!-- 5 --></td>
</tr>
<tr>
<td>Harri Porten <!-- 5 --></td>
<td>Harsh Chouraria J <!-- 5 --></td>
</tr>
<tr>
<td>Helio Castro <!-- 5 --></td>
<td>Heng Liu <!-- 5 --></td>
</tr>
<tr>
<td>Holger Danielsson <!-- 5 --></td>
<td>Ilia Kats <!-- 5 --></td>
</tr>
<tr>
<td>Ivan Čukić <!-- 5 --></td>
<td>Ivo Anjo <!-- 5 --></td>
</tr>
<tr>
<td>Michael Hansen <!-- 5 --></td>
<td>Peter Kümmel <!-- 5 --></td>
</tr>
<tr>
<td>Richard Smith <!-- 5 --></td>
<td>Scott Wheeler <!-- 5 --></td>
</tr>
<tr>
<td>Stephen Kelly <!-- 5 --></td>
<td>Thomas Braun <!-- 5 --></td>
</tr>
<tr>
<td>andreas kainz <!-- 5 --></td>
<td>Andi Fischer <!-- 4 --></td>
</tr>
<tr>
<td>Andrius Štikonas <!-- 4 --></td>
<td>Antonio Rojas <!-- 4 --></td>
</tr>
<tr>
<td>Ben Cooksley <!-- 4 --></td>
<td>Benjamin Meyer <!-- 4 --></td>
</tr>
<tr>
<td>Darío Andrés Rodríguez <!-- 4 --></td>
<td>Dominique Devriese <!-- 4 --></td>
</tr>
<tr>
<td>Emeric Dupont <!-- 4 --></td>
<td>Evgeniy Ivanov <!-- 4 --></td>
</tr>
<tr>
<td>Flavio Castelli <!-- 4 --></td>
<td>Grzegorz Szymaszek <!-- 4 --></td>
</tr>
<tr>
<td>Guo Yunhe <!-- 4 --></td>
<td>Helio Chissini de Castro <!-- 4 --></td>
</tr>
<tr>
<td>Héctor Mesa Jiménez <!-- 4 --></td>
<td>Luboš Luňák <!-- 4 --></td>
</tr>
<tr>
<td>Michael Pyne <!-- 4 --></td>
<td>Nadeem Hasan <!-- 4 --></td>
</tr>
<tr>
<td>Nikita Sirgienko <!-- 4 --></td>
<td>Rolf Eike Beer <!-- 4 --></td>
</tr>
<tr>
<td>Stefan Asserhäll <!-- 4 --></td>
<td>Thomas Schoeps <!-- 4 --></td>
</tr>
<tr>
<td>Toshitaka Fujioka <!-- 4 --></td>
<td>Xaver Hugl <!-- 4 --></td>
</tr>
<tr>
<td>Aaron Puchert <!-- 3 --></td>
<td>Alex Crichton <!-- 3 --></td>
</tr>
<tr>
<td>Bernhard Loos <!-- 3 --></td>
<td>Boris Egorov <!-- 3 --></td>
</tr>
<tr>
<td>Daniele Scasciafratte <!-- 3 --></td>
<td>Dave Corrie <!-- 3 --></td>
</tr>
<tr>
<td>David Leimbach <!-- 3 --></td>
<td>Davide Bettio <!-- 3 --></td>
</tr>
<tr>
<td>Denis Lisov <!-- 3 --></td>
<td>Dāvis Mosāns <!-- 3 --></td>
</tr>
<tr>
<td>Ernesto Castellotti <!-- 3 --></td>
<td>Francis Laniel <!-- 3 --></td>
</tr>
<tr>
<td>Harald Sitter <!-- 3 --></td>
<td>Helge Deller <!-- 3 --></td>
</tr>
<tr>
<td>Holger Schröder <!-- 3 --></td>
<td>Ignacio Castaño Aguado <!-- 3 --></td>
</tr>
<tr>
<td>Ilya Konstantinov <!-- 3 --></td>
<td>Jacob Rideout <!-- 3 --></td>
</tr>
<tr>
<td>Laurence Withers <!-- 3 --></td>
<td>Marcus Camen <!-- 3 --></td>
</tr>
<tr>
<td>Markus Meik Slopianka <!-- 3 --></td>
<td>Martin Kostolný <!-- 3 --></td>
</tr>
<tr>
<td>Michael Goffioul <!-- 3 --></td>
<td>Mickael Marchand <!-- 3 --></td>
</tr>
<tr>
<td>Mike Harris <!-- 3 --></td>
<td>Raymond Wooninck <!-- 3 --></td>
</tr>
<tr>
<td>Rex Dieter <!-- 3 --></td>
<td>Reza Arbab <!-- 3 --></td>
</tr>
<tr>
<td>Richard Dale <!-- 3 --></td>
<td>Richard J. Moore <!-- 3 --></td>
</tr>
<tr>
<td>Robert Knight <!-- 3 --></td>
<td>Sergio Martins <!-- 3 --></td>
</tr>
<tr>
<td>Simon Persson <!-- 3 --></td>
<td>Vladimir Prus <!-- 3 --></td>
</tr>
<tr>
<td>Willy De la Court <!-- 3 --></td>
<td>Wolfgang Bauer <!-- 3 --></td>
</tr>
<tr>
<td>Ömer Fadıl USTA <!-- 3 --></td>
<td>Ömer Fadıl Usta <!-- 3 --></td>
</tr>
<tr>
<td>Ada Sauce <!-- 2 --></td>
<td>Alexander Dymo <!-- 2 --></td>
</tr>
<tr>
<td>Alexander Zhigalin <!-- 2 --></td>
<td>Amaury Bouchra Pilet <!-- 2 --></td>
</tr>
<tr>
<td>Andreas Sturmlechner <!-- 2 --></td>
<td>Andrew Crouthamel <!-- 2 --></td>
</tr>
<tr>
<td>Antonio Larrosa Jimenez <!-- 2 --></td>
<td>Arek Koz (Arusekk) <!-- 2 --></td>
</tr>
<tr>
<td>Arend van Beelen jr <!-- 2 --></td>
<td>Bernhard Rosenkraenzer <!-- 2 --></td>
</tr>
<tr>
<td>Bhushan Shah <!-- 2 --></td>
<td>Caleb Tennis <!-- 2 --></td>
</tr>
<tr>
<td>Casper Boemann <!-- 2 --></td>
<td>Chris Howells <!-- 2 --></td>
</tr>
<tr>
<td>Cornelius Schumacher <!-- 2 --></td>
<td>Craig Drummond <!-- 2 --></td>
</tr>
<tr>
<td>Cristian Oneț <!-- 2 --></td>
<td>Cédric Borgese <!-- 2 --></td>
</tr>
<tr>
<td>Daniel Tang <!-- 2 --></td>
<td>Dmitry Suzdalev <!-- 2 --></td>
</tr>
<tr>
<td>Elvis Angelaccio <!-- 2 --></td>
<td>Enrique Matías Sánchez <!-- 2 --></td>
</tr>
<tr>
<td>Fabian Kosmale <!-- 2 --></td>
<td>Fabian Vogt <!-- 2 --></td>
</tr>
<tr>
<td>Frans Englich <!-- 2 --></td>
<td>Frerich Raabe <!-- 2 --></td>
</tr>
<tr>
<td>Gene Thomas <!-- 2 --></td>
<td>George Florea Bănuș <!-- 2 --></td>
</tr>
<tr>
<td>Gioele Barabucci <!-- 2 --></td>
<td>Gregory S. Hayes <!-- 2 --></td>
</tr>
<tr>
<td>Hoàng Đức Hiếu <!-- 2 --></td>
<td>Hugo Pereira Da Costa <!-- 2 --></td>
</tr>
<tr>
<td>Huon Wilson <!-- 2 --></td>
<td>Ivan Shapovalov <!-- 2 --></td>
</tr>
<tr>
<td>Jaime Torres Amate <!-- 2 --></td>
<td>Jekyll Wu <!-- 2 --></td>
</tr>
<tr>
<td>Jens Dagerbo <!-- 2 --></td>
<td>Julien Antille <!-- 2 --></td>
</tr>
<tr>
<td>Jure Repinc <!-- 2 --></td>
<td>Kurt Pfeifle <!-- 2 --></td>
</tr>
<tr>
<td>Leonardo Finetti <!-- 2 --></td>
<td>Marcello Massaro <!-- 2 --></td>
</tr>
<tr>
<td>Mario Aichinger <!-- 2 --></td>
<td>Markus Brenneis <!-- 2 --></td>
</tr>
<tr>
<td>Markus Pister <!-- 2 --></td>
<td>Markus Slopianka <!-- 2 --></td>
</tr>
<tr>
<td>Maximilian Löffler <!-- 2 --></td>
<td>Michael Palimaka <!-- 2 --></td>
</tr>
<tr>
<td>Michel Hermier <!-- 2 --></td>
<td>Mickael Bosch <!-- 2 --></td>
</tr>
<tr>
<td>Miklos Marton <!-- 2 --></td>
<td>Nikolas Zimmermann <!-- 2 --></td>
</tr>
<tr>
<td>Petter Stokke <!-- 2 --></td>
<td>Phil Young <!-- 2 --></td>
</tr>
<tr>
<td>Pierre-Marie Pédrot <!-- 2 --></td>
<td>Raphael Kubo da Costa <!-- 2 --></td>
</tr>
<tr>
<td>Rob Buis <!-- 2 --></td>
<td>Robert Hoffmann <!-- 2 --></td>
</tr>
<tr>
<td>Roberto Raggi <!-- 2 --></td>
<td>Scott Lawrence <!-- 2 --></td>
</tr>
<tr>
<td>Sebastian Kügler <!-- 2 --></td>
<td>Sebastian Sauer <!-- 2 --></td>
</tr>
<tr>
<td>Sergey Kalinichev <!-- 2 --></td>
<td>Shaun Reich <!-- 2 --></td>
</tr>
<tr>
<td>Silas Lenz <!-- 2 --></td>
<td>Sven Leiber <!-- 2 --></td>
</tr>
<tr>
<td>Thiago Sueto <!-- 2 --></td>
<td>Thomas Braxton <!-- 2 --></td>
</tr>
<tr>
<td>Thomas Häber <!-- 2 --></td>
<td>Thomas Leitner <!-- 2 --></td>
</tr>
<tr>
<td>Thomas Surrel <!-- 2 --></td>
<td>Thorsten Roeder <!-- 2 --></td>
</tr>
<tr>
<td>Tim Beaulen <!-- 2 --></td>
<td>Tim Hutt <!-- 2 --></td>
</tr>
<tr>
<td>Tobias C. Berner <!-- 2 --></td>
<td>Valentin Rouet <!-- 2 --></td>
</tr>
<tr>
<td>Vincent Belliard <!-- 2 --></td>
<td>Vincenzo Buttazzo <!-- 2 --></td>
</tr>
<tr>
<td>Volker Augustin <!-- 2 --></td>
<td>Wes H <!-- 2 --></td>
</tr>
<tr>
<td>Will Entriken <!-- 2 --></td>
<td>William Wold <!-- 2 --></td>
</tr>
<tr>
<td>Zack Rusin <!-- 2 --></td>
<td>Алексей Шилин <!-- 2 --></td>
</tr>
<tr>
<td>Aaron Seigo <!-- 1 --></td>
<td>Alberto Salvia Novella <!-- 1 --></td>
</tr>
<tr>
<td>Aleix Pol Gonzalez <!-- 1 --></td>
<td>Alex Hermann <!-- 1 --></td>
</tr>
<tr>
<td>Alexander Schlarb <!-- 1 --></td>
<td>Alexander Volkov <!-- 1 --></td>
</tr>
<tr>
<td>Alexey Bogdanenko <!-- 1 --></td>
<td>Amit Kumar Jaiswal <!-- 1 --></td>
</tr>
<tr>
<td>Ana Beatriz Guerrero López <!-- 1 --></td>
<td>Anakim Border <!-- 1 --></td>
</tr>
<tr>
<td>Anders Ponga <!-- 1 --></td>
<td>Andre Heinecke <!-- 1 --></td>
</tr>
<tr>
<td>Andrea Canciani <!-- 1 --></td>
<td>Andrea Scarpino <!-- 1 --></td>
</tr>
<tr>
<td>Andreas Hohenegger <!-- 1 --></td>
<td>Andreas Simon <!-- 1 --></td>
</tr>
<tr>
<td>Andrew Chen <!-- 1 --></td>
<td>Andrey Karepin <!-- 1 --></td>
</tr>
<tr>
<td>Andrey S. Cherepanov <!-- 1 --></td>
<td>Andrius da Costa Ribas <!-- 1 --></td>
</tr>
<tr>
<td>André Marcelo Alvarenga <!-- 1 --></td>
<td>Andy Goossens <!-- 1 --></td>
</tr>
<tr>
<td>Antoni Bella Pérez <!-- 1 --></td>
<td>Arctic Ice Studio <!-- 1 --></td>
</tr>
<tr>
<td>Arnaud Ruiz <!-- 1 --></td>
<td>Arnold Dumas <!-- 1 --></td>
</tr>
<tr>
<td>Arnold Krille <!-- 1 --></td>
<td>Ashish Bansal <!-- 1 --></td>
</tr>
<tr>
<td>Axel Kittenberger <!-- 1 --></td>
<td>Ayushmaan jangid <!-- 1 --></td>
</tr>
<tr>
<td>Azat Khuzhin <!-- 1 --></td>
<td>Bart Ribbers <!-- 1 --></td>
</tr>
<tr>
<td>Barış Metin <!-- 1 --></td>
<td>Ben Blum <!-- 1 --></td>
</tr>
<tr>
<td>Benjamin Buch <!-- 1 --></td>
<td>Bernd Buschinski <!-- 1 --></td>
</tr>
<tr>
<td>Björn Peemöller <!-- 1 --></td>
<td>Brad Hards <!-- 1 --></td>
</tr>
<tr>
<td>Brendan Zabarauskas <!-- 1 --></td>
<td>Bruno Virlet <!-- 1 --></td>
</tr>
<tr>
<td>Casper van Donderen <!-- 1 --></td>
<td>Christoph Rüßler <!-- 1 --></td>
</tr>
<tr>
<td>Christopher Blauvelt <!-- 1 --></td>
<td>Claudio Bantaloukas <!-- 1 --></td>
</tr>
<tr>
<td>Conrad Hoffmann <!-- 1 --></td>
<td>Corey Richardson <!-- 1 --></td>
</tr>
<tr>
<td>Cédric Pasteur <!-- 1 --></td>
<td>Dan Vrátil <!-- 1 --></td>
</tr>
<tr>
<td>Daniel Laidig <!-- 1 --></td>
<td>Daniel Micay <!-- 1 --></td>
</tr>
<tr>
<td>Daniel Sonck <!-- 1 --></td>
<td>David Rosca <!-- 1 --></td>
</tr>
<tr>
<td>David Smith <!-- 1 --></td>
<td>Denis Steckelmacher <!-- 1 --></td>
</tr>
<tr>
<td>Diana-Victoria Tiriplica <!-- 1 --></td>
<td>Diggory Hardy <!-- 1 --></td>
</tr>
<tr>
<td>Dirk Rathlev <!-- 1 --></td>
<td>Duncan Mac-Vicar Prett <!-- 1 --></td>
</tr>
<tr>
<td>Ede Rag <!-- 1 --></td>
<td>Ederag <!-- 1 --></td>
</tr>
<tr>
<td>Elias Probst <!-- 1 --></td>
<td>Emanuele Tamponi <!-- 1 --></td>
</tr>
<tr>
<td>Emmanuel Lepage Vallee <!-- 1 --></td>
<td>Ewald Snel <!-- 1 --></td>
</tr>
<tr>
<td>Federico Zenith <!-- 1 --></td>
<td>Felipe Kinoshita <!-- 1 --></td>
</tr>
<tr>
<td>Felix Yan <!-- 1 --></td>
<td>Filipe Saraiva <!-- 1 --></td>
</tr>
<tr>
<td>Francis Herne <!-- 1 --></td>
<td>Francois-Xavier Duranceau <!-- 1 --></td>
</tr>
<tr>
<td>Frederik Banning <!-- 1 --></td>
<td>Fredrik Höglund <!-- 1 --></td>
</tr>
<tr>
<td>Gary Wang <!-- 1 --></td>
<td>Germain Garand <!-- 1 --></td>
</tr>
<tr>
<td>Gregor Tätzner <!-- 1 --></td>
<td>Guillermo Antonio Amaral Bastidas <!-- 1 --></td>
</tr>
<tr>
<td>Guillermo Molteni <!-- 1 --></td>
<td>Hartmut Goebel <!-- 1 --></td>
</tr>
<tr>
<td>Heinz Wiesinger <!-- 1 --></td>
<td>HeroesGrave <!-- 1 --></td>
</tr>
<tr>
<td>Ian Monroe <!-- 1 --></td>
<td>Ignat Semenov <!-- 1 --></td>
</tr>
<tr>
<td>Ivan Koveshnikov <!-- 1 --></td>
<td>Jakub Benda <!-- 1 --></td>
</tr>
<tr>
<td>Jan Grulich <!-- 1 --></td>
<td>Jan Przybylak <!-- 1 --></td>
</tr>
<tr>
<td>Jeremy Whiting <!-- 1 --></td>
<td>Jesse Crossen <!-- 1 --></td>
</tr>
<tr>
<td>Jochen Wilhelmy <!-- 1 --></td>
<td>Joerg Schiermeier <!-- 1 --></td>
</tr>
<tr>
<td>John Schroeder <!-- 1 --></td>
<td>Johnny Jazeix <!-- 1 --></td>
</tr>
<tr>
<td>Jonathan L. Verner <!-- 1 --></td>
<td>Jonathan Raphael Joachim Kolberg <!-- 1 --></td>
</tr>
<tr>
<td>Jonathan Singer <!-- 1 --></td>
<td>Jonathan Verner <!-- 1 --></td>
</tr>
<tr>
<td>José Joaquín Atria <!-- 1 --></td>
<td>Juan Francisco Cantero Hurtado <!-- 1 --></td>
</tr>
<tr>
<td>Juliano F. Ravasi <!-- 1 --></td>
<td>Juraj Oravec <!-- 1 --></td>
</tr>
<tr>
<td>Karol Szwed <!-- 1 --></td>
<td>Kevin Ballard <!-- 1 --></td>
</tr>
<tr>
<td>Kishore Gopalakrishnan <!-- 1 --></td>
<td>Kurt Granroth <!-- 1 --></td>
</tr>
<tr>
<td>Kurt Hindenburg <!-- 1 --></td>
<td>Kyle S Horne <!-- 1 --></td>
</tr>
<tr>
<td>Laurent Cimon <!-- 1 --></td>
<td>Lauri Watts <!-- 1 --></td>
</tr>
<tr>
<td>Lays Rodrigues <!-- 1 --></td>
<td>Leandro Emanuel López <!-- 1 --></td>
</tr>
<tr>
<td>Leandro Santiago <!-- 1 --></td>
<td>Li-yao Xia <!-- 1 --></td>
</tr>
<tr>
<td>Liu Zhe <!-- 1 --></td>
<td>Maciej Mrozowski <!-- 1 --></td>
</tr>
<tr>
<td>Magnus Hoff <!-- 1 --></td>
<td>Malte Starostik <!-- 1 --></td>
</tr>
<tr>
<td>Manuel Tortosa <!-- 1 --></td>
<td>Marc Espie <!-- 1 --></td>
</tr>
<tr>
<td>Marco Martin <!-- 1 --></td>
<td>Marijn Kruisselbrink <!-- 1 --></td>
</tr>
<tr>
<td>Martin Gräßlin <!-- 1 --></td>
<td>Martin Klapetek <!-- 1 --></td>
</tr>
<tr>
<td>Martin Sandsmark <!-- 1 --></td>
<td>Martin Tobias Holmedahl Sandsmark <!-- 1 --></td>
</tr>
<tr>
<td>Marvin Ahlgrimm <!-- 1 --></td>
<td>Matheus C. França <!-- 1 --></td>
</tr>
<tr>
<td>Matt Carberry <!-- 1 --></td>
<td>Matthias Gerstner <!-- 1 --></td>
</tr>
<tr>
<td>Matthias Hoelzer-Kluepfel <!-- 1 --></td>
<td>Matthias Klumpp <!-- 1 --></td>
</tr>
<tr>
<td>Melchior Franz <!-- 1 --></td>
<td>Michael Drueing <!-- 1 --></td>
</tr>
<tr>
<td>Michael Heidelbach <!-- 1 --></td>
<td>Michael Matz <!-- 1 --></td>
</tr>
<tr>
<td>Michael Ritzert <!-- 1 --></td>
<td>Michal Srb <!-- 1 --></td>
</tr>
<tr>
<td>Mikhail Zolotukhin <!-- 1 --></td>
<td>Mikko Perttunen <!-- 1 --></td>
</tr>
<tr>
<td>Miquel Sabaté Solà <!-- 1 --></td>
<td>Momo Cao <!-- 1 --></td>
</tr>
<tr>
<td>Médéric Boquien <!-- 1 --></td>
<td>Nazar Kalinowski <!-- 1 --></td>
</tr>
<tr>
<td>Nico Kruber <!-- 1 --></td>
<td>Nicola Gigante <!-- 1 --></td>
</tr>
<tr>
<td>Nicolas Lécureuil <!-- 1 --></td>
<td>Nikolay Kultashev <!-- 1 --></td>
</tr>
<tr>
<td>Oleksandr Senkovych <!-- 1 --></td>
<td>Oliver Sander <!-- 1 --></td>
</tr>
<tr>
<td>Olivier CHURLAUD <!-- 1 --></td>
<td>Olivier Felt <!-- 1 --></td>
</tr>
<tr>
<td>Ovidiu-Florin BOGDAN <!-- 1 --></td>
<td>Pani Ram <!-- 1 --></td>
</tr>
<tr>
<td>Paolo Borelli <!-- 1 --></td>
<td>Parker Coates <!-- 1 --></td>
</tr>
<tr>
<td>Pat Brown <!-- 1 --></td>
<td>Patrick José Pereira <!-- 1 --></td>
</tr>
<tr>
<td>Paul Brown <!-- 1 --></td>
<td>Paul Gideon Dann <!-- 1 --></td>
</tr>
<tr>
<td>Paulo Barreto <!-- 1 --></td>
<td>Paulo Moura Guedes <!-- 1 --></td>
</tr>
<tr>
<td>Pavel Pertsev <!-- 1 --></td>
<td>Pedro Gimeno <!-- 1 --></td>
</tr>
<tr>
<td>Per Winkvist <!-- 1 --></td>
<td>Peter J. Mello <!-- 1 --></td>
</tr>
<tr>
<td>Peter Mello <!-- 1 --></td>
<td>Peter Penz <!-- 1 --></td>
</tr>
<tr>
<td>Philippe Fremy <!-- 1 --></td>
<td>Rafał Miłecki <!-- 1 --></td>
</tr>
<tr>
<td>Ralf Jung <!-- 1 --></td>
<td>Ralf Nolden <!-- 1 --></td>
</tr>
<tr>
<td>Ramon Zarazua <!-- 1 --></td>
<td>Randy Kron <!-- 1 --></td>
</tr>
<tr>
<td>Raphael Rosch <!-- 1 --></td>
<td>Richard Mader <!-- 1 --></td>
</tr>
<tr>
<td>Robert Gruber <!-- 1 --></td>
<td>Robert-André Mauchin <!-- 1 --></td>
</tr>
<tr>
<td>Rohan Garg <!-- 1 --></td>
<td>Rolf Magnus <!-- 1 --></td>
</tr>
<tr>
<td>Roman Gilg <!-- 1 --></td>
<td>Roman Hujer <!-- 1 --></td>
</tr>
<tr>
<td>Ruediger Gad <!-- 1 --></td>
<td>Samu Voutilainen <!-- 1 --></td>
</tr>
<tr>
<td>Sandro Giessl <!-- 1 --></td>
<td>Sascha Cunz <!-- 1 --></td>
</tr>
<tr>
<td>Sean Gillespie <!-- 1 --></td>
<td>Shane Wright <!-- 1 --></td>
</tr>
<tr>
<td>Simone Scalabrino <!-- 1 --></td>
<td>Stefan Gehn <!-- 1 --></td>
</tr>
<tr>
<td>Steve Mokris <!-- 1 --></td>
<td>Sven Greb <!-- 1 --></td>
</tr>
<tr>
<td>Sven Lüppken <!-- 1 --></td>
<td>Thomas Capricelli <!-- 1 --></td>
</tr>
<tr>
<td>Thomas Horstmeyer <!-- 1 --></td>
<td>Thomas Jarosch <!-- 1 --></td>
</tr>
<tr>
<td>Thomas McGuire <!-- 1 --></td>
<td>Thomas Reitelbach <!-- 1 --></td>
</tr>
<tr>
<td>Till Schfer <!-- 1 --></td>
<td>Tim Jansen <!-- 1 --></td>
</tr>
<tr>
<td>Tom Albers <!-- 1 --></td>
<td>Tore Melangen Havn <!-- 1 --></td>
</tr>
<tr>
<td>Vladimír Vondruš <!-- 1 --></td>
<td>Wale Afolabi <!-- 1 --></td>
</tr>
<tr>
<td>Waqar Ahmed        <!-- 1 --></td>
<td>Werner Trobin <!-- 1 --></td>
</tr>
<tr>
<td>Wilco Greven <!-- 1 --></td>
<td>Will Stephenson <!-- 1 --></td>
</tr>
<tr>
<td>Wojciech Stachurski <!-- 1 --></td>
<td>Wouter Becq <!-- 1 --></td>
</tr>
<tr>
<td>Yuen Hoe Lim <!-- 1 --></td>
<td>Yunhe Guo <!-- 1 --></td>
</tr>
<tr>
<td>Zhigalin Alexander <!-- 1 --></td>
<td>Zoe Clifford <!-- 1 --></td>
</tr>
<tr>
<td>aa bb <!-- 1 --></td>
<td>bombless <!-- 1 --></td>
</tr>
<tr>
<td>est 31 <!-- 1 --></td>
<td>gamazeps <!-- 1 --></td>
</tr>
<tr>
<td>hololeap hololeap <!-- 1 --></td>
<td>m.eik michalke <!-- 1 --></td>
</tr>
<tr>
<td>mdinger <!-- 1 --></td>
<td>tfry <!-- 1 --></td>
</tr>
<tr>
<td>visualfc <!-- 1 --></td>
<td>Émeric Dupont <!-- 1 --></td>
</tr>
<tr>
<td>Сковорода Никита Андреевич <!-- 1 --></td>
</table>

