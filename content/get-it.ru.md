---
author: Christoph Cullmann
date: 2010-07-09 14:40:05+00:00
discoverData:
  kate:
    alt: Установить Kate с использованием центра программ Discover или другого приложения
      для работы с каталогами приложений AppStream
    subtitle: с использованием центра программ Discover
    title: Установить Kate
  kwrite:
    alt: Установить KWrite с использованием центра программ Discover или другого приложения
      для работы с каталогами приложений AppStream
    subtitle: с использованием центра программ Discover
    title: Установить KWrite
hideMeta: true
menu:
  main:
    weight: 3
sassFiles:
- /sass/get-it.scss
title: Загрузить Kate
---
{{< get-it src="/wp-content/uploads/2010/07/Tux.svg_-254x300.png" >}}

### Linux и UNIX-ы

+ Установите [Kate](https://apps.kde.org/en/kate) или [KWrite](https://apps.kde.org/en/kwrite) из [пакетов вашего дистрибутива](https://kde.org/distributions/)

{{< get-it-button-discover >}}

Эти кнопки работают только с [Discover](https://apps.kde.org/en/discover) и другими AppStream-совместимых центрами приложений, в частности [GNOME Software](https://wiki.gnome.org/Apps/Software). Вы также можете воспользоваться пакетным менеджером вашего дистрибутива, в частности [Muon](https://apps.kde.org/en/muon) или [Synaptic](https://wiki.debian.org/Synaptic).

{{< /get-it-button-discover >}}

+ [Пакет Kate в Snapcraft](https://snapcraft.io/kate)

{{< get-it-button link="https://snapcraft.io/kate" img="/images/snap-store-badge-en.png" width="200" alt=`Получить Kate в Snap Store` />}}

+ [Выпуск Kate (64-бит) в формате пакета AppImage](https://binary-factory.kde.org/job/Kate_Release_appimage/) *
+ [Ночная сборка Kate (64-бит) в формате пакета AppImage](https://binary-factory.kde.org/job/Kate_Nightly_appimage/) **
+ [Собрать Kate](/build-it/#linux) из исходников.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/Windows_logo_–_2012_dark_blue.svg_.png" >}}

### Windows

+ [Kate в Microsoft Store](https://www.microsoft.com/store/apps/9NWMW7BB59HW)

{{< get-it-button link="https://www.microsoft.com/store/apps/9NWMW7BB59HW" img="/images/get-it-from-ms.png" width="200" alt=`Получить Kate в Microsoft Store` />}}

+ [Kate в Chocolatey](https://chocolatey.org/packages/kate)
+ [Файл для установки Kate (64-бит)](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Release_win64/) *
+ [Файл для установки ночной сборки Kate (64-бит)](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Nightly_win64/) **
+ [Собрать Kate](/build-it/#windows) из исходников.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/macOS-logo-2017.png" >}}

### macOS

+ [Установщик Kate](https://binary-factory.kde.org/view/MacOS/job/Kate_Release_macos/) *
+ [Установщик ночной сборки Kate](https://binary-factory.kde.org/view/MacOS/job/Kate_Release_macos/) *
+ [Собрать Kate](/build-it/#mac) из исходников.

{{< /get-it >}}


{{< get-it-info >}}

**О выпусках Kate:** <br> [Kate](https://apps.kde.org/en/kate) и [KWrite](https://apps.kde.org/en/kwrite) являются частью набора [KDE Applications](https://apps.kde.org), который [обычно обновляется 3 раза в год](https://community.kde.org/Schedules). Движки [редактирования текста](https://api.kde.org/frameworks/ktexteditor/html/) и [подсветки синтаксиса](https://api.kde.org/frameworks/syntax-highlighting/html/) являются частью библиотек [KDE Frameworks](https://kde.org/announcements/kde-frameworks-5.0/), которые [обновляются ежемесячно](https://community.kde.org/Schedules/Frameworks). О новых выпусках объявляют [здесь](https://kde.org/announcements/).

\* В этих пакетах **сборок** содержатся последние версии Kate и KDE Frameworks.

\*\* **Ночные** сборки автоматически собираются с исходников каждый день, поэтому они могут работать нестабильно, иметь баги или возможности, которые еще не реализованы до конца. Рекомендуется использовать их только с целью тестирования.

{{< /get-it-info >}}
