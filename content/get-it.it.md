---
author: Christoph Cullmann
date: 2010-07-09 14:40:05+00:00
discoverData:
  kate:
    alt: Installa Kate da Discover o da altri negozi di applicazioni AppStream
    subtitle: tramite Discover
    title: Installa Kate
  kwrite:
    alt: Installa KWrite da Discover o da altri negozi di applicazioni AppStream
    subtitle: tramite Discover
    title: Installa KWrite
hideMeta: true
menu:
  main:
    weight: 3
sassFiles:
- /sass/get-it.scss
title: Ottieni Kate
---
{{< get-it src="/wp-content/uploads/2010/07/Tux.svg_-254x300.png" >}}

### Linux & sistemi Unix

+ Installa [Kate](https://apps.kde.org/en/kate) o [KWrite](https://apps.kde.org/en/kwrite) dalla [tua distribuzione](https://kde.org/distributions/)

{{< get-it-button-discover >}}

Questi pulsanti funzionano solo con [Discover](https://apps.kde.org/en/discover) e con altri store di applicazioni AppStream, ad esempio [GNOME Software](https://wiki.gnome.org/Apps/Software). Puoi anche utilizzare il gestore di pacchetti della tua distribuzione, ad esempio [Muon](https://apps.kde.org/en/muon) o [Synaptic](https://wiki.debian.org/Synaptic).

{{< /get-it-button-discover >}}

+ [Il pacchetto Snap di Kate in Snapcraft](https://snapcraft.io/kate)

{{< get-it-button link="https://snapcraft.io/kate" img="/images/snap-store-badge-en.png" width="200" alt=`Ottieni Kate da Snap store` />}}

+ [Pacchetto AppImage della versione di Kate (64bit)](https://binary-factory.kde.org/job/Kate_Release_appimage/) *
+ [Pacchetto AppImage della versione nightly di Kate (64bit)](https://binary-factory.kde.org/job/Kate_Nightly_appimage/) **
+ [Compilalo](/build-it/#linux) dai sorgenti.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/Windows_logo_–_2012_dark_blue.svg_.png" >}}

### Windows

+ [Kate nel Microsoft Store](https://www.microsoft.com/store/apps/9NWMW7BB59HW)

{{< get-it-button link="https://www.microsoft.com/store/apps/9NWMW7BB59HW" img="/images/get-it-from-ms.png" width="200" alt=`Ottieni Kate da Microsoft store` />}}

+ [Kate da Chocolatey](https://chocolatey.org/packages/kate)
+ [Installatore di Kate (64bit)](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Release_win64/) *
+ [Installatore versione nightly di Kate (64bit)](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Nightly_win64/) **
+ [Compilalo](/build-it/#windows) dai sorgenti.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/macOS-logo-2017.png" >}}

### macOS

+ [Installatore di Kate](https://binary-factory.kde.org/view/MacOS/job/Kate_Release_macos/) *
+ [Installatore versione nightly di Kate](https://binary-factory.kde.org/view/MacOS/job/Kate_Nightly_macos/) **
+ [Compilalo](/build-it/#mac) dai sorgenti.

{{< /get-it >}}


{{< get-it-info >}}

**Informazioni sulle versioni:** <br> [Kate](https://apps.kde.org/en/kate) e [KWrite](https://apps.kde.org/en/kwrite) fanno parte delle [applicazioni KDE](https://apps.kde.org), che vengono [rilasciate in massa tipicamente 3 volte all'anno](https://community.kde.org/Schedules). I motori di [modifica del testo](https://api.kde.org/frameworks/ktexteditor/html/) e di [evidenziazione della sintassi](https://api.kde.org/frameworks/syntax-highlighting/html/) sono forniti da [KDE Frameworks](https://kde.org/announcements/kde-frameworks-5.0/), che viene [aggiornato mensilmente](https://community.kde.org/Schedules/Frameworks). Le nuove versioni vengono annunciate [qui](https://kde.org/announcements/).

\* I pacchetti **di rilascio** contengono l'ultima versione di Kate e di KDE Frameworks.

\*\* I pacchetti **nightly** sono compilati automaticamente tutti i giorni dal codice sorgente, quindi potrebbero essere instabili oppure contenere errori o funzionalità non complete. Sono raccomandati solamente a scopo di prova.

{{< /get-it-info >}}
