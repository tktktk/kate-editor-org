---
author: Christoph Cullmann
date: 2021-04-29 18:13:00+00:00
hideMeta: true
menu:
  main:
    parent: menu
    weight: 100
title: Kateva maskota
---
Katevo maskoto, Kate Cyber Žolno, je oblikoval [Tyson Tan](https://www.tysontan.com/).

Trenutni razvoj naše maskote je bil prvič razkrit [v aprilu 2021](/post/2021/2021-04-28-lets-welcome-kate-the-cyber-woodpecker/).

## Kate Cyber Žolna

![Kate Cyber Žolna](/images/mascot/electrichearts_20210103_kate_normal.png)

## Verzija brez spremnega besedila

![Kate Cyber Žolna - brez besedila](/images/mascot/electrichearts_20210103_kate_notext.png)

## Verzija brez ozadja

![Kate Cyber Žolna - brez ozadja](/images/mascot/electrichearts_20210103_kate_transparent.png)

## Licenciranje

Za licenco, če citiramo Tysona:

> S tem podarjam novi Kate Cyber Žolno projektu urejevalnik Kate.Umetniška dela imajo dvojno licenco pod LGPL (ali kako drugače kot urejevalnik Kate) in Creative Commons BY-SA. Na ta način me ni treba prositi za to dovoljenje pred uporabo/spreminjanjem maskote.

## Zgodovina oblikovanja Kate Cyber Žolne

Tysona sem vprašal, ali lahko delim vmesne različice, ki smo si jih izmenjali, da pokažemo evolucijo in strinjal se je ter celo napisal nekaj kratkih povzetkov o posameznih korakih. To vključuje začetno maskoto in ikono, ki ustrezatemu razvoju oblikovanja. Zato samo citiram, kaj je napisal o različnih korakih spodaj.

### 2014 - Kate Žolna

![2014 version - Kate Žolna](/images/mascot/history/Kate_editor_mascot_woodpecker.png)

> Različica iz leta 2014 je bila narejena morda v času moje najnižje točke kot umetnika. V teh letih nisem risal veliko in sem bil na robu, da se popolnoma odpovem risanju. V tej različici je bilo nekaj zanimivih idej, in sicer {} oklepaji na prsih in barvno shemo, ki jo navdihuje shema poudarjanja v barvni shemi kisika. Vendar je bila izvedba zelo poenostavljena - takrat sem namerno prenehal uporabljati kakršne koli tehnike, ki bi razkrile moje neustrezne osnovne spretnosti. V naslednjih letih bi šel skozi naporen proces obnoviti svojo pokvarjeno, samouko umetniško podlago od začetka. Sem hvaležen, da je ekipa Kate še vedno obdržala to različico na svoji spletni strani za več let.

### 2020 - Nova ikona Kate

![2020 - nova ikona Kate](/images/mascot/history/kate-3000px.png)

> Nova ikona Kate je bila moj edini poskus prave vektorske umetnosti. Bilo je ob takrat sem začel razvijati nekaj šibkega umetniškega čuta. Prej še nisem imel izkušenj pri oblikovanju ikon, nisem poznal discipline, kot so matematično vodeni proporci. Vsaka oblika in krivulja je bila prilagojena z uporabo mojega umetniškega instinkta. Koncept je bil zelo preprost: gre za ptico v obliki črke "K", ki mora biti oster. Ptica je bila mogočnejša, dokler je Christoph ni usmeril ven, sem jo kasneje spremenil v bolj gladko obliko.

### 2021 - Skica maskote

![2021 verzija - Skica nove maskote](/images/mascot/history/electrichearts_20210103_kate_sketch.png)

> V tej začetni skici me je zamikalo, da bi se odrekel robotskemu konceptu in namesto tega narisal zgolj prikupno lepo ptico.

### 2021 - Oblikovanje maskote

![2021 verzija - Ponovno oblikovanje nove maskote](/images/mascot/history/electrichearts_20210103_kate_design.png)

> Preoblikovanje Kateve maskote se je spet nadaljevalo, ko sem končal z risanjem novega slike dobrodošlice za prihajajoči program Krita 5.0. Lahko sem se potrudil, da naredim vsakič več, ko sem takrat poskusil novo sliko, sem bil odločen ohraniti robotski koncept. Čeprav so bili proporci neomejeni, je bila mehanska zasnova elementov bolj ali manj skovana v tem.

### 2021 - Končna maskota

![2021 verzija - Končna Kate Cyber Žolna](/images/mascot/electrichearts_20210103_kate_normal.png)

> Proporci so bili prilagojen, ko je na to opozoril Christoph. Poza je bila prilagojena, da se počuti bolj udobno. Krila so bila na novo narisana po preučevanju pravih ptičjih kril. Ročno je bila narisana čudovita, podrobna tipkovnica, dapoudari glavno uporabo aplikacije. Abstraktno ozadje je bilo dodano z ustreznim dizajnom 2D tipografije. Splošna ideja je: nedvomno vrhunska in umetna, a tudi nedvomno polna življenja in človečnosti.
