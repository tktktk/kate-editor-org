---
author: Christoph Cullmann
date: 2010-07-09 14:40:05+00:00
discoverData:
  kate:
    alt: Install Kate via Discover or other AppStream application stores
    subtitle: via Discover
    title: Install Kate
  kwrite:
    alt: Install KWrite via Discover or other AppStream application stores
    subtitle: via Discover
    title: Install KWrite
hideMeta: true
menu:
  main:
    weight: 3
sassFiles:
- /sass/get-it.scss
title: Hangi Kate
---
{{< get-it src="/wp-content/uploads/2010/07/Tux.svg_-254x300.png" >}}

### Linux & Unices

+ Install [Kate](https://apps.kde.org/en/kate) or [KWrite](https://apps.kde.org/en/kwrite) from [your distribution](https://kde.org/distributions/)

{{< get-it-button-discover >}}

These buttons only works with [Discover](https://apps.kde.org/en/discover) and other AppStream application stores, such as [GNOME Software](https://wiki.gnome.org/Apps/Software). You can also use your distribution's package manager, such as [Muon](https://apps.kde.org/en/muon) or [Synaptic](https://wiki.debian.org/Synaptic).

{{< /get-it-button-discover >}}

+ [Kate's Snap package in Snapcraft](https://snapcraft.io/kate)

{{< get-it-button link="https://snapcraft.io/kate" img="/images/snap-store-badge-en.png" width="200" alt=`Get Kate on the Snap store` />}}

+ [Kate's release (64bit) AppImage package](https://binary-factory.kde.org/job/Kate_Release_appimage/) *
+ [Kate's nightly (64bit) AppImage package](https://binary-factory.kde.org/job/Kate_Nightly_appimage/) **
+ [Build it](/build-it/#linux) from source.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/Windows_logo_–_2012_dark_blue.svg_.png" >}}

### Windows

+ [Kate in the Microsoft Store](https://www.microsoft.com/store/apps/9NWMW7BB59HW)

{{< get-it-button link="https://www.microsoft.com/store/apps/9NWMW7BB59HW" img="/images/get-it-from-ms.png" width="200" alt=`Get Kate from the Microsoft store` />}}

+ [Kate via Chocolatey](https://chocolatey.org/packages/kate)
+ [Kate release (64bit) installer](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Release_win64/) *
+ [Kate nightly (64bit) installer](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Nightly_win64/) **
+ [Build it](/build-it/#windows) from source.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/macOS-logo-2017.png" >}}

### macOS

+ [Kate release installer](https://binary-factory.kde.org/view/MacOS/job/Kate_Release_macos/) *
+ [Kate nightly installer](https://binary-factory.kde.org/view/MacOS/job/Kate_Nightly_macos/) **
+ [Build it](/build-it/#mac) from source.

{{< /get-it >}}


{{< get-it-info >}}

**About the releases:** <br> [Kate](https://apps.kde.org/en/kate) and [KWrite](https://apps.kde.org/en/kwrite) are part of [KDE Applications](https://apps.kde.org), which are [released typically 3 times a year en-masse](https://community.kde.org/Schedules). The [text editing](https://api.kde.org/frameworks/ktexteditor/html/) and the [syntax highlighting](https://api.kde.org/frameworks/syntax-highlighting/html/) engines are provided by [KDE Frameworks](https://kde.org/announcements/kde-frameworks-5.0/), which is [updated monthly](https://community.kde.org/Schedules/Frameworks). New releases are announced [here](https://kde.org/announcements/).

\* The **release** packages contain the latest version of Kate and KDE Frameworks.

\*\* The **nightly** packages are automatically compiled daily from source code, therefore they may be unstable and contain bugs or incomplete features. These are only recommended for testing purposes.

{{< /get-it-info >}}
