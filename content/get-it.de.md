---
author: Christoph Cullmann
date: 2010-07-09 14:40:05+00:00
discoverData:
  kate:
    alt: Kate über Discover oder andere AppStream-Anwendungen installieren
    subtitle: über Discover
    title: Kate installieren
  kwrite:
    alt: KWrite über Discover oder andere AppStream-Anwendungen installieren
    subtitle: über Discover
    title: KWrite installieren
hideMeta: true
menu:
  main:
    weight: 3
sassFiles:
- /sass/get-it.scss
title: Kate herunterladen
---
{{< get-it src="/wp-content/uploads/2010/07/Tux.svg_-254x300.png" >}}

### Linux & Unix-Varianten

+ Installieren Sie [Kate](https://apps.kde.org/en/kate) oder [KWrite](https://apps.kde.org/en/kwrite) von [Ihrer Distribution](https://kde.org/distributions/)

{{< get-it-button-discover >}}

Diese Knöpfe funktionieren nur mit [Discover](https://apps.kde.org/en/discover) und anderen AppStream-Anwendungen, wie z. B. [GNOME Software](https://wiki.gnome.org/Apps/Software). Sie können auch die Paketverwaltung Ihrer Distribution verwenden, wie z. B. [Muon](https://apps.kde.org/en/muon) oder [Synaptic](https://wiki.debian.org/Synaptic).

{{< /get-it-button-discover >}}

+ [Snap-Paket von Kate in Snapcraft](https://snapcraft.io/kate)

{{< get-it-button link="https://snapcraft.io/kate" img="/images/snap-store-badge-en.png" width="200" alt=`Kate aus dem Snap Store` />}}

+ [AppImage-Paket von Kate (64bit - Standard-Version)](https://binary-factory.kde.org/job/Kate_Release_appimage/) *
+ [AppImage-Paket von Kate (64bit) - Entwickler-Version](https://binary-factory.kde.org/job/Kate_Release_appimage/) **
+ [Aus dem Quelltext](/build-it/#linux) übersetzen.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/Windows_logo_–_2012_dark_blue.svg_.png" >}}

### Windows

+ [Kate im Microsoft Store](https://www.microsoft.com/store/apps/9NWMW7BB59HW)

{{< get-it-button link="https://www.microsoft.com/store/apps/9NWMW7BB59HW" img="/images/get-it-from-ms.png" width="200" alt=`Kate  aus dem Microsoft Store` />}}

+ [Kate über Chocolatey](https://chocolatey.org/packages/kate)
+ [Kate-Installationsprogramm (64bit) - Standard-Version](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Release_win64/) *
+ [Kate -Installationsprogramm (64bit) - Entwickler-Version](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Nightly_win64/) **
+ [Aus dem Quelltext](/build-it/#windows) übersetzen.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/macOS-logo-2017.png" >}}

### macOS

+ [Kate-Installationsprogramm - Standard-Version](https://binary-factory.kde.org/view/MacOS/job/Kate_Release_macos/) *
+ [Kate-Installationsprogramm - Entwickler-Version](https://binary-factory.kde.org/view/MacOS/job/Kate_Release_macos/) **
+ [Aus dem Quelltext](/build-it/#mac) übersetzen.

{{< /get-it >}}


{{< get-it-info >}}

**Über die Veröffentlichungen:** <br> [Kate](https://apps.kde.org/en/kate) und [KWrite](https://apps.kde.org/en/kwrite) sind Teil der [KDE-Anwendungen](https://apps.kde.org), die normalerweise [3-mal pro Jahr gemeinsam](https://community.kde.org/Schedules) veröffentlicht werden. Der [Texteditor](https://api.kde.org/frameworks/ktexteditor/html/) und die Bibliothek [Syntaxhervorhebung](https://api.kde.org/frameworks/syntax-highlighting/html/) werden von [KDE Frameworks](https://kde.org/announcements/kde-frameworks-5.0/0) bereitgestellt, die  [monatlich aktualisiert](https://community.kde.org/Schedules/Frameworks) wird. Neue Versionen werden [hier](https://kde.org/announcements/) angekündigt.

\* Die **Standard**-Pakete enthalten die letzte Version von Kate und KDE Frameworks.

\*\* Die **Entwickler**-Pakete werden täglich automatisch aus dem Quellcode kompiliert kompiliert, daher können sie instabil sein und Fehler oder unvollständige Funktionen enthalten. Diese werden nur zu Testzwecken empfohlen.

{{< /get-it-info >}}
