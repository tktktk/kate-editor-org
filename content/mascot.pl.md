---
author: Christoph Cullmann
date: 2021-04-29 18:13:00+00:00
hideMeta: true
menu:
  main:
    parent: menu
    weight: 100
title: Maskotka Kate
---
Maskotka Kate, Kate cyber dzięcioł, została zaprojektowana przez [Tysona Tana](https://www.tysontan.com/).

Bieżąca ewolucja naszej maskotki została odsłonięta po raz pierwszy [w kwietniu 2021](/post/2021/2021-04-28-lets-welcome-kate-the-cyber-woodpecker/).

## Kate - cyber dzięcioł

![Kate cyber dzięcioł](/images/mascot/electrichearts_20210103_kate_normal.png)

## Wersja bez tekstu nagłówka

![Kate cyber dzięcioł - Bez tekstu](/images/mascot/electrichearts_20210103_kate_notext.png)

## Wersja bez tła

![Kate cyber dzięcioł - Bez tła](/images/mascot/electrichearts_20210103_kate_transparent.png)

## Licencja

Dla licencjonowania, aby zacytować Tysona:

> Niniejszym podarowuję nowego Kate cyber dzięcioła projektowi edytora Kate. Grafika jest podwójnie licencjonowana jako LGPL (lub tak samo jak edytora Kate) oraz Creative Commons BY-SA. W ten sposób nie musisz mnie pytać o pozwolenie na używanie/zmianę maskotki.

## Historia projektu Kate - cyber dzięcioła

Zapytałem Tysona, czy mogę pokazać pośrednie wersje, którymi się wymienialiśmy, aby pokazać rozwój, a on się zgodził i nawet napisał kilka krótkich posumowań o poszczególnych krokach. Zawiera się w tym początkowa maskotka oraz ikona, która pasuje do rozwoju tego projektu. Stąd poniżej ja tylko cytuję to, co on napisał o poszczególnych krokach.

### 2014 - Kate oraz dzięcioł

![Wersja 2014 - Kate dzięcioł](/images/mascot/history/Kate_editor_mascot_woodpecker.png)

> Wersja z 2014 powstałą prawdopodobnie w najniższym punkcie mojej artystycznej drogi. Nie rysowałem wiele od lat i byłem na progu odpuszczenia sobie tego całkowicie. Ta wersja zawiera ciekawe pomysły, mianowicie nawiasy {} na klatce piersiowej oraz zestaw barw natchniony podświetlaniem kodu doxygen. Jednakże wykonanie było bardzo uproszczone -- na tamten czas, celowo zaprzestałem używania bardziej złożonych technik, aby uwidocznić moje niewystarczające umiejętności podstawowe. W następnych latach przeszedłem przez ciężki proces odbudowywania moich utraconych, samonauczonych podstaw rysowania. Jestem wdzięczny, że zespół Kate nadal trzymało to na swojej stronie przez wiele lat.

### 2020 - Nowa ikona Kate

![2020 - Nowa ikona Kate](/images/mascot/history/kate-3000px.png)

> Jedyna moja próba użycia prawdziwej techniki wektorowej, to podczas tworzenia nowej ikony Kate. To był czas podczas, którego zacząłem rozwijać bardziej złożony zmysł artystyczny. Nie miałem wcześniejszego doświadczenia w projektowaniu ikon, nie było żadnej dyscypliny zachowania matematycznych stosunków. Każdy kształt i krzywa zostały dostosowane przy użyciu mojego instynktu jako artysty. Zamysł był bardzo prosty: to ma być ptak w kształcie litery "K" i musi być ostry. Ptak był masywniejszy, zanim Christoph zwrócił na to uwagę, a później zmieniłem go na trochę chudszego.

### 2021 - Szkic Maskotki

![Wersja 2021 - Szkic nowej maskotki](/images/mascot/history/electrichearts_20210103_kate_sketch.png)

> Na tym początkowym szkicu, kusiło, żeby porzucić robotyczny wygląd i po prostu narysować uroczo wyglądającego ptaka.

### 2021 - Projekt Maskotki

![wersja 2021 - Przeprojektowanie nowej maskotki](/images/mascot/history/electrichearts_20210103_kate_design.png)

> Przeprojektowywanie maskotki Kate zostało wznowione po tym jak ukończyłem rysowanie nowego ekranu powitalnego dla nadchodzącej Krity 5.0. Byłem w stanie wymagać od siebie więcej za każdym razem, gdy próbowałem stworzyć nowy obraz, więc byłem też przekonany aby zachować robotyczny wygląd. Mimo że proporcje się nie zgadzały, to elementy mechaniczne były mniej więcej na swoim miejscu.

### 2021 - Końcowa Maskotka

![Wersja 2021- Ostateczna Kate jako cyber dzięcioł](/images/mascot/electrichearts_20210103_kate_normal.png)

> Proporcje zostały dostosowane po uwadze Christopha. Postawa została dostosowana, aby wyglądała na bardziej wygodną. Skrzydła zostały przerysowane po przyjrzeniu się skrzydłom prawdziwych ptaków. Ciekawie wyglądająca i szczegółowa klawiatura została narysowana od ręki, aby podkreślić główne przeznaczenie aplikacji. Zostało dodane abstrakcyjne tło z właściwie zaprojektowaną typografią 2D. Ogólnym zamysłem jest: nowoczesność i sztuczność nie do pomylenia, lecz także pełnia życia i ludzkość.
