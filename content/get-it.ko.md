---
author: Christoph Cullmann
date: 2010-07-09 14:40:05+00:00
discoverData:
  kate:
    alt: Discover 및 다른 AppStream 앱 스토어로 Kate 설치
    subtitle: Discover 사용
    title: Kate 설치
  kwrite:
    alt: Discover 및 다른 AppStream 앱 스토어로 KWrite 설치
    subtitle: Discover 사용
    title: KWrite 설치
hideMeta: true
menu:
  main:
    weight: 3
sassFiles:
- /sass/get-it.scss
title: Kate 다운로드
---
{{< get-it src="/wp-content/uploads/2010/07/Tux.svg_-254x300.png" >}}

### 리눅스와 유닉스

+ [배포판](https://kde.org/distributions/)에서 [Kate](https://apps.kde.org/ko/kate)나 [KWrite](https://apps.kde.org/ko/kwrite) 설치

{{< get-it-button-discover >}}

이 단추는 [Discover](https://apps.kde.org/ko/discover) 또는 [그놈 소프트웨어](https://wiki.gnome.org/Apps/Software)와 같은 AppStream 앱 스토어가 설치되어 있어야 합니다. [Muon](https://apps.kde.org/ko/muon)이나 [Synaptic](https://wiki.debian.org/Synaptic) 등 배포판 패키지 관리자로도 설치할 수 있습니다..

{{< /get-it-button-discover >}}

+ [Snapcraft의 Kate Snap 패키지](https://snapcraft.io/kate)

{{< get-it-button link="https://snapcraft.io/kate" img="/images/snap-store-badge-en.png" width="200" alt=`Snap 스토어에서 Kate 다운로드` />}}

+ [Kate 릴리스(64비트) AppImage 패키지](https://binary-factory.kde.org/job/Kate_Release_appimage/) *
+ [Kate 일일 빌드 (64비트) AppImage 패키지](https://binary-factory.kde.org/job/Kate_Nightly_appimage/) **
+ 소스 코드에서 [빌드](/build-it/#linux)할 수 있습니다.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/Windows_logo_–_2012_dark_blue.svg_.png" >}}

### Windows

+ [Microsoft 스토어의 Kate](https://www.microsoft.com/store/apps/9NWMW7BB59HW)

{{< get-it-button link="https://www.microsoft.com/store/apps/9NWMW7BB59HW" img="/images/get-it-from-ms.png" width="200" alt=`Microsoft 스토어에서 Kate 다운로드` />}}

+ [Chocolatey에서 Kate 다운로드](https://chocolatey.org/packages/kate)
+ [Kate 릴리스(64비트) 설치 도구](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Release_win64/) *
+ [Kate 일일 빌드(64비트) 설치 도구](https://binary-factory.kde.org/view/Windows%2064-bit/job/Kate_Nightly_win64/) **
+ 소스 코드에서 [빌드](/build-it/#windows)할 수 있습니다.

{{< /get-it >}}

{{< get-it src="/wp-content/uploads/2010/07/macOS-logo-2017.png" >}}

### macOS

+ [Kate 릴리스 설치 도구](https://binary-factory.kde.org/view/MacOS/job/Kate_Release_macos/) *
+ [Kate 일일 빌드 설치 도구](https://binary-factory.kde.org/view/MacOS/job/Kate_Nightly_macos/) **
+ 소스 코드에서 [빌드](/build-it/#mac)할 수 있습니다.

{{< /get-it >}}


{{< get-it-info >}}

**릴리스 정보:** <br> [Kate](https://apps.kde.org/ko/kate)와 [KWrite](https://apps.kde.org/ko/kwrite)는 [년간 3회 릴리스되는](https://community.kde.org/Schedules) [KDE 프로그램](https://apps.kde.org/ko/)의 일부로 출시됩니다. [텍스트 편집](https://api.kde.org/frameworks/ktexteditor/html/)과 [구문 강조](https://api.kde.org/frameworks/syntax-highlighting/html/) 엔진은 [매월마다 업데이트되는](https://community.kde.org/Schedules/Frameworks) [KDE 프레임워크](https://kde.org/announcements/kde-frameworks-5.0/)에서 제공합니다. 새 릴리스는 [여기](https://kde.org/announcements/)에 공지됩니다.

\* **릴리스** 패키지는 Kate와 KDE 프레임워크 최신 버전을 포함합니다.

\*\* **일일 빌드** 패키지는 매일 소스 코드에서 자동으로 컴파일되며, 불안정하거나 버그 또는 불완전한 기능이 있을 수도 있습니다. 테스트 목적으로만 추천합니다.

{{< /get-it-info >}}
