#!/bin/bash

# failures are evil
set -e

# Make sure we run with the latest version of the theme
# hugo mod get invent.kde.org/websites/aether-sass@hugo

# run hugo in local server mode, show all stuff
exec hugo server -D --buildFuture
